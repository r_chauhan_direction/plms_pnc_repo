package zk.springboot.zkspringbootproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//Required to scan jpa repositories which is present in dependent project(i.e. pucplm)
@EnableJpaRepositories(basePackages = { "puc.plm.service" })
//Required to scan entity beans which is present in dependent project(i.e. pucplm)
@EntityScan(basePackages = {"puc.plm"})
//Scanning all entities provided in the base packages
@ComponentScan(basePackages={"puc.plm","zk.springboot"})
public class ZkspringbootprojectApplication {
	public static void main(String[] args) {
		SpringApplication.run(ZkspringbootprojectApplication.class, args);
	}
}
