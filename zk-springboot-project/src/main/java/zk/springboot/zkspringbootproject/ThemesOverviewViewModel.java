
package zk.springboot.zkspringbootproject;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import puc.plm.service.domains.masterdata.MasterdataQueryService;
import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentGroupDto;

//Annotation required to use spring beans inside the model
//DelegatingVariableResolver is a variable resolver used to retrieve the Spring-managed bean, so the variable will be retrieved and instantiated by Spring
@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ThemesOverviewViewModel {

	//Annotation used to wire spring beans. It wont work without @VariableResolver annotation at class level
	@WireVariable
	private MasterdataQueryService masterdataQueryServiceImpl;

	private List<AssortmentGroupDto> assortmentGroupList = new ArrayList<AssortmentGroupDto>();

	private AssortmentGroupDto selectedAssortmentGroup = new AssortmentGroupDto(new AssortmentGroupId(245), 48, "xyz");

	//Initialization of model when it is loaded first time/refreshing the page
	@Init
	public void init() {
		loadData();
	}

	private void loadData() {
		System.out.println("Service:" + this.masterdataQueryServiceImpl);
		if (this.masterdataQueryServiceImpl != null) {
			this.assortmentGroupList = this.masterdataQueryServiceImpl.getAllAssortmentGroups();
			this.assortmentGroupList.add(0, this.selectedAssortmentGroup);
		}
	}

	public List<AssortmentGroupDto> getAssortmentGroupList() {
		return assortmentGroupList;
	}

	public AssortmentGroupDto getSelectedAssortmentGroup() {
		return selectedAssortmentGroup;
	}

	public void setSelectedAssortmentGroup(AssortmentGroupDto selectedAssortmentGroup) {
		this.selectedAssortmentGroup = selectedAssortmentGroup;
	}

}
