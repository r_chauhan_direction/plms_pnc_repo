package puc.plm.service.domains.supply.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class SupplierStyleVariantSizeId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public SupplierStyleVariantSizeId() {
		super();
	}

	public SupplierStyleVariantSizeId(UUID value) {
		super(value);
	}
}
