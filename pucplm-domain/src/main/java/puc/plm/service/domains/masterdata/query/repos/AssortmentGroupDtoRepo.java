package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentGroupDto;

public interface AssortmentGroupDtoRepo extends JpaRepository<AssortmentGroupDto, AssortmentGroupId> {

}
