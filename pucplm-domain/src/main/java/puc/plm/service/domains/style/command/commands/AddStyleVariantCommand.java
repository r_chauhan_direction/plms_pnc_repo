package puc.plm.service.domains.style.command.commands;

import java.util.Optional;

import puc.plm.service.common.messages.Command;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleId;

/**
 * Command to add a new style variant to a style
 */
public class AddStyleVariantCommand implements Command {

	private StyleId styleId;

	private Optional<StyleVariantData> styleVariantData = Optional.empty();

	public AddStyleVariantCommand(StyleId styleId) {
		super();
		this.styleId = styleId;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public Optional<StyleVariantData> getStyleVariantData() {
		return styleVariantData;
	}

	public void setStyleVariantData(Optional<StyleVariantData> styleVariantData) {
		this.styleVariantData = styleVariantData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		result = prime * result + ((styleVariantData == null) ? 0 : styleVariantData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		AddStyleVariantCommand other = (AddStyleVariantCommand) obj;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		if (styleVariantData == null) {
			if (other.styleVariantData != null) return false;
		} else if (!styleVariantData.equals(other.styleVariantData)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "AddStyleVariantCommand [styleId=" + styleId + ", styleVariantData=" + styleVariantData + "]";
	}
}
