package puc.plm.service.domains.supply.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class SupplierSizeId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public SupplierSizeId() {
		super();
	}

	public SupplierSizeId(UUID value) {
		super(value);
	}
}
