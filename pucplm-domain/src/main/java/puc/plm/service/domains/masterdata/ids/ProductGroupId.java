package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class ProductGroupId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected ProductGroupId() {
		super();
	}

	public ProductGroupId(Integer value) {
		super(value);
	}
}
