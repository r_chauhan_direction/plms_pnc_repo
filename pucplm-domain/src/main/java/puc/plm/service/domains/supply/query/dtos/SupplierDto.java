package puc.plm.service.domains.supply.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.supply.ids.SupplierId;

@Entity
public class SupplierDto extends ActiveEntity<SupplierId> {

	/**
	 * number for supplier identification by the user
	 */
	private Integer supplierNumber;

	/**
	 * short name of the supplier
	 */
	private String shortName;

	/**
	 * full name of the supplier
	 */
	private String name;

	protected SupplierDto() {}

	public SupplierDto(SupplierId id, Integer supplierNumber, String shortName, String name) {
		super(id);
		this.supplierNumber = supplierNumber;
		this.shortName = shortName;
		this.name = name;
	}

	public Integer getSupplierNumber() {
		return supplierNumber;
	}

	public String getShortName() {
		return shortName;
	}

	public String getName() {
		return name;
	}
}
