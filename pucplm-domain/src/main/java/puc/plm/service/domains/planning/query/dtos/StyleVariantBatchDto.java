package puc.plm.service.domains.planning.query.dtos;

import puc.plm.service.domains.planning.ids.StyleVariantBatchId;

public class StyleVariantBatchDto {

	/**
	 * ID of the style variant batch
	 */
	private StyleVariantBatchId id;

	public StyleVariantBatchDto(StyleVariantBatchId id) {
		super();
		this.id = id;
	}

	public StyleVariantBatchId getId() {
		return id;
	}

	protected void setId(StyleVariantBatchId id) {
		this.id = id;
	}
}
