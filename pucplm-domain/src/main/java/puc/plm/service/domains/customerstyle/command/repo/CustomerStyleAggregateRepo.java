package puc.plm.service.domains.customerstyle.command.repo;

import org.springframework.stereotype.Component;

import puc.plm.service.common.aggregate.AggregateRepo;
import puc.plm.service.domains.customerstyle.command.CustomerStyle;
import puc.plm.service.domains.customerstyle.command.CustomerStyleAggregate;
import puc.plm.service.domains.customerstyle.ids.CustomerStyleId;

@Component
public class CustomerStyleAggregateRepo
		extends AggregateRepo<CustomerStyleAggregate, CustomerStyle, CustomerStyleId> {

	@Override
	protected CustomerStyleAggregate constructAggregate(CustomerStyleId aggregateId, CustomerStyle root) {
		return new CustomerStyleAggregate(aggregateId, root);
	}

}
