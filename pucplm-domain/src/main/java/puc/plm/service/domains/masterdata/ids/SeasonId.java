package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class SeasonId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected SeasonId() {
		super();
	}

	public SeasonId(Integer value) {
		super(value);
	}
}
