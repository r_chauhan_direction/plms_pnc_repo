package puc.plm.service.domains.style.command.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import puc.plm.service.domains.style.command.Style;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

public interface StyleRepo extends JpaRepository<Style, StyleId> {

	@Query("SELECT sv.style.id FROM StyleVariant sv WHERE sv.id = :styleVariantId")
	StyleId lookupStyleId(@Param("styleVariantId") StyleVariantId styleVariantId);

}
