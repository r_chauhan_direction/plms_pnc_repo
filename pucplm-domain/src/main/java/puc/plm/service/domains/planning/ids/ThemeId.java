package puc.plm.service.domains.planning.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class ThemeId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected ThemeId() {
		super();
	}

	public ThemeId(Integer value) {
		super(value);
	}
}
