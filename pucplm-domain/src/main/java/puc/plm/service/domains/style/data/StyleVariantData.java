package puc.plm.service.domains.style.data;

import javax.persistence.Embeddable;

@Embeddable
public class StyleVariantData {

	private String variantDescription;

	public String getVariantDescription() {
		return variantDescription;
	}

	public void setVariantDescription(String variantDescription) {
		this.variantDescription = variantDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((variantDescription == null) ? 0 : variantDescription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StyleVariantData other = (StyleVariantData) obj;
		if (variantDescription == null) {
			if (other.variantDescription != null) return false;
		} else if (!variantDescription.equals(other.variantDescription)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "StyleVariantData [variantDescription=" + variantDescription + "]";
	}
}
