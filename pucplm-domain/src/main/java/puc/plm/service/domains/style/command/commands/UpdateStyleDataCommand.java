package puc.plm.service.domains.style.command.commands;

import puc.plm.service.common.messages.Command;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.ids.StyleId;

/**
 * Command to update a style's {@link StyleData}.
 */
public class UpdateStyleDataCommand implements Command {

	private StyleId styleId;

	private StyleData data;

	public UpdateStyleDataCommand(StyleId styleId, StyleData data) {
		super();
		this.styleId = styleId;
		this.data = data;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public StyleData getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		UpdateStyleDataCommand other = (UpdateStyleDataCommand) obj;
		if (data == null) {
			if (other.data != null) return false;
		} else if (!data.equals(other.data)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateStyleDataCommand [styleId=" + styleId + ", data=" + data + "]";
	}
}
