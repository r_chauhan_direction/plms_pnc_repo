package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.masterdata.query.dtos.ProductGroupDto;

public interface ProductGroupDtoRepo extends JpaRepository<ProductGroupDto, ProductGroupId> {

}
