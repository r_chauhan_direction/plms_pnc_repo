package puc.plm.service.domains.customerstyle.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class CustomerStyleId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public CustomerStyleId() {
		super();
	}

	public CustomerStyleId(UUID value) {
		super(value);
	}
}
