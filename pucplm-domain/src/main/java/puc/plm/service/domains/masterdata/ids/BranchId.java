package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class BranchId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected BranchId() {
		super();
	}

	public BranchId(Integer value) {
		super(value);
	}
}
