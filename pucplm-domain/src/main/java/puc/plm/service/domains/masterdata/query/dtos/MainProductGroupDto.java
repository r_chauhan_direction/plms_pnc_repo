package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;

@Entity
public class MainProductGroupDto extends ActiveEntity<MainProductGroupId> {

	/**
	 * code used by users for fast retrieval
	 */
	private Integer code;

	/**
	 * full name of the main product group
	 */
	private String name;

	protected MainProductGroupDto() {}

	public MainProductGroupDto(MainProductGroupId id, Integer code, String name) {
		super(id);
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
