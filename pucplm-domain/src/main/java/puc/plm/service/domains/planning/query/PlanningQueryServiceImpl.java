package puc.plm.service.domains.planning.query;

import java.time.Duration;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.common.caching.EntityCache;
import puc.plm.service.domains.planning.PlanningQueryService;
import puc.plm.service.domains.planning.ids.PlanPeriodId;
import puc.plm.service.domains.planning.ids.PlanPeriodStyleId;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodDto;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodStyleDto;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodStyleVariantDto;
import puc.plm.service.domains.planning.query.queries.PlanPeriodStylesByCriteriaQuery;
import puc.plm.service.domains.planning.query.repos.PlanPeriodDtoRepo;

@Component
@Transactional(readOnly = true)
public class PlanningQueryServiceImpl implements PlanningQueryService {

	private static final Duration FOUR_HOURS = Duration.ofHours(4);

	private PlanPeriodDtoRepo planPeriodDtoRepo;

	private EntityCache<PlanPeriodDto, PlanPeriodId> planPeriodDtoCache;

	public PlanningQueryServiceImpl(PlanPeriodDtoRepo planPeriodDtoRepo) {
		super();
		this.planPeriodDtoRepo = planPeriodDtoRepo;

		this.planPeriodDtoCache = new EntityCache<PlanPeriodDto, PlanPeriodId>(FOUR_HOURS,
				() -> this.planPeriodDtoRepo.findAll());

	}

	@Override
	public List<PlanPeriodDto> getAllPlanPeriods() {
		return planPeriodDtoCache.findAll();
	}

	@Override
	public PlanPeriodDto getPlanPeriod(PlanPeriodId planPeriodId) {
		return planPeriodDtoCache.findById(planPeriodId);
	}

	@Override
	public List<PlanPeriodStyleDto> getPlanPeriodStyles(PlanPeriodStylesByCriteriaQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PlanPeriodStyleVariantDto> getPlanPeriodStyleVariants(PlanPeriodStyleId planPeriodStyleId) {
		// TODO Auto-generated method stub
		return null;
	}

}
