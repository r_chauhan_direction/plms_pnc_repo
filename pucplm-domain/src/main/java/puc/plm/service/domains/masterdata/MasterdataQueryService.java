package puc.plm.service.domains.masterdata;

import puc.plm.service.api.ApiMasterdataQueryService;

/**
 * Service for querying master data.
 */
public interface MasterdataQueryService extends ApiMasterdataQueryService {

}
