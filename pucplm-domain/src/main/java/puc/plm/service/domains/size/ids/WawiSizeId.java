package puc.plm.service.domains.size.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class WawiSizeId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected WawiSizeId() {
		super();
	}

	public WawiSizeId(Integer value) {
		super(value);
	}
}
