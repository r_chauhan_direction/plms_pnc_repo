package puc.plm.service.domains.style;

import puc.plm.service.api.ApiStyleCommandService;

/**
 * Service for creating and manipulating styles
 */
public interface StyleCommandService extends ApiStyleCommandService {

}
