package puc.plm.service.domains.planning.query.queries;

import java.util.Optional;

import puc.plm.service.common.messages.Query;
import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.planning.ids.PlanPeriodId;
import puc.plm.service.domains.planning.ids.PlanPeriodThemeId;

/**
 * Query for plan period styles by different critiera. All fields must not be null.
 */
public class PlanPeriodStylesByCriteriaQuery implements Query {

	/**
	 * plan period of the plan period style
	 */
	private PlanPeriodId planPeriod;

	/**
	 * assortment group the plan period style is associated with in the current plan period
	 */
	private AssortmentGroupId assortmentGroup;

	/**
	 * theme of the plan period style
	 */
	private Optional<PlanPeriodThemeId> planPeriodTheme = Optional.empty();

	/**
	 * brand of the plan period style
	 */
	private Optional<BrandId> brand = Optional.empty();

	/**
	 * main product group of the style the plan period style is associated with
	 */
	private Optional<MainProductGroupId> mainProductGroup = Optional.empty();

	public PlanPeriodStylesByCriteriaQuery(PlanPeriodId planPeriod, AssortmentGroupId assortmentGroup) {
		super();
		this.planPeriod = planPeriod;
		this.assortmentGroup = assortmentGroup;
	}

	public PlanPeriodId getPlanPeriod() {
		return planPeriod;
	}

	protected void setPlanPeriod(PlanPeriodId planPeriod) {
		this.planPeriod = planPeriod;
	}

	public AssortmentGroupId getAssortmentGroup() {
		return assortmentGroup;
	}

	protected void setAssortmentGroup(AssortmentGroupId assortmentGroup) {
		this.assortmentGroup = assortmentGroup;
	}

	public Optional<BrandId> getBrand() {
		return brand;
	}

	public void setBrand(Optional<BrandId> brand) {
		this.brand = brand;
	}

	public Optional<MainProductGroupId> getMainProductGroup() {
		return mainProductGroup;
	}

	public void setMainProductGroup(Optional<MainProductGroupId> mainProductGroup) {
		this.mainProductGroup = mainProductGroup;
	}

	public Optional<PlanPeriodThemeId> getPlanPeriodTheme() {
		return planPeriodTheme;
	}

	public void setPlanPeriodTheme(Optional<PlanPeriodThemeId> planPeriodTheme) {
		this.planPeriodTheme = planPeriodTheme;
	}
}
