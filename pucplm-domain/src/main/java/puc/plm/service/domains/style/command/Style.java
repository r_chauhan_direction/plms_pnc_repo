package puc.plm.service.domains.style.command;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import puc.plm.service.common.entity.VersionedEntity;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

@Entity
public class Style extends VersionedEntity<StyleId> {

	@Embedded
	private ProductGroupId productGroupId;

	@Embedded
	private BrandId brandId;

	private Instant creationTime;

	@OneToMany(mappedBy = "style", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<StyleVariant> styleVariants = new ArrayList<>();

	protected Style() {}

	public Style(StyleId id) {
		super(id);
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Instant creationTime) {
		this.creationTime = creationTime;
	}

	public List<StyleVariant> getStyleVariants() {
		return styleVariants;
	}

	public void addStyleVariant(StyleVariant styleVariant) {
		styleVariants.add(styleVariant);
		styleVariant.setStyle(this);
	}

	public void removeStyleVariant(StyleVariant styleVariant) {
		styleVariants.remove(styleVariant);
		styleVariant.setStyle(null);
	}

	public ProductGroupId getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(ProductGroupId productGroupId) {
		this.productGroupId = productGroupId;
	}

	public BrandId getBrandId() {
		return brandId;
	}

	public void setBrandId(BrandId brandId) {
		this.brandId = brandId;
	}

	public StyleVariant getStyleVariant(StyleVariantId styleVariantId) {
		return styleVariants.stream().filter(sv -> styleVariantId.equals(sv.getId())).findFirst().orElse(null);
	}

}
