package puc.plm.service.domains.style.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.domains.style.StyleCommandService;
import puc.plm.service.domains.style.command.commands.AddStyleVariantCommand;
import puc.plm.service.domains.style.command.commands.CreateStyleCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleDataCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleVariantDataCommand;
import puc.plm.service.domains.style.command.repo.StyleAggregateRepo;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

@Component
@Transactional
public class StyleCommandServiceImpl implements StyleCommandService {

	@Autowired
	StyleAggregateRepo aggregateRepo;

	@Override
	public void createStyle(CreateStyleCommand command) {
		StyleId styleId = command.getStyleId();
		aggregateRepo.apply(styleId, a -> a.createStyle(command));
	}

	@Override
	public StyleVariantId addStyleVariantToStyle(AddStyleVariantCommand command) {
		StyleId styleId = command.getStyleId();
		return aggregateRepo.applyWithResult(styleId, a -> a.addStyleVariant(command));
	}

	@Override
	public void updateStyleData(UpdateStyleDataCommand command) {
		StyleId styleId = command.getStyleId();
		aggregateRepo.apply(styleId, a -> a.handle(command));
	}

	@Override
	public void updateStyleVariantData(UpdateStyleVariantDataCommand command) {
		StyleVariantId styleVariantId = command.getStyleVariantId();
		StyleId styleId = aggregateRepo.lookupStyleId(styleVariantId);
		aggregateRepo.apply(styleId, a -> a.handle(command));
	}

}
