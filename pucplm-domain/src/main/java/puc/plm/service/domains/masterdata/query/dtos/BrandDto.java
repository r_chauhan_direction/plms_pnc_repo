package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.BrandId;

@Entity
public class BrandDto extends ActiveEntity<BrandId> {

	/**
	 * code used by users for fast retrieval
	 */
	private Integer code;

	/**
	 * the brand name
	 */
	private String name;

	/**
	 * name of the brand's family
	 */
	private String brandFamilyName;

	protected BrandDto() {}

	public BrandDto(BrandId id, Integer code, String name, String brandFamilyName) {
		super(id);
		this.code = code;
		this.name = name;
		this.brandFamilyName = brandFamilyName;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getBrandFamilyName() {
		return brandFamilyName;
	}
}
