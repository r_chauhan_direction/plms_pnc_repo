package puc.plm.service.domains.customerstyle.command;

import java.time.Instant;

import javax.persistence.Entity;
import javax.persistence.Id;

import puc.plm.service.domains.customerstyle.ids.CustomerStyleId;
import puc.plm.service.domains.style.ids.StyleId;

@Entity
public class CustomerStyle {

	private CustomerStyleId id;

	private Instant instant = Instant.now();

	private StyleId friendStyle;

	private StyleId friendStyle2;

	public CustomerStyle() {}

	@Id
	public CustomerStyleId getId() {
		return id;
	}

	protected void setId(CustomerStyleId id) {
		this.id = id;
	}

	public Instant getInstant() {
		return instant;
	}

	public void setInstant(Instant instant) {
		this.instant = instant;
	}

	public StyleId getFriendStyle2() {
		return friendStyle2;
	}

	public void setFriendStyle2(StyleId friendStyle2) {
		this.friendStyle2 = friendStyle2;
	}

	public StyleId getFriendStyle() {
		return friendStyle;
	}

	public void setFriendStyle(StyleId friendStyle) {
		this.friendStyle = friendStyle;
	}
}
