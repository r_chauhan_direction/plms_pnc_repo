package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.SeasonId;

@Entity
public class SeasonDto extends ActiveEntity<SeasonId> {

	/**
	 * code used by users for fast retrieval
	 */
	private Integer code;

	/**
	 * the season name
	 */
	private String name;

	protected SeasonDto() {}

	public SeasonDto(SeasonId id, Integer code, String name) {
		super(id);
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
