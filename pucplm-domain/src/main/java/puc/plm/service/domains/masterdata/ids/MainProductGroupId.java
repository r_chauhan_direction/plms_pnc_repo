package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class MainProductGroupId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected MainProductGroupId() {
		super();
	}

	public MainProductGroupId(Integer value) {
		super(value);
	}
}
