package puc.plm.service.domains.customerstyle.command.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.customerstyle.command.CustomerStyle;
import puc.plm.service.domains.customerstyle.ids.CustomerStyleId;

public interface CustomerStyleRepo extends JpaRepository<CustomerStyle, CustomerStyleId> {

}
