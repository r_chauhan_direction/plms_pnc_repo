package puc.plm.service.domains.style.data;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.StringValue;

/**
 * unique key used for identifying a Style by users
 */
@Embeddable
public class StyleNumber extends StringValue {

	private static final long serialVersionUID = 1L;

	protected StyleNumber() {
		super();
	}

	public StyleNumber(String value) {
		super(value);
	}
}
