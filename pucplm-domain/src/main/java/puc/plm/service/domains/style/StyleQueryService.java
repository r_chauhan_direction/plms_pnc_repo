package puc.plm.service.domains.style;

import puc.plm.service.api.ApiStyleQueryService;

/**
 * Service for querying styles
 */
public interface StyleQueryService extends ApiStyleQueryService {

}
