package puc.plm.service.domains.customerstyle.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class CustomerStyleColorId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public CustomerStyleColorId() {
		super();
	}

	public CustomerStyleColorId(UUID value) {
		super(value);
	}
}
