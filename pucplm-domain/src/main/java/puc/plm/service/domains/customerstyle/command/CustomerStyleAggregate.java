package puc.plm.service.domains.customerstyle.command;

import puc.plm.service.common.aggregate.Aggregate;
import puc.plm.service.domains.customerstyle.ids.CustomerStyleId;

public class CustomerStyleAggregate extends Aggregate<CustomerStyle, CustomerStyleId> {

	public CustomerStyleAggregate(CustomerStyleId id, CustomerStyle rootEntity) {
		super(id, rootEntity);
	}

	public void doBla() {

		root = new CustomerStyle();
		root.setId(id);

		System.out.println("bla");
	}

	@Override
	protected void mapEvents() {
		// TODO Auto-generated method stub

	}

}
