package puc.plm.service.domains.supply.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class SupplierStyleId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public SupplierStyleId() {
		super();
	}

	public SupplierStyleId(UUID value) {
		super(value);
	}
}
