package puc.plm.service.domains.masterdata.query;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.common.caching.EntityCache;
import puc.plm.service.common.comparators.LevenshteinComparator;
import puc.plm.service.domains.masterdata.MasterdataQueryService;
import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;
import puc.plm.service.domains.masterdata.ids.AssortmentSectionId;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.masterdata.ids.SeasonId;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentSectionDto;
import puc.plm.service.domains.masterdata.query.dtos.BrandDto;
import puc.plm.service.domains.masterdata.query.dtos.MainProductGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.ProductGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.SeasonDto;
import puc.plm.service.domains.masterdata.query.repos.AssortmentGroupDtoRepo;
import puc.plm.service.domains.masterdata.query.repos.AssortmentSectionDtoRepo;
import puc.plm.service.domains.masterdata.query.repos.BrandDtoRepo;
import puc.plm.service.domains.masterdata.query.repos.MainProductGroupDtoRepo;
import puc.plm.service.domains.masterdata.query.repos.ProductGroupDtoRepo;
import puc.plm.service.domains.masterdata.query.repos.SeasonDtoRepo;

@Component
@Transactional(readOnly = true)
public class MasterdataQueryServiceImpl implements MasterdataQueryService {

	private static final Duration FOUR_HOURS = Duration.ofHours(4);

	private AssortmentGroupDtoRepo assortmentGroupDtoRepo;

	private BrandDtoRepo brandDtoRepo;

	private MainProductGroupDtoRepo mainProductGroupDtoRepo;

	private ProductGroupDtoRepo productGroupDtoRepo;

	private SeasonDtoRepo seasonDtoRepo;

	private AssortmentSectionDtoRepo assortmentSectionDtoRepo;

	private EntityCache<AssortmentGroupDto, AssortmentGroupId> assortmentGroupDtoCache;

	private EntityCache<BrandDto, BrandId> brandDtoCache;

	private EntityCache<MainProductGroupDto, MainProductGroupId> mainProductGroupDtoCache;

	private EntityCache<ProductGroupDto, ProductGroupId> productGroupDtoCache;

	private EntityCache<SeasonDto, SeasonId> seasonDtoCache;

	private EntityCache<AssortmentSectionDto, AssortmentSectionId> assortmentSectionDtoCache;

	public MasterdataQueryServiceImpl(AssortmentGroupDtoRepo assortmentGroupDtoRepo, BrandDtoRepo brandDtoRepo,
			MainProductGroupDtoRepo mainProductGroupDtoRepo, ProductGroupDtoRepo productGroupDtoRepo,
			SeasonDtoRepo seasonDtoRepo, AssortmentSectionDtoRepo assortmentSectionDtoRepo) {
		super();
		this.assortmentGroupDtoRepo = assortmentGroupDtoRepo;
		this.brandDtoRepo = brandDtoRepo;
		this.mainProductGroupDtoRepo = mainProductGroupDtoRepo;
		this.productGroupDtoRepo = productGroupDtoRepo;
		this.seasonDtoRepo = seasonDtoRepo;
		this.assortmentSectionDtoRepo = assortmentSectionDtoRepo;

		this.assortmentGroupDtoCache = new EntityCache<AssortmentGroupDto, AssortmentGroupId>(FOUR_HOURS,
				() -> this.assortmentGroupDtoRepo.findAll());

		this.brandDtoCache = new EntityCache<BrandDto, BrandId>(FOUR_HOURS, () -> this.brandDtoRepo.findAll());

		this.mainProductGroupDtoCache = new EntityCache<MainProductGroupDto, MainProductGroupId>(FOUR_HOURS,
				() -> this.mainProductGroupDtoRepo.findAll());

		this.productGroupDtoCache = new EntityCache<ProductGroupDto, ProductGroupId>(FOUR_HOURS,
				() -> this.productGroupDtoRepo.findAll());

		this.seasonDtoCache = new EntityCache<SeasonDto, SeasonId>(FOUR_HOURS, () -> this.seasonDtoRepo.findAll());

		this.assortmentSectionDtoCache = new EntityCache<AssortmentSectionDto, AssortmentSectionId>(FOUR_HOURS,
				() -> this.assortmentSectionDtoRepo.findAll());
	}

	@Override
	public List<AssortmentGroupDto> getAllAssortmentGroups() {
		return assortmentGroupDtoCache.findAll();
	}

	@Override
	public List<AssortmentGroupDto> getActiveAssortmentGroups() {
		return assortmentGroupDtoCache.findAllByFilter(AssortmentGroupDto::isActive);
	}

	@Override
	public AssortmentGroupDto getAssortmentGroup(AssortmentGroupId assortmentGroupId) {
		return assortmentGroupDtoCache.findById(assortmentGroupId);
	}

	@Override
	public List<MainProductGroupDto> getAllMainProductGroups() {
		return mainProductGroupDtoCache.findAll();
	}

	@Override
	public List<MainProductGroupDto> getActiveMainProductGroups() {
		return mainProductGroupDtoCache.findAllByFilter(MainProductGroupDto::isActive);
	}

	@Override
	public MainProductGroupDto getMainProductGroup(MainProductGroupId mainProductGroupId) {
		return mainProductGroupDtoCache.findById(mainProductGroupId);
	}

	@Override
	public MainProductGroupDto getMainProductGroupForProductGroup(ProductGroupId productGroupId) {
		ProductGroupDto productGroupDto = productGroupDtoCache.findById(productGroupId);
		return mainProductGroupDtoCache.findById(productGroupDto.getMainProductGroup());
	}

	@Override
	public List<ProductGroupDto> getAllProductGroupsForMainProductGroup(MainProductGroupId mainProductGroupId) {
		return productGroupDtoCache.findAllByFilter(pg -> pg.getMainProductGroup().equals(mainProductGroupId));
	}

	@Override
	public List<ProductGroupDto> getActiveProductGroupsForMainProductGroup(MainProductGroupId mainProductGroupId) {
		return productGroupDtoCache
				.findAllByFilter(pg -> pg.isActive() && pg.getMainProductGroup().equals(mainProductGroupId));
	}

	@Override
	public ProductGroupDto getProductGroup(ProductGroupId productGroupId) {
		return productGroupDtoCache.findById(productGroupId);
	}

	@Override
	public List<BrandDto> searchBrandCandidatesForString(String queryString, int maxResults) {
		List<BrandDto> allBrands = brandDtoCache.findAll();

		Function<BrandDto, String> intStringExtractor = s -> s.getCode().toString();
		Function<BrandDto, String> stringExtractor = BrandDto::getName;

		return LevenshteinComparator.getBestMatching(queryString, allBrands, maxResults, intStringExtractor,
				stringExtractor);
	}

	@Override
	public BrandDto getBrand(BrandId brandId) {
		return brandDtoCache.findById(brandId);
	}

	@Override
	public List<SeasonDto> getAllSeasons() {
		return seasonDtoCache.findAll();
	}

	@Override
	public List<SeasonDto> getActiveSeasons() {
		return seasonDtoCache.findAllByFilter(SeasonDto::isActive);
	}

	@Override
	public SeasonDto getSeason(SeasonId seasonId) {
		return seasonDtoCache.findById(seasonId);
	}

	@Override
	public List<AssortmentSectionDto> getAllAssortmentSections() {
		return assortmentSectionDtoCache.findAll();
	}

	@Override
	public List<AssortmentSectionDto> getActiveAssortmentSections() {
		return assortmentSectionDtoCache.findAllByFilter(AssortmentSectionDto::isActive);
	}

	@Override
	public AssortmentSectionDto getAssortmentSection(AssortmentSectionId assortmentSectionId) {
		return assortmentSectionDtoCache.findById(assortmentSectionId);
	}

}
