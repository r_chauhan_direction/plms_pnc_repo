package puc.plm.service.domains.style.command.events;

import puc.plm.service.common.messages.Event;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

/**
 * Event, signaling an update of a style variants's {@link StyleVariantData}.
 */
public class StyleVariantDataUpdatedEvent implements Event {

	private StyleId styleId;

	private StyleVariantId styleVariantId;

	private StyleVariantData data;

	public StyleVariantDataUpdatedEvent(StyleId styleId, StyleVariantId styleVariantId, StyleVariantData data) {
		super();
		this.styleId = styleId;
		this.styleVariantId = styleVariantId;
		this.data = data;
	}

	public StyleVariantId getStyleVariantId() {
		return styleVariantId;
	}

	public StyleVariantData getData() {
		return data;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		result = prime * result + ((styleVariantId == null) ? 0 : styleVariantId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StyleVariantDataUpdatedEvent other = (StyleVariantDataUpdatedEvent) obj;
		if (data == null) {
			if (other.data != null) return false;
		} else if (!data.equals(other.data)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		if (styleVariantId == null) {
			if (other.styleVariantId != null) return false;
		} else if (!styleVariantId.equals(other.styleVariantId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "StyleVariantDataUpdatedEvent [styleId=" + styleId + ", styleVariantId=" + styleVariantId + ", data="
				+ data + "]";
	}
}
