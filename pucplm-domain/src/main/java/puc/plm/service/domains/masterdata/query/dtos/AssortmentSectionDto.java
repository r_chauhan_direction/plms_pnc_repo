package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.AssortmentSectionId;

@Entity
public class AssortmentSectionDto extends ActiveEntity<AssortmentSectionId> {

	/**
	 * code used by user for fast retrieval
	 */
	private Integer code;

	/**
	 * name of the assortment section
	 */
	private String name;

	/**
	 * cost center used for the assorment section
	 */
	private Integer costCenter;

	/**
	 * name of the buyer managing the assortment section
	 */
	private String buyer;

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public Integer getCostCenter() {
		return costCenter;
	}

	public String getBuyer() {
		return buyer;
	}
}
