package puc.plm.service.domains.style.command.commands;

import java.util.Optional;

import puc.plm.service.common.messages.Command;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.ids.StyleId;

/**
 * Command to create a new style
 */
public class CreateStyleCommand implements Command {

	private StyleId styleId;

	private ProductGroupId productGroupId;

	private BrandId brandId;

	private Optional<StyleData> styleData;

	public CreateStyleCommand(StyleId styleId, ProductGroupId productGroupId, BrandId brandId,
			Optional<StyleData> styleData) {
		super();
		this.styleId = styleId;
		this.productGroupId = productGroupId;
		this.brandId = brandId;
		this.styleData = styleData;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public Optional<StyleData> getStyleData() {
		return styleData;
	}

	public ProductGroupId getProductGroupId() {
		return productGroupId;
	}

	public BrandId getBrandId() {
		return brandId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brandId == null) ? 0 : brandId.hashCode());
		result = prime * result + ((productGroupId == null) ? 0 : productGroupId.hashCode());
		result = prime * result + ((styleData == null) ? 0 : styleData.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		CreateStyleCommand other = (CreateStyleCommand) obj;
		if (brandId == null) {
			if (other.brandId != null) return false;
		} else if (!brandId.equals(other.brandId)) return false;
		if (productGroupId == null) {
			if (other.productGroupId != null) return false;
		} else if (!productGroupId.equals(other.productGroupId)) return false;
		if (styleData == null) {
			if (other.styleData != null) return false;
		} else if (!styleData.equals(other.styleData)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "CreateStyleCommand [styleId=" + styleId + ", productGroupId=" + productGroupId + ", brandId=" + brandId
				+ ", styleData=" + styleData + "]";
	}
}
