package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class AssortmentGroupId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected AssortmentGroupId() {
		super();
	}

	public AssortmentGroupId(Integer value) {
		super(value);
	}
}
