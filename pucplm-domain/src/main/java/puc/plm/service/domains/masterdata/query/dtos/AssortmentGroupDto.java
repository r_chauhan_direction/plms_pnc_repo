package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;

@Entity
public class AssortmentGroupDto extends ActiveEntity<AssortmentGroupId> {

	/**
	 * code used by user for fast retrieval
	 */
	private Integer code;

	/**
	 * name of the assortment group
	 */
	private String name;

	protected AssortmentGroupDto() {}

	public AssortmentGroupDto(AssortmentGroupId id, Integer code, String name) {
		super(id);
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
