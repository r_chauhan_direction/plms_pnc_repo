package puc.plm.service.domains.style.command.events;

import puc.plm.service.common.messages.Event;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.ids.StyleId;

/**
 * Event, signaling an update of a style's {@link StyleData}.
 */
public class StyleDataUpdatedEvent implements Event {

	private StyleId styleId;

	private StyleData data;

	public StyleDataUpdatedEvent(StyleId styleId, StyleData data) {
		super();
		this.styleId = styleId;
		this.data = data;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public StyleData getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StyleDataUpdatedEvent other = (StyleDataUpdatedEvent) obj;
		if (data == null) {
			if (other.data != null) return false;
		} else if (!data.equals(other.data)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateStyleDataCommand [styleId=" + styleId + ", data=" + data + "]";
	}
}
