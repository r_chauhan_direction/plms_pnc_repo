package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.SeasonId;
import puc.plm.service.domains.masterdata.query.dtos.SeasonDto;

public interface SeasonDtoRepo extends JpaRepository<SeasonDto, SeasonId> {

}
