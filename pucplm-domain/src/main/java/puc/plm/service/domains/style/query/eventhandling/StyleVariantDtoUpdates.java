package puc.plm.service.domains.style.query.eventhandling;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.common.eventhandling.Subscribes;
import puc.plm.service.domains.style.command.events.StyleVariantAddedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantDataUpdatedEvent;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;
import puc.plm.service.domains.style.query.dtos.StyleVariantDto;
import puc.plm.service.domains.style.query.repos.StyleVariantDtoRepo;

/**
 * Handles events relevant to a {@link StyleVariantDto}'s state.
 */
@Component
@Transactional
public class StyleVariantDtoUpdates {

	@Autowired
	StyleVariantDtoRepo styleVariantDtoRepo;

	@Subscribes
	public void onStyleVariantAdded(StyleVariantAddedEvent evt) {
		StyleId styleId = evt.getStyleId();
		StyleVariantId styleVariantId = evt.getStyleVariantId();

		StyleVariantDto styleVariantDto = new StyleVariantDto(styleVariantId, styleId);
		styleVariantDto.setData(evt.getStyleVariantData());

		styleVariantDtoRepo.save(styleVariantDto);
	}

	@Subscribes
	public void onStyleVariantDataUpdated(StyleVariantDataUpdatedEvent evt) {
		StyleVariantId styleVariantId = evt.getStyleVariantId();
		StyleVariantDto styleVairantDto = getStyleVariantDto(styleVariantId);
		styleVairantDto.setData(evt.getData());
		styleVariantDtoRepo.save(styleVairantDto);
	}

	private StyleVariantDto getStyleVariantDto(StyleVariantId styleVariantId) {
		Optional<StyleVariantDto> styleVariantDtoOpt = styleVariantDtoRepo.findById(styleVariantId);
		StyleVariantDto styleVariantDto = styleVariantDtoOpt
				.orElseThrow(() -> new IllegalArgumentException("no StyleVariantDto found for " + styleVariantId));
		return styleVariantDto;
	}
}
