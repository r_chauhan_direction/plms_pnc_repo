package puc.plm.service.domains.style.command;

import puc.plm.service.common.aggregate.Aggregate;
import puc.plm.service.common.time.Time;
import puc.plm.service.domains.style.command.commands.AddStyleVariantCommand;
import puc.plm.service.domains.style.command.commands.CreateStyleCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleDataCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleVariantDataCommand;
import puc.plm.service.domains.style.command.events.StyleCreatedEvent;
import puc.plm.service.domains.style.command.events.StyleDataUpdatedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantAddedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantDataUpdatedEvent;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.exeptions.StyleVariantNotExistingException;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

public class StyleAggregate extends Aggregate<Style, StyleId> {

	public StyleAggregate(StyleId id, Style rootEntity) {
		super(id, rootEntity);
	}

	@Override
	protected void mapEvents() {
		map(StyleCreatedEvent.class, this::onStyleCreated);
		map(StyleVariantAddedEvent.class, this::onStyleVariantAdded);
		map(StyleDataUpdatedEvent.class, this::onStyleDataUpdated);
		map(StyleVariantDataUpdatedEvent.class, this::onStyleVariantDataUpdated);
	}

	public void createStyle(CreateStyleCommand cmd) {
		checkUninitialized("can't create style for existing style with id " + id);

		StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(id, cmd.getProductGroupId(), cmd.getBrandId(),
				cmd.getStyleData().orElse(new StyleData()), Time.now());

		apply(styleCreatedEvent);
	}

	protected void onStyleCreated(StyleCreatedEvent evt) {
		Style style = new Style(evt.getStyleId());
		style.setProductGroupId(evt.getProductGroupId());
		style.setBrandId(evt.getBrandId());
		style.setCreationTime(evt.getCreationTime());

		root = style;
	}

	public StyleVariantId addStyleVariant(AddStyleVariantCommand cmd) {
		checkInitialized("can't add style variant for non-existing style with id " + id);

		StyleVariantId styleVariantId = new StyleVariantId();
		StyleVariantAddedEvent styleVariantAddedEvent = new StyleVariantAddedEvent(cmd.getStyleId(), styleVariantId,
				cmd.getStyleVariantData().orElse(new StyleVariantData()), Time.now());

		apply(styleVariantAddedEvent);

		return styleVariantId;
	}

	protected void onStyleVariantAdded(StyleVariantAddedEvent evt) {
		StyleVariant variant = new StyleVariant(evt.getStyleVariantId());

		root.addStyleVariant(variant);
	}

	public void handle(UpdateStyleDataCommand cmd) {
		checkInitialized("can't update StyleData for non-existing style with id " + id);

		StyleId styleId = cmd.getStyleId();
		StyleData styleData = cmd.getData();
		apply(new StyleDataUpdatedEvent(styleId, styleData));
	}

	protected void onStyleDataUpdated(StyleDataUpdatedEvent evt) {
		// do nothing
	}

	public void handle(UpdateStyleVariantDataCommand cmd) {
		checkInitialized("can't update StyleVariantData for non-existing style with id " + id);

		StyleVariantId styleVariantId = cmd.getStyleVariantId();
		StyleVariant styleVariant = root.getStyleVariant(styleVariantId);
		if (styleVariant == null) {
			String message = "can't update StyleVariantData for non-existing style variant with id " + styleVariantId;
			logger.error(message);
			throw new StyleVariantNotExistingException(message);
		}

		StyleVariantData styleVariantData = cmd.getData();
		apply(new StyleVariantDataUpdatedEvent(getId(), styleVariantId, styleVariantData));
	}

	protected void onStyleVariantDataUpdated(StyleVariantDataUpdatedEvent evt) {
		// do nothing
	}

}
