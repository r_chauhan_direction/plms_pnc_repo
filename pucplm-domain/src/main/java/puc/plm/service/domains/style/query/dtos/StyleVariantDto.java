package puc.plm.service.domains.style.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.VersionedEntity;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

@Entity
public class StyleVariantDto extends VersionedEntity<StyleVariantId> {

	private StyleId styleId;

	private StyleVariantData data;

	public StyleVariantDto() {}

	public StyleVariantDto(StyleVariantId styleVariantId, StyleId styleId) {
		super(styleVariantId);
		this.styleId = styleId;
	}

	public StyleVariantData getData() {
		return data;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public void setData(StyleVariantData data) {
		this.data = data;
	}

}
