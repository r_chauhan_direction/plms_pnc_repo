package puc.plm.service.domains.style.query.queries;

import java.util.Objects;
import java.util.Optional;

import puc.plm.service.common.messages.Query;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.data.StyleNumber;

/**
 * Query for styles by different critiera. All fields must not be null.
 */
public class StylesByCriteriaQuery implements Query {

	private Optional<StyleNumber> styleNumber = Optional.empty();

	private Optional<BrandId> brandId = Optional.empty();

	private Optional<MainProductGroupId> mainProductGroupId = Optional.empty();

	private Optional<ProductGroupId> productGroupId = Optional.empty();

	public Optional<StyleNumber> getStyleNumber() {
		return styleNumber;
	}

	public void setStyleNumber(Optional<StyleNumber> styleNumber) {
		this.styleNumber = styleNumber;
	}

	public Optional<BrandId> getBrandId() {
		return brandId;
	}

	public void setBrandId(Optional<BrandId> brandId) {
		this.brandId = brandId;
	}

	public Optional<MainProductGroupId> getMainProductGroupId() {
		return mainProductGroupId;
	}

	public void setMainProductGroupId(Optional<MainProductGroupId> mainProductGroupId) {
		this.mainProductGroupId = mainProductGroupId;
	}

	public Optional<ProductGroupId> getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(Optional<ProductGroupId> productGroupId) {
		this.productGroupId = productGroupId;
	}

	@Override
	public int hashCode() {
		return Objects.hash(brandId, mainProductGroupId, productGroupId, styleNumber);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StylesByCriteriaQuery other = (StylesByCriteriaQuery) obj;
		return Objects.equals(brandId, other.brandId) && Objects.equals(mainProductGroupId, other.mainProductGroupId)
				&& Objects.equals(productGroupId, other.productGroupId)
				&& Objects.equals(styleNumber, other.styleNumber);
	}
}
