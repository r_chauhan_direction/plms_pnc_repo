package puc.plm.service.domains.planning.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class PlanPeriodThemeId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public PlanPeriodThemeId() {
		super();
	}

	public PlanPeriodThemeId(UUID value) {
		super(value);
	}
}
