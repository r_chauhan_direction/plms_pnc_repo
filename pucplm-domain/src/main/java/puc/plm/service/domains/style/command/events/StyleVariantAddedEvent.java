package puc.plm.service.domains.style.command.events;

import java.time.Instant;

import puc.plm.service.common.messages.Event;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

/**
 * Event, signaling an addes style variant to a style
 */
public class StyleVariantAddedEvent implements Event {

	private StyleId styleId;

	private StyleVariantId styleVariantId;

	private StyleVariantData styleVariantData;

	private Instant addingTime;

	public StyleVariantAddedEvent(StyleId styleId, StyleVariantId styleVariantId, StyleVariantData styleVariantData,
			Instant addingTime) {
		super();
		this.styleId = styleId;
		this.styleVariantId = styleVariantId;
		this.styleVariantData = styleVariantData;
		this.addingTime = addingTime;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public StyleVariantId getStyleVariantId() {
		return styleVariantId;
	}

	public StyleVariantData getStyleVariantData() {
		return styleVariantData;
	}

	public Instant getAddingTime() {
		return addingTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addingTime == null) ? 0 : addingTime.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		result = prime * result + ((styleVariantData == null) ? 0 : styleVariantData.hashCode());
		result = prime * result + ((styleVariantId == null) ? 0 : styleVariantId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StyleVariantAddedEvent other = (StyleVariantAddedEvent) obj;
		if (addingTime == null) {
			if (other.addingTime != null) return false;
		} else if (!addingTime.equals(other.addingTime)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		if (styleVariantData == null) {
			if (other.styleVariantData != null) return false;
		} else if (!styleVariantData.equals(other.styleVariantData)) return false;
		if (styleVariantId == null) {
			if (other.styleVariantId != null) return false;
		} else if (!styleVariantId.equals(other.styleVariantId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "StyleVariantAddedEvent [styleId=" + styleId + ", styleVariantId=" + styleVariantId
				+ ", styleVariantData=" + styleVariantData + ", addingTime=" + addingTime + "]";
	}
}
