package puc.plm.service.domains.style.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.query.dtos.StyleDto;

public interface StyleDtoRepo extends JpaRepository<StyleDto, StyleId> {

}
