package puc.plm.service.domains.planning.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.planning.ids.PlanPeriodId;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodDto;

public interface PlanPeriodDtoRepo extends JpaRepository<PlanPeriodDto, PlanPeriodId> {

}
