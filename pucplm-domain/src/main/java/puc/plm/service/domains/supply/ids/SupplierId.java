package puc.plm.service.domains.supply.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class SupplierId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected SupplierId() {
		super();
	}

	public SupplierId(Integer value) {
		super(value);
	}
}
