package puc.plm.service.domains.style.query.eventhandling;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.common.eventhandling.Subscribes;
import puc.plm.service.domains.masterdata.MasterdataQueryService;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.masterdata.query.dtos.MainProductGroupDto;
import puc.plm.service.domains.style.command.events.StyleCreatedEvent;
import puc.plm.service.domains.style.command.events.StyleDataUpdatedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantAddedEvent;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.data.StyleNumber;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.query.dtos.StyleDto;
import puc.plm.service.domains.style.query.repos.StyleDtoRepo;

/**
 * Handles events relevant to a {@link StyleDto}'s state.
 */
@Component
@Transactional
public class StyleDtoUpdates {

	@Autowired
	StyleDtoRepo styleDtoRepo;

	@Autowired
	MasterdataQueryService masterDataQuerySerivce;

	@Subscribes
	public void onStyleCreated(StyleCreatedEvent evt) {
		StyleDto styleDto = new StyleDto(evt.getStyleId());

		StyleNumber styleNumber = new StyleNumber(""); // TODO
		BrandId brandId = evt.getBrandId();
		ProductGroupId productGroupId = evt.getProductGroupId();
		MainProductGroupDto mainProductGroupDto = masterDataQuerySerivce
				.getMainProductGroupForProductGroup(productGroupId);
		StyleData styleData = evt.getStyleData();

		styleDto.setStyleNumber(styleNumber);
		styleDto.setBrandId(brandId);
		styleDto.setProductGroupId(productGroupId);
		styleDto.setMainProductGroupId(mainProductGroupDto.getId());
		styleDto.setNumberOfStyleVariants(0);
		styleDto.setData(styleData);

		styleDtoRepo.save(styleDto);
	}

	@Subscribes
	public void onStyleDataUpdated(StyleDataUpdatedEvent evt) {
		StyleId styleId = evt.getStyleId();
		StyleDto styleDto = getStyleDto(styleId);
		styleDto.setData(evt.getData());
		styleDtoRepo.save(styleDto);
	}

	@Subscribes
	public void onStyleVariantAdded(StyleVariantAddedEvent evt) {
		StyleId styleId = evt.getStyleId();
		StyleDto styleDto = getStyleDto(styleId);
		styleDto.setNumberOfStyleVariants(styleDto.getNumberOfStyleVariants() + 1);
		styleDtoRepo.save(styleDto);
	}

	private StyleDto getStyleDto(StyleId styleId) {
		Optional<StyleDto> styleDtoOpt = styleDtoRepo.findById(styleId);
		StyleDto styleDto = styleDtoOpt
				.orElseThrow(() -> new IllegalArgumentException("no StyleDto found for " + styleId));
		return styleDto;
	}
}
