package puc.plm.service.domains.supply.query;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.common.caching.EntityCache;
import puc.plm.service.common.comparators.LevenshteinComparator;
import puc.plm.service.domains.supply.SupplyQuerySerivce;
import puc.plm.service.domains.supply.ids.SupplierId;
import puc.plm.service.domains.supply.query.dtos.SupplierDto;
import puc.plm.service.domains.supply.query.repos.SupplierDtoRepo;

@Component
@Transactional(readOnly = true)
public class SupplyQuerySerivceImpl implements SupplyQuerySerivce {

	private static final Duration FOUR_HOURS = Duration.ofHours(4);

	private SupplierDtoRepo supplierDtoRepo;

	private EntityCache<SupplierDto, SupplierId> supplierDtoCache;

	public SupplyQuerySerivceImpl(SupplierDtoRepo supplierDtoRepo) {
		super();
		this.supplierDtoRepo = supplierDtoRepo;

		this.supplierDtoCache = new EntityCache<SupplierDto, SupplierId>(FOUR_HOURS,
				() -> this.supplierDtoRepo.findAll());
	}

	@Override
	public List<SupplierDto> searchSupplierCandidatesForString(String queryString, int maxResults) {
		List<SupplierDto> allSuppliers = supplierDtoCache.findAll();

		Function<SupplierDto, String> intStringExtractor = s -> s.getSupplierNumber().toString();
		Function<SupplierDto, String> stringExtractor = s -> s.getName();

		return LevenshteinComparator.getBestMatching(queryString, allSuppliers, maxResults, intStringExtractor,
				stringExtractor);
	}

	@Override
	public SupplierDto getSupplier(SupplierId supplierId) {
		return supplierDtoCache.findById(supplierId);
	}

}
