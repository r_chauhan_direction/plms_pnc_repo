package puc.plm.service.domains.style.command;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import puc.plm.service.domains.style.ids.StyleVariantId;

@Entity
public class StyleVariant {

	@Id
	private StyleVariantId id;

	@ManyToOne
	private Style style;

	protected StyleVariant() {}

	public StyleVariant(StyleVariantId id) {
		super();
		this.id = id;
	}

	public StyleVariantId getId() {
		return id;
	}

	public Style getStyle() {
		return style;
	}

	protected void setStyle(Style style) {
		this.style = style;
	}
}
