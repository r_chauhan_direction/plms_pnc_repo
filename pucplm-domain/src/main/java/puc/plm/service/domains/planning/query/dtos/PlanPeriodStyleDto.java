package puc.plm.service.domains.planning.query.dtos;

import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.planning.ids.PlanPeriodStyleId;
import puc.plm.service.domains.planning.ids.PlanPeriodThemeId;

public class PlanPeriodStyleDto {

	/**
	 * ID of the plan period style
	 */
	private PlanPeriodStyleId id;

	/**
	 * plan period theme the pp-style is associated with
	 */
	private PlanPeriodThemeId planPeriodTheme;

	/**
	 * product group the style is associated with
	 */
	private ProductGroupId productGroup;

	public PlanPeriodStyleDto(PlanPeriodStyleId id) {
		super();
		this.id = id;
	}

	public PlanPeriodStyleId getId() {
		return id;
	}

	protected void setId(PlanPeriodStyleId id) {
		this.id = id;
	}

	public PlanPeriodThemeId getPlanPeriodTheme() {
		return planPeriodTheme;
	}

	public void setPlanPeriodTheme(PlanPeriodThemeId planPeriodTheme) {
		this.planPeriodTheme = planPeriodTheme;
	}

	public ProductGroupId getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(ProductGroupId productGroup) {
		this.productGroup = productGroup;
	}
}
