package puc.plm.service.domains.masterdata.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.ActiveEntity;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;

@Entity
public class ProductGroupDto extends ActiveEntity<ProductGroupId> {

	/**
	 * the associated main product group
	 */
	private MainProductGroupId mainProductGroup;

	/**
	 * code used by users for fast retrieval
	 */
	private Integer code;

	/**
	 * name of the product group
	 */
	private String name;

	protected ProductGroupDto() {}

	public ProductGroupDto(ProductGroupId id, MainProductGroupId mainProductGroup, Integer code, String name) {
		super(id);
		this.mainProductGroup = mainProductGroup;
		this.code = code;
		this.name = name;
	}

	public ProductGroupId getId() {
		return id;
	}

	public MainProductGroupId getMainProductGroup() {
		return mainProductGroup;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
