package puc.plm.service.domains.size.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class SizeDimensionId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public SizeDimensionId() {
		super();
	}

	public SizeDimensionId(UUID value) {
		super(value);
	}
}
