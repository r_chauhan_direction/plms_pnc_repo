package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.AssortmentSectionId;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentSectionDto;

public interface AssortmentSectionDtoRepo extends JpaRepository<AssortmentSectionDto, AssortmentSectionId> {

}
