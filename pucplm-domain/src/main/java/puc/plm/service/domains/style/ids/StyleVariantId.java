package puc.plm.service.domains.style.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class StyleVariantId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public StyleVariantId() {
		super();
	}

	public StyleVariantId(UUID value) {
		super(value);
	}
}
