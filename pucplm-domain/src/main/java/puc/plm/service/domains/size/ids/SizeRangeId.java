package puc.plm.service.domains.size.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class SizeRangeId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public SizeRangeId() {
		super();
	}

	public SizeRangeId(UUID value) {
		super(value);
	}
}
