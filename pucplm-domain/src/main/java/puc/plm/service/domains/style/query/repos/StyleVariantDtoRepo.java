package puc.plm.service.domains.style.query.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;
import puc.plm.service.domains.style.query.dtos.StyleVariantDto;

public interface StyleVariantDtoRepo extends JpaRepository<StyleVariantDto, StyleVariantId> {

	List<StyleVariantDto> findByStyleId(StyleId styleId);

}
