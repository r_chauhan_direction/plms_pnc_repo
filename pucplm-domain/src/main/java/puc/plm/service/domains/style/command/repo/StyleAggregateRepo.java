package puc.plm.service.domains.style.command.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import puc.plm.service.common.aggregate.AggregateRepo;
import puc.plm.service.domains.style.command.Style;
import puc.plm.service.domains.style.command.StyleAggregate;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

@Component
public class StyleAggregateRepo extends AggregateRepo<StyleAggregate, Style, StyleId> {

	@Autowired
	StyleRepo styleRepo;

	@Override
	protected StyleAggregate constructAggregate(StyleId aggregateId, Style root) {
		return new StyleAggregate(aggregateId, root);
	}

	public StyleId lookupStyleId(StyleVariantId styleVariantId) {
		return styleRepo.lookupStyleId(styleVariantId);
	}

}
