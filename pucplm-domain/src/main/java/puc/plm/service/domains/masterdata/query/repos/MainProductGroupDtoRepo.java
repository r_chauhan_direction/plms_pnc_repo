package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.query.dtos.MainProductGroupDto;

public interface MainProductGroupDtoRepo extends JpaRepository<MainProductGroupDto, MainProductGroupId> {

}
