package puc.plm.service.domains.style.command.commands;

import puc.plm.service.common.messages.Command;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleVariantId;

/**
 * Command to update a style variants's {@link StyleVariantData}.
 */
public class UpdateStyleVariantDataCommand implements Command {

	private StyleVariantId styleVariantId;

	private StyleVariantData data;

	public UpdateStyleVariantDataCommand(StyleVariantId styleVariantId, StyleVariantData data) {
		super();
		this.styleVariantId = styleVariantId;
		this.data = data;
	}

	public StyleVariantId getStyleVariantId() {
		return styleVariantId;
	}

	public StyleVariantData getData() {
		return data;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((styleVariantId == null) ? 0 : styleVariantId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		UpdateStyleVariantDataCommand other = (UpdateStyleVariantDataCommand) obj;
		if (data == null) {
			if (other.data != null) return false;
		} else if (!data.equals(other.data)) return false;
		if (styleVariantId == null) {
			if (other.styleVariantId != null) return false;
		} else if (!styleVariantId.equals(other.styleVariantId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "UpdateStyleVariantDataCommand [styleVariantId=" + styleVariantId + ", data=" + data + "]";
	}

}
