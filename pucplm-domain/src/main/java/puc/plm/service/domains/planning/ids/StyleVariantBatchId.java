package puc.plm.service.domains.planning.ids;

import java.util.UUID;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.UUIDValue;

@Embeddable
public class StyleVariantBatchId extends UUIDValue {

	private static final long serialVersionUID = 1L;

	public StyleVariantBatchId() {
		super();
	}

	public StyleVariantBatchId(UUID value) {
		super(value);
	}
}
