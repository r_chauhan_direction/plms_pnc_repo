package puc.plm.service.domains.style.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.VersionedEntity;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.data.StyleNumber;
import puc.plm.service.domains.style.ids.StyleId;

@Entity
public class StyleDto extends VersionedEntity<StyleId> {

	private StyleNumber styleNumber;

	private BrandId brandId;

	private MainProductGroupId mainProductGroupId;

	private ProductGroupId productGroupId;

	private Integer numberOfStyleVariants;

	private StyleData data;

	public StyleDto() {
		super();
	}

	public StyleDto(StyleId id) {
		super(id);
	}

	public StyleNumber getStyleNumber() {
		return styleNumber;
	}

	public void setStyleNumber(StyleNumber styleNumber) {
		this.styleNumber = styleNumber;
	}

	public BrandId getBrandId() {
		return brandId;
	}

	public void setBrandId(BrandId brandId) {
		this.brandId = brandId;
	}

	public MainProductGroupId getMainProductGroupId() {
		return mainProductGroupId;
	}

	public void setMainProductGroupId(MainProductGroupId mainProductGroupId) {
		this.mainProductGroupId = mainProductGroupId;
	}

	public ProductGroupId getProductGroupId() {
		return productGroupId;
	}

	public void setProductGroupId(ProductGroupId productGroupId) {
		this.productGroupId = productGroupId;
	}

	public Integer getNumberOfStyleVariants() {
		return numberOfStyleVariants;
	}

	public void setNumberOfStyleVariants(Integer numberOfStyleVariants) {
		this.numberOfStyleVariants = numberOfStyleVariants;
	}

	public StyleData getData() {
		return data;
	}

	public void setData(StyleData data) {
		this.data = data;
	}

}
