package puc.plm.service.domains.style.command.events;

import java.time.Instant;

import puc.plm.service.common.messages.Event;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.ids.StyleId;

/**
 * Event, signaling the creation of a style
 */
public class StyleCreatedEvent implements Event {

	private StyleId styleId;

	private ProductGroupId productGroupId;

	private BrandId brandId;

	private StyleData styleData;

	private Instant creationTime;

	public StyleCreatedEvent(StyleId styleId, ProductGroupId productGroupId, BrandId brandId, StyleData styleData,
			Instant creationTime) {
		super();
		this.styleId = styleId;
		this.productGroupId = productGroupId;
		this.brandId = brandId;
		this.styleData = styleData;
		this.creationTime = creationTime;
	}

	public StyleId getStyleId() {
		return styleId;
	}

	public StyleData getStyleData() {
		return styleData;
	}

	public Instant getCreationTime() {
		return creationTime;
	}

	public ProductGroupId getProductGroupId() {
		return productGroupId;
	}

	public BrandId getBrandId() {
		return brandId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brandId == null) ? 0 : brandId.hashCode());
		result = prime * result + ((creationTime == null) ? 0 : creationTime.hashCode());
		result = prime * result + ((productGroupId == null) ? 0 : productGroupId.hashCode());
		result = prime * result + ((styleData == null) ? 0 : styleData.hashCode());
		result = prime * result + ((styleId == null) ? 0 : styleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		StyleCreatedEvent other = (StyleCreatedEvent) obj;
		if (brandId == null) {
			if (other.brandId != null) return false;
		} else if (!brandId.equals(other.brandId)) return false;
		if (creationTime == null) {
			if (other.creationTime != null) return false;
		} else if (!creationTime.equals(other.creationTime)) return false;
		if (productGroupId == null) {
			if (other.productGroupId != null) return false;
		} else if (!productGroupId.equals(other.productGroupId)) return false;
		if (styleData == null) {
			if (other.styleData != null) return false;
		} else if (!styleData.equals(other.styleData)) return false;
		if (styleId == null) {
			if (other.styleId != null) return false;
		} else if (!styleId.equals(other.styleId)) return false;
		return true;
	}

	@Override
	public String toString() {
		return "StyleCreatedEvent [styleId=" + styleId + ", productGroupId=" + productGroupId + ", brandId=" + brandId
				+ ", styleData=" + styleData + ", creationTime=" + creationTime + "]";
	}
}
