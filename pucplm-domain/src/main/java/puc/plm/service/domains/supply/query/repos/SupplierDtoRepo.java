package puc.plm.service.domains.supply.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.supply.ids.SupplierId;
import puc.plm.service.domains.supply.query.dtos.SupplierDto;

public interface SupplierDtoRepo extends JpaRepository<SupplierDto, SupplierId> {

}
