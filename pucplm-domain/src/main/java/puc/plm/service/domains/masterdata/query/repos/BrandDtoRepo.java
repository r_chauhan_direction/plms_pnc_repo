package puc.plm.service.domains.masterdata.query.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.query.dtos.BrandDto;

public interface BrandDtoRepo extends JpaRepository<BrandDto, BrandId> {

}
