package puc.plm.service.domains.style.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import puc.plm.service.domains.style.StyleQueryService;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.query.dtos.StyleDto;
import puc.plm.service.domains.style.query.dtos.StyleVariantDto;
import puc.plm.service.domains.style.query.queries.StylesByCriteriaQuery;
import puc.plm.service.domains.style.query.repos.StyleDtoRepo;
import puc.plm.service.domains.style.query.repos.StyleVariantDtoRepo;

@Component
@Transactional(readOnly = true)
public class StyleQueryServiceImpl implements StyleQueryService {

	@Autowired
	StyleDtoRepo styleDtoRepo;

	@Autowired
	StyleVariantDtoRepo styleVariantDtoRepo;

	@Override
	public List<StyleDto> getStyles(StylesByCriteriaQuery query) {

		StyleDto example = new StyleDto();
		example.setBrandId(query.getBrandId().orElse(null));
		example.setMainProductGroupId(query.getMainProductGroupId().orElse(null));
		example.setProductGroupId(query.getProductGroupId().orElse(null));
		example.setStyleNumber(query.getStyleNumber().orElse(null));

		// return styleDtoRepo.findAll(Example.of(example));

		return styleDtoRepo.findAll();
	}

	@Override
	public StyleDto getStyle(StyleId styleId) {
		return styleDtoRepo.findById(styleId).orElse(null);
	}

	@Override
	public List<StyleVariantDto> getStyleVariants(StyleId styleId) {
		return styleVariantDtoRepo.findByStyleId(styleId);
	}

}
