package puc.plm.service.domains.planning.query.dtos;

import java.util.ArrayList;
import java.util.List;

import puc.plm.service.domains.planning.ids.PlanPeriodStyleVariantId;

public class PlanPeriodStyleVariantDto {

	/**
	 * ID of the plan period style variant
	 */
	private PlanPeriodStyleVariantId id;

	/**
	 * batches associated with the plan period style variant
	 */
	private List<StyleVariantBatchDto> batchDtos = new ArrayList<>();

	public PlanPeriodStyleVariantDto(PlanPeriodStyleVariantId id) {
		super();
		this.id = id;
	}

	public PlanPeriodStyleVariantId getId() {
		return id;
	}

	protected void setId(PlanPeriodStyleVariantId id) {
		this.id = id;
	}

	public List<StyleVariantBatchDto> getBatchDtos() {
		return batchDtos;
	}

	public void setBatchDtos(List<StyleVariantBatchDto> batchDtos) {
		this.batchDtos = batchDtos;
	}
}
