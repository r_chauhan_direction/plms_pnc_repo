package puc.plm.service.domains.planning.query.dtos;

import javax.persistence.Entity;

import puc.plm.service.common.entity.SimpleEntity;
import puc.plm.service.domains.masterdata.ids.SeasonId;
import puc.plm.service.domains.planning.ids.PlanPeriodId;

@Entity
public class PlanPeriodDto extends SimpleEntity<PlanPeriodId> {

	/**
	 * short name of the plan period
	 */
	private String shortName;

	/**
	 * full name of the plan period
	 */
	private String name;

	/**
	 * season the plan period is part of
	 */
	private SeasonId season;

	protected PlanPeriodDto() {}

	public PlanPeriodDto(PlanPeriodId id, String shortName, String name, SeasonId season) {
		super(id);
		this.shortName = shortName;
		this.name = name;
		this.season = season;
	}

	public String getName() {
		return name;
	}

	public SeasonId getSeason() {
		return season;
	}

	public String getShortName() {
		return shortName;
	}

}
