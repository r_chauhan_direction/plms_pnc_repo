package puc.plm.service.domains.masterdata.ids;

import javax.persistence.Embeddable;

import puc.plm.service.common.value.IntValue;

@Embeddable
public class ClientId extends IntValue {

	private static final long serialVersionUID = 1L;

	protected ClientId() {
		super();
	}

	public ClientId(Integer value) {
		super(value);
	}
}
