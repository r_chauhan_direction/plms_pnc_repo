package puc.plm.service.domains.style.exeptions;

public class StyleVariantNotExistingException extends RuntimeException {

	public StyleVariantNotExistingException() {
		super();
	}

	public StyleVariantNotExistingException(String message, Throwable cause) {
		super(message, cause);
	}

	public StyleVariantNotExistingException(String message) {
		super(message);
	}

	public StyleVariantNotExistingException(Throwable cause) {
		super(cause);
	}

	private static final long serialVersionUID = 1L;

}
