package puc.plm.service.common.eventhandling;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Marks a method that subcribes to an {@link Event}.
 * 
 * The method must be public and take any {@link Event} as first and only argument.
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Subscribes {

}
