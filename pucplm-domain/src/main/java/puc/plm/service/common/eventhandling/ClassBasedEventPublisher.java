package puc.plm.service.common.eventhandling;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import puc.plm.service.common.messages.Event;

/**
 * Publishes events to event subscribers based on the event's class.
 * 
 * Note: this event publisher does not take class hierarchies into account. This means if A extends
 * B, A and B are seen as distinct as A and a completely independent class C
 */
public class ClassBasedEventPublisher implements EventPublisher {

	/**
	 * Mapping from event class to event subscribers
	 */
	private final Map<Class<? extends Event>, List<Consumer<? extends Event>>> eventSubscribers = new ConcurrentHashMap<>();

	@Override
	public void publish(List<? extends Event> eventsToPublish) {
		for (Event event : eventsToPublish) {
			publish(event);
		}
	}

	@SuppressWarnings("unchecked")
	private <T extends Event> void publish(T event) {
		Class<? extends Event> eventClass = event.getClass();
		List<Consumer<? extends Event>> matchingEventSubscribers = eventSubscribers.getOrDefault(eventClass,
				Collections.emptyList());

		for (Consumer<? extends Event> eventSubscriber : matchingEventSubscribers) {
			((Consumer<T>) eventSubscriber).accept(event);
		}
	}

	/**
	 * Registers an event subscriber for a specific event class
	 * 
	 * @param eventClass
	 *            class of the event the subscriber is registered for
	 * @param eventSubscriber
	 *            the subscriber registered for the event class
	 */
	public synchronized <T extends Event> void registerEventSubscriber(Class<T> eventClass,
			Consumer<T> eventSubscriber) {
		eventSubscribers.computeIfAbsent(eventClass, e -> new ArrayList<>()).add(eventSubscriber);
	}

}
