package puc.plm.service.common.value;

import java.io.Serializable;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SimpleValue<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	protected T value;

	protected SimpleValue(T value) {
		this.value = value;
	}

	public T getValue() {
		return value;
	}

	protected void setValue(T value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj.getClass() == this.getClass())) return false;
		SimpleValue<?> other = (SimpleValue<?>) obj;
		if (value == null) {
			if (other.value != null) return false;
		} else if (!value.equals(other.value)) return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [value=" + value + "]";
	}
}
