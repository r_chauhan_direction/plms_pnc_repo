package puc.plm.service.common.entity;

import javax.persistence.MappedSuperclass;

/**
 * Abstract {@link SimpleEntity} with active/inactive state
 *
 * @param <T>
 *            type of the entity's ID
 */
@MappedSuperclass
public abstract class ActiveEntity<T> extends SimpleEntity<T> {

	protected boolean active = true;

	protected ActiveEntity() {
		super();
	}

	protected ActiveEntity(T id) {
		super(id);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
