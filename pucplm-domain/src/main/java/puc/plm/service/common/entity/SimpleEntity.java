package puc.plm.service.common.entity;

import java.util.Objects;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Abstract base class for JPA entities.
 *
 * @param <T>
 *            type of the entity's ID
 */
@MappedSuperclass
public abstract class SimpleEntity<T> {

	@Id
	protected T id;

	protected SimpleEntity() {}

	protected SimpleEntity(T id) {
		super();
		this.id = id;
	}

	public T getId() {
		return id;
	}

	protected void setId(T id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		SimpleEntity other = (SimpleEntity) obj;
		return Objects.equals(id, other.id);
	}

}
