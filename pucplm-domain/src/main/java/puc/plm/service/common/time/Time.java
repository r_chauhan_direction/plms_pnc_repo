package puc.plm.service.common.time;

import java.time.Instant;

public class Time {

	private static TimeProvider TIME_PROVIDER = new SystemTimeProvider();

	public static Instant now() {
		return TIME_PROVIDER.now();
	}

	public static void setTimeProvider(TimeProvider timeProvider) {
		TIME_PROVIDER = timeProvider;
	}

}
