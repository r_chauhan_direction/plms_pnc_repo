package puc.plm.service.common.value;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class IntValue extends SimpleValue<Integer> {

	private static final long serialVersionUID = 1L;

	protected IntValue() {
		super(null);
	}

	protected IntValue(Integer value) {
		super(value);
	}
}
