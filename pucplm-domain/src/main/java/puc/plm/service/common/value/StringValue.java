package puc.plm.service.common.value;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class StringValue extends SimpleValue<String> {

	private static final long serialVersionUID = 1L;

	protected StringValue() {
		super(null);
	}

	protected StringValue(String value) {
		super(value);
	}
}
