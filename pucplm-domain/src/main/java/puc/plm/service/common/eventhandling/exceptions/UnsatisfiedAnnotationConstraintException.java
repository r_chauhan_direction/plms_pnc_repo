package puc.plm.service.common.eventhandling.exceptions;

/**
 * Signals a constraint for an annotated method that was not satisfied.
 */
public class UnsatisfiedAnnotationConstraintException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnsatisfiedAnnotationConstraintException() {}

	public UnsatisfiedAnnotationConstraintException(String message) {
		super(message);
	}

	public UnsatisfiedAnnotationConstraintException(Throwable cause) {
		super(cause);
	}

	public UnsatisfiedAnnotationConstraintException(String message, Throwable cause) {
		super(message, cause);
	}

}
