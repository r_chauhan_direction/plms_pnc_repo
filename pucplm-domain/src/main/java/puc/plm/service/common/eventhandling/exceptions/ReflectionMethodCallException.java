package puc.plm.service.common.eventhandling.exceptions;

/**
 * Signals an exception while executing a method call via reflection.
 */
public class ReflectionMethodCallException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ReflectionMethodCallException() {}

	public ReflectionMethodCallException(String message) {
		super(message);
	}

	public ReflectionMethodCallException(Throwable cause) {
		super(cause);
	}

	public ReflectionMethodCallException(String message, Throwable cause) {
		super(message, cause);
	}

}
