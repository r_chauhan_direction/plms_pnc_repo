package puc.plm.service.common.aggregate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import puc.plm.service.common.eventhandling.EventPublisher;
import puc.plm.service.common.messages.Event;

/**
 * Base aggregate repository to provide direct methods for loading and saving {@link Aggregate}s.
 * 
 * @param <A>
 *            type of the {@link Aggregate}
 * @param <R>
 *            type of the aggregate's root entity
 * @param <I>
 *            type of the aggregate's ID
 */
public abstract class AggregateRepo<A extends Aggregate<R, I>, R, I> {

	/**
	 * Repo for retrieving and storing the aggregate root entity
	 */
	@Autowired
	CrudRepository<R, I> aggregateRootRepo;

	/**
	 * used for publishing events created by aggregates
	 */
	@Autowired
	EventPublisher eventPublisher;

	/**
	 * Loads an aggregate by it's ID, passes it to the given function and returns the function
	 * result
	 * 
	 * @param aggregateId
	 *            ID of the aggregate
	 * @param aggregateFunction
	 *            Function to apply on the aggregate with the given aggregateId
	 * @return result of the aggregate function applied on the aggregate
	 */
	public <T> T applyWithResult(I aggregateId, Function<A, T> aggregateFunction) {
		A aggregate = findAggregateById(aggregateId);
		T result = aggregateFunction.apply(aggregate);
		saveAggregate(aggregate);
		return result;
	}

	/**
	 * Loads an aggregate by it's ID and passes it to the given consumer
	 * 
	 * @param aggregateId
	 *            ID of the aggregate
	 * @param aggregateConsumer
	 *            consumer to accept the aggregate with the given aggregateId
	 */
	public void apply(I aggregateId, Consumer<A> aggregateConsumer) {
		applyWithResult(aggregateId, a -> {
			aggregateConsumer.accept(a);
			return null;
		});
	}

	/**
	 * Finds an aggregate by ID. Depending whether the aggregats root entity can be found in the
	 * {@link #aggregateRootRepo}, an aggregate instance initialized with the root entity or an
	 * unitialized aggregate instance with only the given aggregateId set is created.
	 * 
	 * @param aggregateId
	 *            ID of the aggregate. null values are not allowed
	 * @return an aggregate instance (initialized or uninitialized)
	 * @throws NullPointerException
	 *             if aggregateId is null
	 */
	public A findAggregateById(I aggregateId) {
		Objects.requireNonNull(aggregateId, "aggregateId must not be null for loading an aggregate");
		Optional<R> rootOpt = findAggregateRootById(aggregateId);
		return rootOpt.map(root -> constructAggregate(aggregateId, root))
				.orElseGet(() -> constructAggregate(aggregateId, null));
	}

	private Optional<R> findAggregateRootById(I aggregateId) {
		return aggregateRootRepo.findById(aggregateId);
	}

	/**
	 * Saves an aggregate by saving the aggregates root entity.
	 * 
	 * @param aggregate
	 *            the aggregate to save
	 * 
	 * @throws NullPointerException
	 *             if aggregate or it's root entity is null
	 */
	public void saveAggregate(A aggregate) {
		Objects.requireNonNull(aggregate, "aggregate to store must not be null");
		I aggregateId = aggregate.getId();
		R root = aggregate.getRootEntity();
		Objects.requireNonNull(root, "root entity of aggregate " + aggregateId + " must not be null");
		saveAggregateRoot(root);
		List<Event> unpublishedEvents = aggregate.getAndClearUnpublishedEvents();
		publishEvents(unpublishedEvents);
	}

	private void publishEvents(List<Event> unpublishedEvents) {
		eventPublisher.publish(unpublishedEvents);
	}

	private void saveAggregateRoot(R root) {
		aggregateRootRepo.save(root);
	}

	/**
	 * Constructs an aggregate instance from a given aggregate ID and an root entity instance.
	 * 
	 * @param aggregateId
	 *            ID of the aggregate to construct
	 * @param root
	 *            root entity of the aggregate to construct. May be null which signals an
	 *            unititialized aggregate
	 * @return an aggregate instance
	 */
	abstract protected A constructAggregate(I aggregateId, R root);

}
