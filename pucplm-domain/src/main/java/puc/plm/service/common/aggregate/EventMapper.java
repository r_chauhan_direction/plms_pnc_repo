package puc.plm.service.common.aggregate;

import java.util.ArrayList;
import java.util.List;

import puc.plm.service.common.messages.Event;

public class EventMapper {

	private List<EventMapping<? extends Event>> mappings = new ArrayList<>();

	public void addEventMapping(EventMapping<? extends Event> mapping) {
		mappings.add(mapping);
	}

	public void handle(Event event) {
		for (EventMapping<? extends Event> mapping : mappings) {
			if (mapping.canHandle(event)) {
				mapping.handle(event);
				return;
			}
		}
		throw new IllegalArgumentException("no event mapping for " + event);
	}

}
