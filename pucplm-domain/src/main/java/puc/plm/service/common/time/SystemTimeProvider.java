package puc.plm.service.common.time;

import java.time.Instant;

public class SystemTimeProvider implements TimeProvider {

	@Override
	public Instant now() {
		return Instant.now();
	}

}
