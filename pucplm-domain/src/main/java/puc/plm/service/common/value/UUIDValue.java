package puc.plm.service.common.value;

import java.util.UUID;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class UUIDValue extends SimpleValue<UUID> {

	private static final long serialVersionUID = 1L;

	protected UUIDValue() {
		super(UUID.randomUUID());
	}

	protected UUIDValue(UUID value) {
		super(value);
	}
}
