package puc.plm.service.common.eventhandling;

import java.util.List;

import puc.plm.service.common.messages.Event;

/**
 * Publishes events to interested subscribers
 */
public interface EventPublisher {

	/**
	 * Publish a list of events
	 * 
	 * @param eventsToPublish
	 *            list of events to publish
	 */
	void publish(List<? extends Event> eventsToPublish);

}
