package puc.plm.service.common.aggregate;

import java.util.function.Consumer;

import puc.plm.service.common.messages.Event;

public class EventMapping<T extends Event> {

	private Class<? extends T> eventClass;

	private Consumer<T> eventHandler;

	public EventMapping(Class<? extends T> eventClass, Consumer<T> eventHandler) {
		super();
		this.eventClass = eventClass;
		this.eventHandler = eventHandler;
	}

	public boolean canHandle(Event event) {
		return eventClass.isAssignableFrom(event.getClass());
	}

	@SuppressWarnings("unchecked")
	public void handle(Event event) {
		eventHandler.accept((T) event);
	}
}
