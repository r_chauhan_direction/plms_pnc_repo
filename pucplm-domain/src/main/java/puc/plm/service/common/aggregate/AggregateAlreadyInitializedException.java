package puc.plm.service.common.aggregate;

/**
 * Signals, that an aggregate is already initialized when an uninitialized aggregate is expected.
 */
public class AggregateAlreadyInitializedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AggregateAlreadyInitializedException() {}

	public AggregateAlreadyInitializedException(String message) {
		super(message);
	}

	public AggregateAlreadyInitializedException(Throwable cause) {
		super(cause);
	}

	public AggregateAlreadyInitializedException(String message, Throwable cause) {
		super(message, cause);
	}
}
