package puc.plm.service.common.comparators;

import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Compares two objects of type T against a search string according to their Levenshtein distance to
 * the search string. The Levenshtein distance is not calculated against the complete "object
 * string" but against each word in the "object string".
 * 
 * @param <T>
 *            Type of objects the comparator can compare
 */
public class LevenshteinComparator<T> implements Comparator<T> {

	private String searchString;
	private Function<T, String> stringExtractor;

	/**
	 * Gets the best matching candidates from a list of potential candidate objects by calculating
	 * the Leventshtein distance between the query string and a string extracted from the candidate
	 * objects.
	 * 
	 * @param queryString
	 *            the query string the candidate objects are compared against
	 * @param potentialCandidates
	 *            list of potential candidates
	 * @param maxSize
	 *            maximum size of the returned candidate list
	 * @param intStringExtractor
	 *            extractor from the candidate object to a string - used if the query string is
	 *            parsable as int
	 * @param stringExtractor
	 *            extractor from the candidate object to a string - used if the query string is NOT
	 *            parsable as int - might be the same as intStringExtractor
	 * @return list of at most maxSize best matching candidates - best matching candidates first
	 */
	public static <T> List<T> getBestMatching(String queryString, List<T> potentialCandidates, int maxSize,
			Function<T, String> intStringExtractor, Function<T, String> stringExtractor) {
		LevenshteinComparator<T> comparator = null;
		try {
			// try to compare by extracted int value
			Integer intQueryString = Integer.valueOf(queryString);
			comparator = new LevenshteinComparator<>(intQueryString.toString(), intStringExtractor);
		} catch (Exception e) {
			// compare by string value instead
			comparator = new LevenshteinComparator<>(queryString, stringExtractor);
		}

		return potentialCandidates.stream().sorted(comparator).limit(maxSize).collect(Collectors.toList());
	}

	/**
	 * Instanciates a LevenshteinComparator for a given search string.
	 * 
	 * @param searchString
	 *            String to calculate the Levenshtein distance against
	 * @param stringExtractor
	 *            Function to extract the object specific string the Levenshtein distance is
	 *            calculated against
	 */
	public LevenshteinComparator(String searchString, Function<T, String> stringExtractor) {
		super();
		this.searchString = searchString;
		this.stringExtractor = stringExtractor;
	}

	public static int getLevenshteinDistance(char[] s, String search) {
		int distance = Integer.MAX_VALUE;
		int minimum = Integer.MAX_VALUE;
		int index = 0;
		int count = 0;
		StringTokenizer stringTokenizer = new StringTokenizer(search, " ");
		while (stringTokenizer.hasMoreTokens()) {
			String token = stringTokenizer.nextToken();
			distance = getLevenshteinDistance(s, token.toCharArray());
			if (minimum > distance) {
				minimum = distance;
				index = count;
			}
			count++;
		}
		return minimum * 2 + index;
	}

	public static int getLevenshteinDistance(char[] search, char[] source) {
		int hit = 0;
		int[][] d = new int[source.length + 1][search.length + 1];

		for (int sourceIndex = 0; sourceIndex <= source.length; sourceIndex++) {
			d[sourceIndex][0] = sourceIndex;
		}
		for (int searchIndex = 1; searchIndex <= search.length; searchIndex++) {
			d[0][searchIndex] = searchIndex;
		}

		for (int sourceIndex = 1; sourceIndex <= source.length; sourceIndex++) {
			for (int searchIndex = 1; searchIndex <= search.length; searchIndex++) {
				int cost;
				if (source[sourceIndex - 1] == search[searchIndex - 1]) {
					cost = 0;
					hit = sourceIndex;
				} else {
					cost = 1;
				}

				int del = d[sourceIndex - 1][searchIndex] + 1;
				int ins = d[sourceIndex][searchIndex - 1] + 1;
				int subst = d[sourceIndex - 1][searchIndex - 1] + cost;
				d[sourceIndex][searchIndex] = Math.min(Math.min(del, ins), subst);
			}
		}
		return d[Math.min(Math.max(search.length, hit), source.length)][search.length];
	}

	@Override
	public int compare(T o1, T o2) {
		int ld1 = getLevenshteinDistance(searchString.toLowerCase().toCharArray(),
				stringExtractor.apply(o1).toLowerCase());
		int ld2 = getLevenshteinDistance(searchString.toLowerCase().toCharArray(),
				stringExtractor.apply(o2).toLowerCase());
		if (ld1 == ld2 || (ld1 > 20 && ld2 > 20)) {
			return stringExtractor.apply(o1).toLowerCase().compareTo(stringExtractor.apply(o2).toLowerCase());
		} else if (ld1 >= ld2) {
			return 1;
		} else {
			return -1;
		}
	}
}
