package puc.plm.service.common.aggregate;

/**
 * Signals, that an aggregate is uninitialized when an initialized aggregate is expected.
 */
public class AggregateUninitializedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AggregateUninitializedException() {}

	public AggregateUninitializedException(String message) {
		super(message);
	}

	public AggregateUninitializedException(Throwable cause) {
		super(cause);
	}

	public AggregateUninitializedException(String message, Throwable cause) {
		super(message, cause);
	}
}
