package puc.plm.service.common.caching;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import puc.plm.service.common.entity.SimpleEntity;
import puc.plm.service.common.time.Time;

/**
 * Caches a List of {@link SimpleEntity}s for a certain {@link Duration} and makes them available as
 * bulk and for lookup by id.
 *
 * @param <T>
 *            Type of the entity
 * @param <ID>
 *            Type of the entity's ID
 */
public class EntityCache<T extends SimpleEntity<ID>, ID> {

	private static final Logger log = LoggerFactory.getLogger(EntityCache.class);

	/**
	 * supplies a list of entites for the cache
	 */
	private Supplier<List<T>> entitiesSupplier;

	/**
	 * duration after which the cache is invalidated
	 */
	private Duration cachingDuration;

	/**
	 * Instant, the cache must be invalidated the next time
	 */
	private volatile Instant nextInvalidationTime = Time.now();

	/**
	 * List of cached entities
	 */
	private List<T> cachedEntities;

	/**
	 * Map of cached entities by ID
	 */
	private Map<ID, T> cachedEntitiesById;

	/**
	 * Creates an entity cache that caches entities for the given duration and retrieves the
	 * entities to cache from the given entitiesSupplier.
	 * 
	 * @param cachingDuration
	 *            Duration to cache the entities for
	 * @param entitiesSupplier
	 *            Supplier for retrieving the entities to cache
	 */
	public EntityCache(Duration cachingDuration, Supplier<List<T>> entitiesSupplier) {
		super();
		this.cachingDuration = cachingDuration;
		this.entitiesSupplier = entitiesSupplier;
	}

	/**
	 * Gets all cached entities
	 * 
	 * @return all cached entities
	 */
	public List<T> findAll() {
		refreshEntitiesIfNeeded();
		return new ArrayList<>(cachedEntities);
	}

	/**
	 * Filters the cached entities.
	 * 
	 * @param predicate
	 *            Filter predicate for the entity type T
	 * @return cached entities matching the given predicate
	 */
	public List<T> findAllByFilter(Predicate<? super T> predicate) {
		refreshEntitiesIfNeeded();
		return cachedEntities.stream().filter(predicate).collect(toList());
	}

	/**
	 * invalidates the cache
	 */
	public void invalidateCache() {
		nextInvalidationTime = Time.now();
	}

	/**
	 * Finds a cached entity by its ID
	 * 
	 * @param entityId
	 *            ID of the cached entity
	 * @return the cached entity or null if no entity found
	 */
	public T findById(ID entityId) {
		refreshEntitiesIfNeeded();
		return cachedEntitiesById.get(entityId);
	}

	private void refreshEntitiesIfNeeded() {
		if (nextInvalidationTime.isBefore(Time.now())) {
			log.info("executing cache refresh needed");
			cachedEntities = entitiesSupplier.get();
			cachedEntitiesById = cachedEntities.stream().collect(toMap(SimpleEntity::getId, Function.identity()));
			nextInvalidationTime = Time.now().plus(cachingDuration);
			log.info("cache refreshed, next invalidation: " + nextInvalidationTime + ", entries: "
					+ cachedEntities.size());
		}
	}
}
