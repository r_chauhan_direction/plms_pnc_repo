package puc.plm.service.common.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Abstract base class for versioned JPA entities. A dedicated int column is used for versioning.
 *
 * @param <T>
 *            type of the entity's ID
 */
@MappedSuperclass
public abstract class VersionedEntity<T> extends SimpleEntity<T> {

	@Version
	protected Integer version;

	protected VersionedEntity() {
		super();
	}

	protected VersionedEntity(T id) {
		super(id);
	}
}
