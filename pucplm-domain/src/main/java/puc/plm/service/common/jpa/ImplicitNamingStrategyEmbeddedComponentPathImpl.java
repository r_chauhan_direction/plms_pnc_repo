package puc.plm.service.common.jpa;

import org.hibernate.boot.model.naming.ImplicitNamingStrategyComponentPathImpl;
import org.hibernate.boot.model.source.spi.AttributePath;

import puc.plm.service.common.value.SimpleValue;

/**
 * Extension of hibernate's {@link ImplicitNamingStrategyComponentPathImpl} that removes the value
 * suffix from composite attribute paths in order to provide readable column names for embedded
 * {@link SimpleValue}s.
 */
public class ImplicitNamingStrategyEmbeddedComponentPathImpl extends ImplicitNamingStrategyComponentPathImpl {

	private static final long serialVersionUID = 1L;
	private static final String VALUE_SUFFIX = "_value";

	@Override
	protected String transformAttributePath(AttributePath attributePath) {

		String transformedAttributePath = super.transformAttributePath(attributePath);

		if (hasParent(attributePath) && endsWithValue(transformedAttributePath)) {
			return removeValueSuffix(transformedAttributePath);
		} else {
			return transformedAttributePath;
		}
	}

	private String removeValueSuffix(String transformedAttributePath) {
		return transformedAttributePath.substring(0, transformedAttributePath.length() - VALUE_SUFFIX.length());
	}

	private boolean endsWithValue(String transformedAttributePath) {
		return transformedAttributePath != null && transformedAttributePath.endsWith(VALUE_SUFFIX);
	}

	private boolean hasParent(AttributePath attributePath) {
		return attributePath.getParent() != null && !"".equals(attributePath.getParent().getProperty());
	}
}
