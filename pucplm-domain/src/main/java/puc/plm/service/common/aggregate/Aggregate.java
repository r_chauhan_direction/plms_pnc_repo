package puc.plm.service.common.aggregate;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import puc.plm.service.common.messages.Event;

/**
 * Base class for an aggregate.
 * 
 * @param <R>
 *            type of the aggregate's root entity
 * @param <I>
 *            type of the aggregate's ID
 */
public abstract class Aggregate<R, I> {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * ID of the aggregate
	 */
	protected I id;

	/**
	 * root entity of the aggregate
	 */
	protected R root;

	/**
	 * events applied to the aggregate but unpublished
	 */
	private List<Event> unpublishedEvents = new ArrayList<>();

	/**
	 * maps event classes to event handlers within the aggregate
	 */
	private EventMapper eventMapper = new EventMapper();

	/**
	 * Constructs an aggregate from it's ID and it's root entity
	 * 
	 * @param id
	 *            ID of the aggregate
	 * @param root
	 *            root entity of the aggregate
	 */
	protected Aggregate(I id, R root) {
		super();
		this.id = id;
		this.root = root;
		mapEvents();
	}

	/**
	 * Must be implemented by the individual aggregate. Must call {@link #map(Class, Consumer)} for
	 * every event class.
	 */
	protected abstract void mapEvents();

	/**
	 * Adds an event mapping from an event class to an event handler to the aggregate
	 * 
	 * @param eventClass
	 *            the {@link Class} of the event to handle
	 * @param eventHandler
	 *            the event handler for the given eventClass
	 */
	protected <T extends Event> void map(Class<? extends T> eventClass, Consumer<T> eventHandler) {
		eventMapper.addEventMapping(new EventMapping<T>(eventClass, eventHandler));
	}

	/**
	 * applies an {@link Event} by invoking the appropriate event handler.
	 * 
	 * It is important to apply the event via this method instead of invoking the event handler
	 * directly. This way the event can be registered and later be published.
	 * 
	 * @param event
	 *            the event to apply
	 */
	public <T extends Event> void apply(T event) {
		eventMapper.handle(event);
		unpublishedEvents.add(event);
	}

	/**
	 * Checks whether the aggreagate has any unpublished events.
	 * 
	 * @return true, if the aggregate has any unpublished events, false otherwise
	 */
	public boolean hasUnpublishedEvents() {
		return !unpublishedEvents.isEmpty();
	}

	/**
	 * Gets all of the aggregate's unpublished events and clears the list of unpublished events in
	 * one go.
	 * 
	 * IMPORTANT: as the list of unpublished events is cleared with this call, events not handled
	 * after calling this method will be lost
	 * 
	 * @return list of unpublished events
	 */
	public List<Event> getAndClearUnpublishedEvents() {
		List<Event> copyOfUnpublishedEvents = new ArrayList<>(unpublishedEvents);
		unpublishedEvents.clear();
		return copyOfUnpublishedEvents;
	}

	public I getId() {
		return id;
	}

	public R getRootEntity() {
		return root;
	}

	/**
	 * Checks whether an aggregate is initialized. An aggregate counts as initialized if it's root
	 * entity is set.
	 * 
	 * @return true if aggregate is initialized, false otherwise
	 */
	public boolean isInitialized() {
		return root != null;
	}

	/**
	 * Checks whether an aggregate is uninitialized and throws an exception if already initialized
	 * 
	 * @param errorMessageIfInitialized
	 *            error message used if the aggregate is already initialized
	 * @throws AggregateAlreadyInitializedException
	 *             if the aggregate is already initialized
	 */
	protected void checkUninitialized(String errorMessageIfInitialized) {
		if (isInitialized()) {
			logger.error("Aggregate is already initialized: " + errorMessageIfInitialized);
			throw new AggregateAlreadyInitializedException(errorMessageIfInitialized);
		}
	}

	/**
	 * Checks whether an aggregate is initialized and throws an exception if still uninitialized
	 * 
	 * @param errorMessageIfUninitialized
	 *            error message used if the aggregate is still uninitialized
	 * @throws AggregateUninitializedException
	 *             if the aggregate is still uninitialized
	 */
	protected void checkInitialized(String errorMessageIfUninitialized) {
		if (!isInitialized()) {
			logger.error("Aggregate is uninitialized: " + errorMessageIfUninitialized);
			throw new AggregateUninitializedException(errorMessageIfUninitialized);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (!(obj instanceof Aggregate)) return false;
		Aggregate<?, ?> other = (Aggregate<?, ?>) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}

}
