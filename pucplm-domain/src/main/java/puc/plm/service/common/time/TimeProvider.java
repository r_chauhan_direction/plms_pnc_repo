package puc.plm.service.common.time;

import java.time.Instant;

public interface TimeProvider {

	/**
	 * @return the time providers current time
	 */
	Instant now();

}
