package puc.plm.service.common.eventhandling;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;

import puc.plm.service.common.eventhandling.exceptions.ReflectionMethodCallException;
import puc.plm.service.common.eventhandling.exceptions.UnsatisfiedAnnotationConstraintException;
import puc.plm.service.common.messages.Event;

/**
 * Scans Spring beans for {@link Subscribes} annotations and registers the found methods as event
 * subscribers
 * 
 * Each annotated method must take any {@link Event} as first and only parameter.
 */
public class EventSubscriptionAnnotationScanner implements BeanPostProcessor {

	private static Logger logger = LoggerFactory.getLogger(EventSubscriptionAnnotationScanner.class);

	private ClassBasedEventPublisher eventPublisher;

	/**
	 * Creates a {@link EventSubscriptionAnnotationScanner} with the event publisher to register the
	 * subscribers at.
	 * 
	 * @param eventPublisher
	 *            event publisher to register the event subscribers at
	 */
	public EventSubscriptionAnnotationScanner(ClassBasedEventPublisher eventPublisher) {
		this.eventPublisher = eventPublisher;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		logger.debug("processing bean " + beanName);
		scanForAnnotation(bean, beanName);
		return bean;
	}

	/**
	 * Scans a bean methods annotated with {@link Subscribes} and registers them respectively.
	 * 
	 * @param bean
	 * @param beanName
	 */
	private void scanForAnnotation(Object bean, String beanName) {

		Class<?> targetClass = AopUtils.getTargetClass(bean);
		Method[] beanMethods = targetClass.getMethods();

		for (Method beanMethod : beanMethods) {
			scanForSubscribesAnnotation(bean, beanName, beanMethod);
		}
	}

	/**
	 * Scans a {@link Method} for {@link Subscribes} annotation and registers any annotated method
	 * as event subscriber
	 * 
	 * @param bean
	 * @param beanName
	 * @param beanMethod
	 */
	private void scanForSubscribesAnnotation(Object bean, String beanName, Method beanMethod) {
		Class<? extends Event> handledEventClass = getHandledEventClassForMethod(Subscribes.class, beanMethod);

		if (handledEventClass != null) {
			logger.info("register handler method " + beanMethod.getName() + " at bean " + beanName + " for "
					+ handledEventClass.getName());
			eventPublisher.registerEventSubscriber(handledEventClass, createEventSubscriber(bean, beanMethod));
		}
	}

	private <T> Consumer<T> createEventSubscriber(Object bean, Method beanMethod) {
		return event -> {
			try {
				beanMethod.invoke(bean, event);
			} catch (Exception e) {
				throw new ReflectionMethodCallException(
						"Invocation of " + beanMethod + " failed with parameter " + event, e);
			}
		};

	}

	/**
	 * Scans the given method for the given annotation type and extracts the exact type of the
	 * {@link Event} parameter
	 * 
	 * @param annotationType
	 * @param beanMethod
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T extends Annotation> Class<? extends Event> getHandledEventClassForMethod(Class<T> annotationType,
			Method beanMethod) {
		T annotation = AnnotationUtils.getAnnotation(beanMethod, annotationType);
		if (annotation == null) {
			return null;
		}

		String annotationName = annotation.annotationType().getName();

		Class<?>[] parameterTypes = beanMethod.getParameterTypes();
		if (parameterTypes.length != 1) {
			throw new UnsatisfiedAnnotationConstraintException("Method " + beanMethod + " annotated with "
					+ annotationName + " must have single parameter of type " + Event.class.getName());
		}

		Class<?> eventParameterType = parameterTypes[0];
		if (!Event.class.isAssignableFrom(eventParameterType)) {
			throw new UnsatisfiedAnnotationConstraintException(
					"Method " + beanMethod + "annotated with " + annotationName + " - first parameter must be of type "
							+ Event.class.getName() + " but ist of type " + eventParameterType.getName());
		}

		return (Class<? extends Event>) eventParameterType;
	}

}
