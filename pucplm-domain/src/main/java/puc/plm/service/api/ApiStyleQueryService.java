package puc.plm.service.api;

import java.util.List;

import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.query.dtos.StyleDto;
import puc.plm.service.domains.style.query.dtos.StyleVariantDto;
import puc.plm.service.domains.style.query.queries.StylesByCriteriaQuery;

/**
 * API service for querying styles
 */
public interface ApiStyleQueryService {

	/**
	 * Gets styles by different criteria
	 * 
	 * @param query
	 *            query object that holds all criteria
	 * @return list of styles matching the criteria
	 */
	List<StyleDto> getStyles(StylesByCriteriaQuery query);

	/**
	 * Gets a style by it's ID
	 * 
	 * @param styleId
	 *            ID of the style
	 * @return style for the given ID
	 */
	StyleDto getStyle(StyleId styleId);

	/**
	 * Gets plan period style variants for a plan period style
	 * 
	 * @param styleId
	 *            ID of the style
	 * @return sorted list of style variants for the given style
	 */
	List<StyleVariantDto> getStyleVariants(StyleId styleId);
}
