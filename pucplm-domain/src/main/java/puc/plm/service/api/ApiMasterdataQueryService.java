package puc.plm.service.api;

import java.util.List;

import puc.plm.service.domains.masterdata.ids.AssortmentGroupId;
import puc.plm.service.domains.masterdata.ids.AssortmentSectionId;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.MainProductGroupId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.masterdata.ids.SeasonId;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.AssortmentSectionDto;
import puc.plm.service.domains.masterdata.query.dtos.BrandDto;
import puc.plm.service.domains.masterdata.query.dtos.MainProductGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.ProductGroupDto;
import puc.plm.service.domains.masterdata.query.dtos.SeasonDto;

/**
 * API service for querying master data.
 */
public interface ApiMasterdataQueryService {

	/**
	 * @return all seasons
	 */
	List<SeasonDto> getAllSeasons();

	/**
	 * @return all active seasons
	 */
	List<SeasonDto> getActiveSeasons();

	/**
	 * Gets a season by it's ID
	 * 
	 * @param seasonId
	 *            ID of the season
	 * @return the season for the given ID
	 */
	SeasonDto getSeason(SeasonId seasonId);

	/**
	 * @return all assortment groups
	 */
	List<AssortmentGroupDto> getAllAssortmentGroups();

	/**
	 * @return all active assortment groups
	 */
	List<AssortmentGroupDto> getActiveAssortmentGroups();

	/**
	 * Gets a assortment group by it's ID
	 * 
	 * @param assortmentGroupId
	 *            ID of the assortment group
	 * @return the assortment group for the given ID
	 */
	AssortmentGroupDto getAssortmentGroup(AssortmentGroupId assortmentGroupId);

	/**
	 * @return all assortment sections
	 */
	List<AssortmentSectionDto> getAllAssortmentSections();

	/**
	 * @return all active assortment sections
	 */
	List<AssortmentSectionDto> getActiveAssortmentSections();

	/**
	 * Gets a assortment section by it's ID
	 * 
	 * @param assortmentSectionId
	 *            ID of the assortment section
	 * @return the assortment section for the given ID
	 */
	AssortmentSectionDto getAssortmentSection(AssortmentSectionId assortmentSectionId);

	/**
	 * @return all main product groups
	 */
	List<MainProductGroupDto> getAllMainProductGroups();

	/**
	 * @return all active main product groups
	 */
	List<MainProductGroupDto> getActiveMainProductGroups();

	/**
	 * Gets a main product group by it's ID
	 * 
	 * @param mainProductGroupId
	 *            ID of the main product group
	 * @return the main product group for the given ID
	 */
	MainProductGroupDto getMainProductGroup(MainProductGroupId mainProductGroupId);

	/**
	 * Gets a main product group by a poduct group ID
	 * 
	 * @param productGroupId
	 *            ID of the product group
	 * @return the main product group for the given product group ID
	 */
	MainProductGroupDto getMainProductGroupForProductGroup(ProductGroupId productGroupId);

	/**
	 * Gets all product groups for a main product group
	 * 
	 * @param mainProductGroupId
	 *            ID of the main product group to the the product groups for
	 * @return product groups for the given main product group
	 */
	List<ProductGroupDto> getAllProductGroupsForMainProductGroup(MainProductGroupId mainProductGroupId);

	/**
	 * Gets all active product groups for a main product group
	 * 
	 * @param mainProductGroupId
	 *            ID of the main product group to the the product groups for
	 * @return active product groups for the given main product group
	 */
	List<ProductGroupDto> getActiveProductGroupsForMainProductGroup(MainProductGroupId mainProductGroupId);

	/**
	 * Gets a product group by it's ID
	 * 
	 * @param productGroupId
	 *            ID of the product group
	 * @return the product group for the given ID
	 */
	ProductGroupDto getProductGroup(ProductGroupId productGroupId);

	/**
	 * Searches for matching candidate brands with a query string
	 * 
	 * @param queryString
	 *            the query string for searching for brands
	 * @param maxResults
	 *            maximum number of results
	 * @return List of brand candidates
	 */
	List<BrandDto> searchBrandCandidatesForString(String queryString, int maxResults);

	/**
	 * Gets a brand by it's ID
	 * 
	 * @param brandId
	 *            ID of the brand
	 * @return the brand for the given ID
	 */
	BrandDto getBrand(BrandId brandId);
}
