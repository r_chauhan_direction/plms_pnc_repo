package puc.plm.service.api;

import java.util.List;

import puc.plm.service.domains.planning.ids.PlanPeriodId;
import puc.plm.service.domains.planning.ids.PlanPeriodStyleId;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodDto;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodStyleDto;
import puc.plm.service.domains.planning.query.dtos.PlanPeriodStyleVariantDto;
import puc.plm.service.domains.planning.query.queries.PlanPeriodStylesByCriteriaQuery;

/**
 * API service for querying planning data.
 */
public interface ApiPlanningQuerySerivce {

	/**
	 * @return all plan periods
	 */
	List<PlanPeriodDto> getAllPlanPeriods();

	/**
	 * Gets a plan period by it's ID
	 * 
	 * @param planPeriodId
	 *            ID of the plan period
	 * @return the plan period for the given ID
	 */
	PlanPeriodDto getPlanPeriod(PlanPeriodId planPeriodId);

	/**
	 * Gets plan period styles by different criteria
	 * 
	 * @param query
	 *            query object that holds all criteria
	 * @return list of plan period styles matching the criteria
	 */
	List<PlanPeriodStyleDto> getPlanPeriodStyles(PlanPeriodStylesByCriteriaQuery query);

	/**
	 * Gets plan period style variants for a plan period style
	 * 
	 * @param planPeriodStyleId
	 *            ID of the plan period style
	 * @return sorted list of plan period style variants for the given plan period style
	 */
	List<PlanPeriodStyleVariantDto> getPlanPeriodStyleVariants(PlanPeriodStyleId planPeriodStyleId);
}
