package puc.plm.service.api;

import puc.plm.service.domains.style.command.commands.AddStyleVariantCommand;
import puc.plm.service.domains.style.command.commands.CreateStyleCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleDataCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleVariantDataCommand;
import puc.plm.service.domains.style.ids.StyleVariantId;

/**
 * API service for creating and manipulating styles
 */
public interface ApiStyleCommandService {

	/**
	 * Creates a style
	 * 
	 * @param command
	 */
	public void createStyle(CreateStyleCommand command);

	/**
	 * Adds a new style variant to a style
	 * 
	 * @param command
	 * @return id of the newly added style variant
	 */
	public StyleVariantId addStyleVariantToStyle(AddStyleVariantCommand command);

	/**
	 * Updates a style's StyleData
	 * 
	 * @param command
	 */
	public void updateStyleData(UpdateStyleDataCommand command);

	/**
	 * Updates a style variants's StyleVariantData
	 * 
	 * @param command
	 */
	public void updateStyleVariantData(UpdateStyleVariantDataCommand command);
}
