package puc.plm.service.api;

import java.util.List;

import puc.plm.service.domains.supply.ids.SupplierId;
import puc.plm.service.domains.supply.query.dtos.SupplierDto;

/**
 * API service for querying everything supply-related
 */
public interface ApiSupplyQuerySerivce {

	/**
	 * Searches for matching candidate suppliers with a query string
	 * 
	 * @param queryString
	 *            the query string for searching for suppliers
	 * @param maxResults
	 *            maximum number of results
	 * 
	 * @return List of supplier candidates
	 */
	List<SupplierDto> searchSupplierCandidatesForString(String queryString, int maxResults);

	/**
	 * Gets a supplier by it's ID
	 * 
	 * @param supplierId
	 *            ID of the supplier
	 * @return the supplier for the given ID
	 */
	SupplierDto getSupplier(SupplierId supplierId);

}
