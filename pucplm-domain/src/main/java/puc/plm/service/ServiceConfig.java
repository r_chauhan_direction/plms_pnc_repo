package puc.plm.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import puc.plm.service.common.eventhandling.ClassBasedEventPublisher;
import puc.plm.service.common.eventhandling.EventSubscriptionAnnotationScanner;
import puc.plm.service.common.eventhandling.Subscribes;

@Configuration
public class ServiceConfig {

	/**
	 * publisher used for publishing events to event subscribers
	 */
	@Bean
	public ClassBasedEventPublisher eventPublisher() {
		return new ClassBasedEventPublisher();
	}

	/**
	 * scans for {@link Subscribes} annotations and registers them as event subscribers with the
	 * event publisher
	 */
	@Bean
	public EventSubscriptionAnnotationScanner eventSubscriptionAnnotationScanner(
			ClassBasedEventPublisher eventPublisher) {
		return new EventSubscriptionAnnotationScanner(eventPublisher);
	}

}
