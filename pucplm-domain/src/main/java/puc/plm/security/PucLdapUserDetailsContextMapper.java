package puc.plm.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

/**
 * Custom {@link UserDetailsContextMapper} that maps authorities from the P&C-LDAP to relevant app
 * authorities and produces {@link UserDetails} objects.
 */
public class PucLdapUserDetailsContextMapper implements UserDetailsContextMapper {

	/**
	 * Spring Security prefix for user roles
	 */
	private static final String ROLE_PREFIX = "ROLE_";

	/**
	 * LDAP DN of the pucApplication LDAP object that holds the rights relevant for this
	 * application, converted to uppercase (e.g.
	 * CN=PLM,OU=APPLICATIONS,OU=APPLICATIONDATA,DC=P-UND-C,DC=DE)
	 */
	private String pucApplicationDn;

	/**
	 * Creates a custom {@link UserDetailsContextMapper} for the P&C LDAP
	 * 
	 * @param pucApplicationDn
	 *            the LDAP DN of this application (e.g.
	 *            cn=PLM,ou=Applications,ou=ApplicationData,dc=p-und-c,dc=de)
	 */
	public PucLdapUserDetailsContextMapper(String pucApplicationDn) {
		super();
		// convert to uppercase
		this.pucApplicationDn = pucApplicationDn.toUpperCase();
	}

	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx, String username,
			Collection<? extends GrantedAuthority> authorities) {

		// checks if a granted LDAP authority is relevant for this application and maps it to an
		// authority, that is meaningfull for this app (e.g. ROLE_ADMIN or ROLE_BUYER)
		List<GrantedAuthority> userAuthorities = new ArrayList<>();
		for (GrantedAuthority authority : authorities) {
			if (isAppAuthority(authority)) {
				userAuthorities.add(mapToAppAuthority(authority));
			}
		}

		// create user with granted authorities and with erased password
		User user = new User(username, "passwordToBeErased", userAuthorities);
		user.eraseCredentials();
		return user;
	}

	/**
	 * Maps an LDAP authority to an authority that is relevant for this application.
	 * 
	 * @param authority
	 *            a granted LDAP authority
	 * @return a {@link GrantedAuthority} that is meaningfull in the application context
	 */
	private GrantedAuthority mapToAppAuthority(GrantedAuthority authority) {
		// 1. make sure the authority is uppercase
		String authorityUppercase = authority.getAuthority();

		// 2. remove the application dn suffix
		String authorityWithoutApplicationSuffix = authorityUppercase.replaceFirst(getPucApplicationDnSuffix(), "");

		// 3. remove "CN=" strings from authority
		String authorityWithLdapSeparator = authorityWithoutApplicationSuffix.replaceAll("CN=", "");

		// 4. replace the LDAP hierarchy separator "," with "_"
		String authorityWithAppSeparator = authorityWithLdapSeparator.replaceAll(",", "_");

		// 5. add the role prefix
		String authorityRoleName = ROLE_PREFIX + authorityWithAppSeparator;

		// 6. create a GrantedAuthority object from role name
		return new SimpleGrantedAuthority(authorityRoleName);
	}

	/**
	 * Checks if a given authority from the LDAP is relevant for this application
	 * 
	 * @param authority
	 *            LDAP authority to check
	 * @return true, if authority is relevant, else false
	 */
	private boolean isAppAuthority(GrantedAuthority authority) {
		// check if the pucApplicationRight in the authority belongs to the pucApplicationDn
		// LDAP hierarchy
		return authority.getAuthority().endsWith(getPucApplicationDnSuffix());
	}

	/**
	 * @return suffix of the pucApplicationDn
	 */
	private String getPucApplicationDnSuffix() {
		// add leading comma as it is part of the suffix
		return "," + pucApplicationDn;
	}

	@Override
	public void mapUserToContext(UserDetails user, DirContextAdapter ctx) {
		throw new UnsupportedOperationException(
				"PucLdapUserDetailsContextMapper only supports reading from a context.");
	}

}
