package puc.plm.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.search.FilterBasedLdapUserSearch;
import org.springframework.security.ldap.search.LdapUserSearch;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;

/**
 * Defines a configuration for Spring Security with LDAP users
 */
@Configuration
@Profile("users-ldap")
public class SecurityConfigLdapUsers extends WebSecurityConfigurerAdapter {

	/**
	 * Creates an LDAP context source for Spring Security.
	 * 
	 * @param ldapProviderUrl
	 *            the LDAP URL
	 * @return new {@link DefaultSpringSecurityContextSource} configured with the ldapProviderUrl
	 */
	@Bean
	public DefaultSpringSecurityContextSource defaultSpringSecurityContextSource(
			@Value("${ldap.url}") String ldapProviderUrl) {
		return new DefaultSpringSecurityContextSource(ldapProviderUrl);
	}

	/**
	 * Creates a {@link LdapAuthenticationProvider} from an authenticator and an
	 * authoritiesPopulator for a P&C application
	 * 
	 * @param authenticator
	 *            the {@link BindAuthenticator} to use for LDAP authentication checks
	 * @param authoritiesPopulator
	 *            the {@link LdapAuthoritiesPopulator} that reads the app rights from LDAP
	 * @param pucApplicationDn
	 *            the LDAP DN of the pucApplication object used for managing the rights of this
	 *            application
	 * @return a configured {@link LdapAuthenticationProvider} that is able to authenticate against
	 *         the P&C LDAP
	 */
	@Bean
	public LdapAuthenticationProvider ldapAuthenticationProvider(BindAuthenticator authenticator,
			LdapAuthoritiesPopulator authoritiesPopulator, @Value("${ldap.pucApplicationDn}") String pucApplicationDn) {

		LdapAuthenticationProvider provider = new LdapAuthenticationProvider(authenticator, authoritiesPopulator);

		// set custom UserDetailsContextMapper for the P&C LDAP
		UserDetailsContextMapper userDetailsContextMapper = new PucLdapUserDetailsContextMapper(pucApplicationDn);
		provider.setUserDetailsContextMapper(userDetailsContextMapper);

		return provider;

	}

	/**
	 * Creates a LDAP {@link BindAuthenticator} that uses a custom LDAP search query for retrieving
	 * users.
	 * 
	 * @param contextSource
	 * @return a LDAP {@link BindAuthenticator} to use with the P&C LDAP
	 */
	@Bean
	public BindAuthenticator ldapBindAuthenticator(BaseLdapPathContextSource contextSource) {
		BindAuthenticator authenticator = new BindAuthenticator(contextSource);

		// use custom user search in the P&C LDAP
		String ldapUserSearchQuery = "(&(pucUserName={0})(objectClass=pucOrganizationalPerson))";
		String ldapUserSearchBase = "";
		LdapUserSearch userSearch = new FilterBasedLdapUserSearch(ldapUserSearchBase, ldapUserSearchQuery,
				contextSource);
		authenticator.setUserSearch(userSearch);

		return authenticator;
	}

	/**
	 * Creates a {@link DefaultLdapAuthoritiesPopulator} that knows how to read authorities from the
	 * P&C LDAP
	 * 
	 * @param contextSource
	 *            the LDAP context source
	 * @return a {@link DefaultLdapAuthoritiesPopulator} for the P&C LDAP
	 */
	@Bean
	public DefaultLdapAuthoritiesPopulator pucLdapAuthoritiesPopulator(ContextSource contextSource) {

		// search everywhere including subtrees
		DefaultLdapAuthoritiesPopulator populator = new DefaultLdapAuthoritiesPopulator(contextSource, "");
		populator.setSearchSubtree(true);

		// search for pucApplicationRole objects where the user is a member of
		populator.setGroupSearchFilter("(&(member={0})&(objectClass=pucApplicationRole))");

		// take the roles from the pucAppRight attribute(s)
		populator.setGroupRoleAttribute("pucAppRight");

		// leave the pucAppRight unchanged without a role prefix. Mapping to roles must be taken
		// care of in a special PucLdapUserDetailsContextMapper in the LdapAuthenticationProvider
		populator.setRolePrefix("");

		return populator;
	}
}
