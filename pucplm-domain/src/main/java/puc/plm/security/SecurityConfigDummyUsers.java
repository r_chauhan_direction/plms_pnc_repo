package puc.plm.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

/**
 * Defines a configuration for Spring Security with dummy users in an in-memory database
 */
@Configuration
@Profile("users-dummy")
public class SecurityConfigDummyUsers extends WebSecurityConfigurerAdapter {

	@Bean
	public UserDetailsManager userDetailsManager() {

		// dummy passwords are not properly encrypted - ignore deprecation warning
		@SuppressWarnings("deprecation")
		UserBuilder users = User.withDefaultPasswordEncoder();
		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
		manager.createUser(users.username("buyer").password("buyer").roles("BUYER").build());
		manager.createUser(users.username("admin").password("admin").roles("BUYER", "ADMIN").build());
		return manager;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// make /h2-console/** accessible without login
		http.authorizeRequests().antMatchers("/h2-console/**").permitAll().antMatchers("/**").authenticated().and()
				.formLogin().and().httpBasic();

		// disable CSRF-Prevention and frame option for H2 console to function
		http.csrf().disable();
		http.headers().frameOptions().disable();
	}

}
