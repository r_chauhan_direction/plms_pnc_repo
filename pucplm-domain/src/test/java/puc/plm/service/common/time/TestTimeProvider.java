package puc.plm.service.common.time;

import java.time.Instant;

/**
 * {@link TimeProvider} that can freeze the time for testing.
 */
public class TestTimeProvider implements TimeProvider {

	/**
	 * the frozen instant
	 */
	private Instant frozenTime;

	/**
	 * stops the time if not already stopped
	 * 
	 * @return the instant the time is frozen in
	 */
	public Instant freezeTime() {
		if (frozenTime == null) {
			frozenTime = Instant.now();
		}

		return frozenTime;
	}

	/**
	 * resumes the time with the current system time
	 */
	public void resumeWithCurrentSystemTime() {
		frozenTime = null;
	}

	@Override
	public Instant now() {
		if (frozenTime != null) {
			return frozenTime;
		}

		return Instant.now();
	}

}
