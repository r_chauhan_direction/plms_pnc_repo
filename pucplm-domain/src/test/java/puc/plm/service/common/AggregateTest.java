package puc.plm.service.common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.lang.reflect.Constructor;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import puc.plm.service.common.aggregate.Aggregate;
import puc.plm.service.common.messages.Event;
import puc.plm.service.common.time.TestTimeProvider;
import puc.plm.service.common.time.Time;

public class AggregateTest<T extends Aggregate<R, I>, R, I> {

	private T testAggregate;

	private List<Event> actualEvents = new ArrayList<>();

	private int nextEventPosition = 0;

	private Exception actualException;

	private TestTimeProvider timeProvider = new TestTimeProvider();

	public static <T extends Aggregate<R, I>, R, I> AggregateTest<T, R, I> with(Class<T> aggregateClass) {
		return new AggregateTest<T, R, I>(aggregateClass, null, null);
	}

	public static <T extends Aggregate<R, I>, R, I> AggregateTest<T, R, I> withId(Class<T> aggregateClass,
			I aggregateId) {
		return new AggregateTest<T, R, I>(aggregateClass, null, aggregateId);
	}

	public static <T extends Aggregate<R, I>, R, I> AggregateTest<T, R, I> withRoot(Class<T> aggregateClass,
			R aggregateRoot) {
		return new AggregateTest<T, R, I>(aggregateClass, aggregateRoot, null);
	}

	public static <T extends Aggregate<R, I>, R, I> AggregateTest<T, R, I> withRootAndId(Class<T> aggregateClass,
			R aggregateRoot, I aggregateId) {
		return new AggregateTest<T, R, I>(aggregateClass, aggregateRoot, aggregateId);
	}

	private AggregateTest(Class<T> aggregateClass, R aggregateRoot, I aggregateId) {
		testAggregate = createAggregate(aggregateClass, aggregateRoot, aggregateId);
		Time.setTimeProvider(timeProvider);
	}

	@SuppressWarnings("unchecked")
	private T createAggregate(Class<T> aggregateClass, R aggregateRoot, I aggregateId) {
		try {
			Constructor<?>[] constructors = aggregateClass.getConstructors();
			for (Constructor<?> constructor : constructors) {
				Class<?>[] parameterTypes = constructor.getParameterTypes();
				if (parameterTypes.length == 2) {
					boolean isIdFirstParam = aggregateId == null || parameterTypes[0].isInstance(aggregateId);
					boolean isRootSecondParam = aggregateRoot == null || parameterTypes[1].isInstance(aggregateRoot);

					if (isIdFirstParam && isRootSecondParam) {
						return (T) constructor.newInstance(aggregateId, aggregateRoot);
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Error creating test aggregte of type " + aggregateClass, e);
		}
		throw new RuntimeException("No valid constructor found for creating test aggregate of type " + aggregateClass);
	}

	public AggregateTest<T, R, I> when(Consumer<T> aggregateConsumer) {
		clearActualEvents();
		clearActualException();
		try {
			aggregateConsumer.accept(testAggregate);
		} catch (Exception e) {
			this.actualException = e;
		}
		return this;
	}

	public <U> U whenSuccessfull(Function<T, U> aggregateConsumer) {
		clearActualEvents();
		clearActualException();
		return aggregateConsumer.apply(testAggregate);
	}

	private void clearActualException() {
		this.actualException = null;
	}

	private void clearActualEvents() {
		actualEvents.clear();
		nextEventPosition = 0;
	}

	public AggregateTest<T, R, I> expectEvent(Event expectedEvent) {
		Event actualEvent = getNextActualEvent();
		assertEquals(expectedEvent, actualEvent);
		return this;
	}

	public AggregateTest<T, R, I> expectOnlyEvent(Event expectedEvent) {
		expectEvent(expectedEvent);
		expectNoEvents();
		return this;
	}

	public AggregateTest<T, R, I> expectEvents(Event... expectedEvents) {
		for (Event expectedEvent : expectedEvents) {
			expectEvent(expectedEvent);
		}
		return this;
	}

	public AggregateTest<T, R, I> expectOnlyEvents(Event... expectedEvents) {
		expectEvents(expectedEvents);
		expectNoEvents();
		return this;
	}

	public void expectNoEvents() {
		Event nextActualEvent = getNextActualEvent();
		assertNull(nextActualEvent, "At least one more event emitted: " + nextActualEvent);
	}

	public AggregateTest<T, R, I> expectException(Class<? extends Exception> expectedExceptionClass) {
		assertNotNull(actualException, "Missing expected exception of type " + expectedExceptionClass);
		assertEquals(expectedExceptionClass, actualException.getClass());
		return this;
	}

	private Event getNextActualEvent() {
		expectNoException();
		actualEvents.addAll(testAggregate.getAndClearUnpublishedEvents());

		// check if next event exists
		if (actualEvents.size() <= nextEventPosition) {
			return null;
		}

		return actualEvents.get(nextEventPosition++);
	}

	public AggregateTest<T, R, I> expectAggregateState(R expectedAggregateState) {
		expectNoException();
		R rootEntity = testAggregate.getRootEntity();
		assertEquals(expectedAggregateState, rootEntity);
		return this;
	}

	public AggregateTest<T, R, I> expectAggregateState(Consumer<R> aggregateStateChecker) {
		expectNoException();
		R rootEntity = testAggregate.getRootEntity();
		aggregateStateChecker.accept(rootEntity);
		return this;
	}

	public AggregateTest<T, R, I> given(Event appliedEvent) {
		testAggregate.apply(appliedEvent);
		testAggregate.getAndClearUnpublishedEvents();
		return this;
	}

	public AggregateTest<T, R, I> expectNoException() {
		assertNull(actualException, "exception present " + actualException);
		return this;
	}

	public Instant freezeTime() {
		return timeProvider.freezeTime();
	}

	public void resumeTime() {
		timeProvider.resumeWithCurrentSystemTime();
	}

}
