package puc.plm.service.style.command;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Instant;
import java.util.Optional;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import puc.plm.service.common.AggregateTest;
import puc.plm.service.common.aggregate.AggregateAlreadyInitializedException;
import puc.plm.service.common.aggregate.AggregateUninitializedException;
import puc.plm.service.common.messages.Event;
import puc.plm.service.common.time.Time;
import puc.plm.service.domains.masterdata.ids.BrandId;
import puc.plm.service.domains.masterdata.ids.ProductGroupId;
import puc.plm.service.domains.style.command.Style;
import puc.plm.service.domains.style.command.StyleAggregate;
import puc.plm.service.domains.style.command.commands.AddStyleVariantCommand;
import puc.plm.service.domains.style.command.commands.CreateStyleCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleDataCommand;
import puc.plm.service.domains.style.command.commands.UpdateStyleVariantDataCommand;
import puc.plm.service.domains.style.command.events.StyleCreatedEvent;
import puc.plm.service.domains.style.command.events.StyleDataUpdatedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantAddedEvent;
import puc.plm.service.domains.style.command.events.StyleVariantDataUpdatedEvent;
import puc.plm.service.domains.style.data.StyleData;
import puc.plm.service.domains.style.data.StyleVariantData;
import puc.plm.service.domains.style.ids.StyleId;
import puc.plm.service.domains.style.ids.StyleVariantId;

public class AStyleAggregate {

	@Nested
	public static class WhenCreating {

		@Test
		public void shouldEmitStyleCreatedEvent() {

			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();

			CreateStyleCommand createStyleCommand = new CreateStyleCommand(styleId, productGroupId, brandId,
					Optional.of(styleData));

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			Instant frozenTime = test.freezeTime();
			test.when(a -> a.createStyle(createStyleCommand));

			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId, styleData,
					frozenTime);
			test.expectEvent(styleCreatedEvent);
		}

		@Test
		public void shouldUpdateAggregateState() {

			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();

			CreateStyleCommand createStyleCommand = new CreateStyleCommand(styleId, productGroupId, brandId,
					Optional.of(styleData));

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.when(a -> a.createStyle(createStyleCommand));
			test.expectAggregateState(s -> assertEquals(styleId, s.getId()));
			test.expectAggregateState(s -> assertEquals(brandId, s.getBrandId()));
			test.expectAggregateState(s -> assertEquals(productGroupId, s.getProductGroupId()));
			test.expectAggregateState(s -> assertNotNull(s.getCreationTime()));
		}

		@Test
		public void shouldFailForAlreadyInitializedAggregate() {
			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();

			CreateStyleCommand createStyleCommand = new CreateStyleCommand(styleId, productGroupId, brandId,
					Optional.of(styleData));

			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId, styleData,
					Time.now());

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.given(styleCreatedEvent);
			test.when(a -> a.createStyle(createStyleCommand));
			test.expectException(AggregateAlreadyInitializedException.class);
		}

	}

	@Nested
	public static class WhenAddingStyleVariant {

		@Test
		public void shouldEmitStyleVariantAddedEvent() {
			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();
			StyleVariantData styleVariantData = new StyleVariantData();

			AddStyleVariantCommand addStyleVariantCommand = new AddStyleVariantCommand(styleId);
			addStyleVariantCommand.setStyleVariantData(Optional.of(styleVariantData));

			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId, styleData,
					Time.now());

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			Instant frozenTime = test.freezeTime();
			test.given(styleCreatedEvent);
			StyleVariantId styleVariantId = test.whenSuccessfull(a -> a.addStyleVariant(addStyleVariantCommand));

			StyleVariantAddedEvent styleVariantAddedEvent = new StyleVariantAddedEvent(styleId, styleVariantId,
					styleVariantData, frozenTime);
			test.expectEvent(styleVariantAddedEvent);

		}

		@Test
		public void shouldUpdateAggregateState() {

			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();
			StyleVariantData styleVariantData = new StyleVariantData();

			AddStyleVariantCommand addStyleVariantCommand = new AddStyleVariantCommand(styleId);
			addStyleVariantCommand.setStyleVariantData(Optional.of(styleVariantData));

			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId, styleData,
					Time.now());

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.given(styleCreatedEvent);
			test.when(a -> a.addStyleVariant(addStyleVariantCommand));
			test.expectAggregateState(s -> assertEquals(1, s.getStyleVariants().size()));
		}

		@Test
		public void shouldFailForUninitializedAggregate() {
			StyleId styleId = new StyleId();
			AddStyleVariantCommand addStyleVariantCommand = new AddStyleVariantCommand(styleId);

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.when(a -> a.addStyleVariant(addStyleVariantCommand));
			test.expectException(AggregateUninitializedException.class);

		}
	}

	@Nested
	public static class WhenUpdatingStyleData {

		@Test
		public void shouldEmitStyleDataUpdatedEvent() {
			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData initialStyleData = new StyleData();
			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId,
					initialStyleData, Time.now());

			StyleData updatedStyleData = new StyleData();
			updatedStyleData.setStyleName("test style name");

			UpdateStyleDataCommand updateStyleDataCommand = new UpdateStyleDataCommand(styleId, updatedStyleData);
			assertNotEquals(initialStyleData, updatedStyleData);

			Event styleVariantDataUpatedEvent = new StyleDataUpdatedEvent(styleId, updatedStyleData);

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.given(styleCreatedEvent);
			test.when(a -> a.handle(updateStyleDataCommand));
			test.expectEvent(styleVariantDataUpatedEvent);
		}

	}

	@Nested
	public static class WhenUpdatingStyleVariantData {

		@Test
		public void shouldEmitStyleVariantDataUpdatedEvent() {
			StyleId styleId = new StyleId();
			ProductGroupId productGroupId = new ProductGroupId(1234);
			BrandId brandId = new BrandId(4321);
			StyleData styleData = new StyleData();
			StyleCreatedEvent styleCreatedEvent = new StyleCreatedEvent(styleId, productGroupId, brandId, styleData,
					Time.now());

			StyleVariantId styleVariantId = new StyleVariantId();
			StyleVariantData initialStyleVariantData = new StyleVariantData();
			StyleVariantAddedEvent styleVariantAddedEvent = new StyleVariantAddedEvent(styleId, styleVariantId,
					initialStyleVariantData, Time.now());

			StyleVariantData updatedStyleVariantData = new StyleVariantData();
			updatedStyleVariantData.setVariantDescription("updated");
			UpdateStyleVariantDataCommand updateStyleVariantDataCommand = new UpdateStyleVariantDataCommand(
					styleVariantId, updatedStyleVariantData);

			assertNotEquals(updatedStyleVariantData, initialStyleVariantData);

			Event styleVariantDataUpatedEvent = new StyleVariantDataUpdatedEvent(styleId, styleVariantId,
					updatedStyleVariantData);

			AggregateTest<StyleAggregate, Style, StyleId> test = AggregateTest.withId(StyleAggregate.class, styleId);
			test.given(styleCreatedEvent);
			test.given(styleVariantAddedEvent);
			test.when(a -> a.handle(updateStyleVariantDataCommand));
			test.expectEvent(styleVariantDataUpatedEvent);
		}
	}
}
