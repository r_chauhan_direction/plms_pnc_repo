package com.dss.service;

import java.util.List;

import com.dss.model.Theme;
import com.dss.model.ThemeOverviewSearchCriteria;

public interface ThemeService {

	List<Theme> findAll();

	List<Theme> findThemeOverviewBySearchCriteria(ThemeOverviewSearchCriteria themeOverviewSearchCriteria);
}
