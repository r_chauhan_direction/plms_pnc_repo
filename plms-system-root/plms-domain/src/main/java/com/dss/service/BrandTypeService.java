package com.dss.service;

import java.util.List;

import com.dss.model.BrandType;

public interface BrandTypeService {
List<BrandType> findAll();
}
