package com.dss.service;

import java.util.List;

import com.dss.model.FashionLevel;

public interface FashionLevelService {

	List<FashionLevel> findAll();
}
