package com.dss.service;

import java.util.List;

import com.dss.model.Season;

public interface SeasonService {

	List <Season> findAll();
}
