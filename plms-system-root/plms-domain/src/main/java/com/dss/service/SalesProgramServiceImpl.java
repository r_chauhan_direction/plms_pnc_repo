package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.SalesProgramDao;
import com.dss.entity.SalesProgramEntity;
import com.dss.model.SalesProgram;

@Service("salesProgramService")
public class SalesProgramServiceImpl implements SalesProgramService{
  
  @Autowired
  SalesProgramDao salesProgramDao;

  @Override
  public List<SalesProgram> findAll() {
  
  List<SalesProgram> listOfSalesProgram=new ArrayList<>();
    
  for (SalesProgramEntity salesProgramEntity : this.salesProgramDao.findAll()) {
    listOfSalesProgram.add(salesProgramEntity.convertToDto(new SalesProgram()));
  }
    return listOfSalesProgram;
  }

}
