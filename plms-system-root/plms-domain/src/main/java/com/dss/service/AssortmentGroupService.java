package com.dss.service;

import java.util.List;

import com.dss.model.AssortmentGroup;

public interface AssortmentGroupService {

	List <AssortmentGroup> findAll();
}
