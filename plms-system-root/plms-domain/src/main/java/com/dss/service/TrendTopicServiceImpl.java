package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.TrendTopicDao;
import com.dss.entity.TrendTopicEntity;
import com.dss.model.TrendTopic;

@Service("trendTopicService")
public class TrendTopicServiceImpl implements TrendTopicService {

  @Autowired
  TrendTopicDao trendTopicDao;
  
	@Override
	public List<TrendTopic> findAll() {
		List<TrendTopic> trendTopicList =  new ArrayList<>();
		 
		 
for (TrendTopicEntity trendTopicEntity : trendTopicDao.findAll()) {
  trendTopicList.add(trendTopicEntity.convertToDto(new TrendTopic()));
}
		return trendTopicList;
	}

}
