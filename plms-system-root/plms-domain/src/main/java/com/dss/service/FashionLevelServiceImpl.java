
package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.FashionLevelDao;
import com.dss.entity.FashionLevelEntity;
import com.dss.model.FashionLevel;

@Service("fashionLevelService")
public class FashionLevelServiceImpl implements FashionLevelService {

  @Autowired
  FashionLevelDao fashionLevelDao;

  @Override
  public List<FashionLevel> findAll() {
    List<FashionLevel> fashionLevelList = new ArrayList<>();

    for (FashionLevelEntity fashionLevelEntity : fashionLevelDao.findAll()) {
      fashionLevelList.add(fashionLevelEntity.converToDto(new FashionLevel()));
    }

    return fashionLevelList;
  }

}
