package com.dss.service;

import java.util.List;

import com.dss.model.StyleMaster;
import com.dss.model.StyleOverviewSearchCriteria;

public interface StyleMasterService {

	List<StyleMaster>findAll();
	
	List<StyleMaster>findStyleMasterBySearchCriteria(StyleOverviewSearchCriteria styleOverviewSearchCriteria);
}
