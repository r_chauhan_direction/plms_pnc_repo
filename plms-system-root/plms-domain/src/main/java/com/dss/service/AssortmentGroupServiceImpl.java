package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.AssortmentGroupDao;
import com.dss.entity.AssortmentGroupEntity;
import com.dss.model.AssortmentGroup;
 

@Service("assortmentGroupService")
public class AssortmentGroupServiceImpl implements AssortmentGroupService {
	
	@Autowired
	AssortmentGroupDao assortmentGroupDao;

	@Override
	public List<AssortmentGroup> findAll() {
		
		List<AssortmentGroupEntity> asssortmentGroupEntityList = this.assortmentGroupDao.findAll();
		
		List<AssortmentGroup> assortmentGroupList = new ArrayList<>();
		
		for (AssortmentGroupEntity asssortmentGroupEntity : asssortmentGroupEntityList) {
			AssortmentGroup asssortmentGroup=new AssortmentGroup();
			assortmentGroupList.add(asssortmentGroupEntity.convertToDto(asssortmentGroup));
		}

		/*assortmentGroupList.add(new AssortmentGroup(21221l, "Haka Hemden"));
		assortmentGroupList.add(new AssortmentGroup(33335l, "DOB Coordinates"));
		assortmentGroupList.add(new AssortmentGroup(21218l, "HAKA Marken übergreifend"));
		assortmentGroupList.add(new AssortmentGroup(21835l, "HAKA Gürtel"));
		assortmentGroupList.add(new AssortmentGroup(21221l, "Haka Hemden"));
		assortmentGroupList.add(new AssortmentGroup(33333l, "DOB Exquisit"));
		assortmentGroupList.add(new AssortmentGroup(43830l, "DOB Wäsche"));*/
			 
		return assortmentGroupList;
	}

}
