
package com.dss.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.StyleMasterDao;
import com.dss.entity.StyleMasterEntity;
import com.dss.model.StyleMaster;
import com.dss.model.StyleOverviewSearchCriteria;

@Service("styleMasterService")
public class StyleMasterServiceImpl implements StyleMasterService {

  @Autowired
  StyleMasterDao styleMasterDao;

  @Override
  public List<StyleMaster> findAll() {
    List<StyleMaster> styleList = new ArrayList<>();

    for (StyleMasterEntity styleMasterEntity : styleMasterDao.findAll()) {
      styleList.add(styleMasterEntity.convertToDto(new StyleMaster()));
    }

    return styleList;
  }

  @Override
  public List<StyleMaster> findStyleMasterBySearchCriteria(
      StyleOverviewSearchCriteria styleOverviewSearchCriteria) {
    List<StyleMasterEntity> listOfStyleMasterEntity = styleMasterDao
        .findStyleMasterBySearchCriteria(styleOverviewSearchCriteria);

    return convertToStyleMasterDtos(listOfStyleMasterEntity);
  }

  private List<StyleMaster> convertToStyleMasterDtos(
      List<StyleMasterEntity> listOfStyleMasterEntity) {
    List<StyleMaster> styleList = new ArrayList<>();
    Map<Long, StyleMaster> mapOfStyleDtosByStyleNo = new HashMap<>();
    for (StyleMasterEntity styleMasterEntity : listOfStyleMasterEntity) {
      Long styleNo = styleMasterEntity.getStyleNumber();
      StyleMaster styleMaster = null;
      if (!mapOfStyleDtosByStyleNo.containsKey(styleNo)) {
        styleMaster = new StyleMaster();
        styleList.add(styleMasterEntity.convertToDto(styleMaster));
        styleMaster.setSeasons(styleMasterEntity.getSeasonEntity().getSeasonCode().toString());
        styleMaster.setNumberOfColors(styleMasterEntity.getListOfArticleEntity() != null
            ? styleMasterEntity.getListOfArticleEntity().size()
            : null);
        mapOfStyleDtosByStyleNo.put(styleNo, styleMaster);
      } else {
        styleMaster = mapOfStyleDtosByStyleNo.get(styleNo);
        if (styleMaster.getSeasons() != null) {
          styleMaster.setSeasons(
              styleMaster.getSeasons() + ";" + styleMasterEntity.getSeasonEntity().getSeasonCode());
        }
      }
    }
    return styleList;
  }

}
