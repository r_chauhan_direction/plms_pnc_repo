
package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.BrandDao;
import com.dss.entity.BrandEntity;
import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;

@Service("brandService")
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandDao brandDao;

	@Override
	public List<Brand> findAll() {
		List<BrandEntity> brandEntityList = this.brandDao.findAll();
		List<Brand> brandList = new ArrayList<>();
		for (BrandEntity brandEntity : brandEntityList) {
			Brand brand = new Brand();
			brandList.add(brandEntity.convertToDto(brand));
		}
		return brandList;

	}

	@Override
	public List<Brand> findBrandsByBrandTypeId(Long brandTypeId) {
		List<BrandEntity> brandEntityList = this.brandDao.findBrandsByBrandTypeId(brandTypeId);
		List<Brand> brandList = new ArrayList<>();
		for (BrandEntity brandEntity : brandEntityList) {
			Brand brand = new Brand();
			brandList.add(brandEntity.convertToDto(brand));
		}
		return brandList;
	}

}
