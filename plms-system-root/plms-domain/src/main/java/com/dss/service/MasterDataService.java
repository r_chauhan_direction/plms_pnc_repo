package com.dss.service;

import java.util.List;

import com.dss.model.Brand;
import com.dss.model.Season;

public interface MasterDataService {
	
	List<Brand> findAllActiveBrands();
	List<Season> findAllSeasons();

}
