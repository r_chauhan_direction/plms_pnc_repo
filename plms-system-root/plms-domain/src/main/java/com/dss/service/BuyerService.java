package com.dss.service;

import java.util.List;

import com.dss.model.Buyer;

public interface BuyerService {

	List<Buyer> findAll();
}
