package com.dss.service;

import java.util.List;

import com.dss.model.SalesProgram;

public interface SalesProgramService {
List<SalesProgram> findAll();
}
