package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.SeasonDao;
import com.dss.entity.SeasonEntity;
import com.dss.model.Season;

@Service("seasonService")
public class SeasonServiceImpl implements SeasonService {

	@Autowired
	SeasonDao seasonDao;
	
	@Override
	public List<Season> findAll() {
		ArrayList<Season> seasonList =  new ArrayList<>();
		
		for (SeasonEntity seasonEntity :seasonDao.findAll()  ) {
			seasonList.add(seasonEntity.convertToDto(new Season()));
		}
		/*seasonList.add(new Season(45l));
		seasonList.add(new Season(46l));
		seasonList.add(new Season(47l));
		seasonList.add(new Season(48l));
		seasonList.add(new Season(49l));
		seasonList.add(new Season(50l));
		seasonList.add(new Season(51l));
		seasonList.add(new Season(52l));*/
		return seasonList;
	}

}
