package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.MasterDataDao;
import com.dss.entity.BrandEntity;
import com.dss.entity.SeasonEntity;
//import com.dss.dao.MasterDataDao;
import com.dss.model.Brand;
import com.dss.model.Season;

@Service
public class MasterDataServiceImpl implements MasterDataService {

	/*
	 * @PersistenceContext private EntityManager entityManager;
	 */
	@Autowired
	private MasterDataDao masterDataDao;

	@Override
	public List<Brand> findAllActiveBrands() {
		// System.out.println("entitymanager"+entityManager.toString());
		System.out.println("in matserDao");
		List<BrandEntity> allBrandEntitiy = masterDataDao.findAllBrands();

		List<Brand> brandList = new ArrayList<>();
		for (final Object obj : allBrandEntitiy) {
			Brand brand = new Brand();
			final BrandEntity brandEntity = (BrandEntity) obj;
			brandList.add(brandEntity.convertToDto(brand));
			 System.out.println(brandEntity.getBrandDesc());

		}

		System.out.println("! ALERT - No Employees Are Present In The 'Employee' Table !");

		// return masterDataDao.findAllBrands();
		return brandList;
	}

	
	@Override
	public List<Season> findAllSeasons() {
		// System.out.println("entitymanager"+entityManager.toString());
		System.out.println("in matserDao");
		List<SeasonEntity> allSeasonEntities = masterDataDao.findAllSeasons();

		List<Season> seasonList = new ArrayList<>();
		for (final SeasonEntity seasonEntity : allSeasonEntities) {
			Season season = new Season();
//			final SeasonEntity seasonEntity =  seasonEntity;
		seasonList.add(seasonEntity.convertToDto(season));
			 System.out.println(seasonEntity.getSeasonCode());

		}
		System.out.println("! ALERT - No Employees Are Present In The 'Employee' Table !");
		return seasonList;
	}

	
}
