
package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.ThemeDao;
import com.dss.entity.ThemeEntity;
import com.dss.model.Theme;
import com.dss.model.ThemeOverviewSearchCriteria;

@Service("themeService")
public class ThemeServiceImpl implements ThemeService {

  @Autowired
  ThemeDao themeDao;

  @Override
  public List<Theme> findAll() {
    List<Theme> themeList = new ArrayList<>();

    for (ThemeEntity themeEntity : themeDao.findAll()) {
      themeList.add(themeEntity.convertToDto(new Theme()));
    }
    return themeList;
  }

  @Override
  public List<Theme> findThemeOverviewBySearchCriteria(
      ThemeOverviewSearchCriteria themeOverviewSearchCriteria) {
    List<ThemeEntity> listOfThemeEntity = themeDao
        .findThemeOverviewBySearchCriteria(themeOverviewSearchCriteria);
    return convertToThemeDtos(listOfThemeEntity);
  }

  private List<Theme> convertToThemeDtos(List<ThemeEntity> listOfThemeEntity) {
    List<Theme> themeList = new ArrayList<>();
    for (ThemeEntity themeEntity : listOfThemeEntity) {
      themeList.add(themeEntity.convertToDto(new Theme()));
    }
    return themeList;
  }
}
