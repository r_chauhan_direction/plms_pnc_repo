
package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.BuyingDirectorDao;
import com.dss.entity.AssortmentGroupEntity;
import com.dss.entity.BuyingDirectorEntity;
import com.dss.model.AssortmentGroup;
import com.dss.model.BuyingDirector;

@Service("buyingDirectorService")
public class BuyingDirectorServiceImpl implements BuyingDirectorService {

	@Autowired
	BuyingDirectorDao buyingDirectorDao;

	@Override
	public List<BuyingDirector> findAll() {
		List<BuyingDirector> buyingDirectorList = new ArrayList<>();

		for (BuyingDirectorEntity buyingDirectorEntity : buyingDirectorDao.findAll()) {
			buyingDirectorList.add(buyingDirectorEntity.converToDto(new BuyingDirector()));
		}
		return buyingDirectorList;
	}

	@Override
	public List<AssortmentGroup> findAssortmentGroupByBuyingDirectorId(Long buyingDirectorId) {
		List<AssortmentGroupEntity> assortmentGroupEntityList = this.buyingDirectorDao
				.findAssortmentGroupByBuyingDirectorId(buyingDirectorId);

		List<AssortmentGroup> assortmentGroupList = new ArrayList<>();

		for (AssortmentGroupEntity asssortmentGroupEntity : assortmentGroupEntityList) {
			AssortmentGroup asssortmentGroup = new AssortmentGroup();
			assortmentGroupList.add(asssortmentGroupEntity.convertToDto(asssortmentGroup));
		}
		return assortmentGroupList;
	}
}
