package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dss.model.Menu;
import com.dss.model.SubMenu;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	

	private Menu masterData = new Menu(1l, "Master Data", getSubMenuListForMasterData());
	private Menu styles = new Menu(2l, "Styles", getSubMenuListForStyleOverview());
	private Menu linelist = new Menu(3l, "Linelist", getSubMenuListForStyleOverview());
	private Menu quantity_calculation = new Menu(4l, "Quantity calculation ", null);
	private Menu settings = new Menu(5l, "Settings", getSubMenuListForMasterData());

	public List<Menu> findAll() {

		List<Menu> menuList = new ArrayList<Menu>();

	

		menuList.add(masterData);
		menuList.add(styles);
		menuList.add(linelist);
		menuList.add(quantity_calculation);
		menuList.add(settings);

		return menuList;
	}
	
	public List<SubMenu> getSubMenuListForMasterData(){
		List<SubMenu> subMenuForMasterdataList = new ArrayList<SubMenu>();
		subMenuForMasterdataList.add(new SubMenu(3l, "Size", "menu/masterdata/masterdata.zul"));
		subMenuForMasterdataList.add(new SubMenu(3l, "Sizerange", "menu/masterdata/masterdata.zul"));
		subMenuForMasterdataList.add(new SubMenu(3l, "AssortmentGroup", "menu/masterdata/masterdata.zul"));
	
		return subMenuForMasterdataList;
	}

	public List<SubMenu> getSubMenuListForStyleOverview() {
		List<SubMenu> subMenuList = new ArrayList<SubMenu>();
		SubMenu overviewSubMenu = new SubMenu(1l, "Overview", "menu/styles/styleoverview.zul");
		SubMenu supplierStylesSubMenu = new SubMenu(2l, "Supplier styles", "menu/supplierstyles/supplierstyle.zul");
		SubMenu themeSubMenu =new SubMenu(4l, "Theme", "menu/themes/themes.zul");
		SubMenu bulkCreateSubMenu = new SubMenu(3l, "Bulk create", "menu/masterdata/masterdata.zul");
		
		subMenuList.add(overviewSubMenu);
		subMenuList.add(supplierStylesSubMenu);
		subMenuList.add(bulkCreateSubMenu);
		subMenuList.add(themeSubMenu);

		return subMenuList;
	}
}
