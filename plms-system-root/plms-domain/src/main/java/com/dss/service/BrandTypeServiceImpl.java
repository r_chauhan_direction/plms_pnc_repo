package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.BrandTypeDao;
import com.dss.entity.BrandTypeEntity;
import com.dss.model.BrandType;

@Service("brandTypeService")
public class BrandTypeServiceImpl implements BrandTypeService{

  
  @Autowired
  BrandTypeDao brandTypeDao;
  
  @Override
  public List<BrandType> findAll() {
    
    List<BrandType> listOfBrandType=new ArrayList<>();
    
    for (BrandTypeEntity brandTypeEntity : brandTypeDao.findAll()) {
      listOfBrandType.add(brandTypeEntity.convertToDto(new BrandType()));
    }
    return listOfBrandType;
  }

}
