
package com.dss.service;

import java.util.List;

import com.dss.model.AssortmentGroup;
import com.dss.model.BuyingDirector;

public interface BuyingDirectorService {

  public List<BuyingDirector> findAll();

  public List<AssortmentGroup> findAssortmentGroupByBuyingDirectorId(Long buyingDirectorId);
}
