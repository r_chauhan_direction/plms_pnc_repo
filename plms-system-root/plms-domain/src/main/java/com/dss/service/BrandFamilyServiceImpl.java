package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dss.dao.BrandFamilyDao;
import com.dss.entity.BrandFamilyEntity;
import com.dss.model.BrandFamily;

@Service("brandFamilyService")
public class BrandFamilyServiceImpl implements BrandFamilyService{
  
  @Autowired
  BrandFamilyDao brandFamilyDao;
  

  @Override
  public List<BrandFamily> findAll() {
   
    List<BrandFamily> listOfBrandFamily=new ArrayList<>();
    
  for (BrandFamilyEntity brandFamilyEntity : this.brandFamilyDao.findAll()) {
    listOfBrandFamily.add(brandFamilyEntity.convertToDto(new BrandFamily()));
  }
    return listOfBrandFamily;
  }

}
