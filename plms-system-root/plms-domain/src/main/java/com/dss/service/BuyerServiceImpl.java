package com.dss.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.dss.model.Buyer;

@Service
public class BuyerServiceImpl implements BuyerService {

	@Override
	public List<Buyer> findAll() {
		List<Buyer> buyerList =  new ArrayList<>();
		buyerList.add(new Buyer("EKL1", "Herr Kessin"));
		buyerList.add(new Buyer("EKL1", "Frau Helten"));
		buyerList.add(new Buyer("EKL1", " Frau Merkewitsch"));
		buyerList.add(new Buyer("EKL1", " Frau von Brockhusen"));
		buyerList.add(new Buyer("EKL1", " Frau Kletzin"));
		buyerList.add(new Buyer("EKL1", "Herr Zimdars"));
		buyerList.add(new Buyer("EKL1", " Herr Ballin"));
		buyerList.add(new Buyer("EKL1", "Frau Kletzin"));

		return buyerList;
	}

}
