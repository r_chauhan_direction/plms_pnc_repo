package com.dss.service;

import java.util.List;

import com.dss.model.Brand;

public interface BrandService {
	List<Brand> findAll();

	List<Brand> findBrandsByBrandTypeId(Long brandTypeId);
}
