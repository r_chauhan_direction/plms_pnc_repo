//package com.dss.config;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.sql.DataSource;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ComponentScans;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
///*
//@Configuration
//@EnableTransactionManagement
//@ComponentScans(value = { @ComponentScan("com.dss.dao"), @ComponentScan("com.dss.service") })
//public class AppConfig {
//
//	@Bean
//	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//		LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
//		emf.setDataSource(dataSource());
//		emf.setPersistenceUnitName("PLMS_PERSISTENCE_UNIT");
//		return emf;
//	}
//
//	@Bean
//	public DataSource dataSource() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName("org.postgresql.Driver");
//		dataSource.setUrl("jdbc:postgresql://172.16.1.45:5432/PLMS");
//		dataSource.setUsername("plms_app");
//		dataSource.setPassword("plms_prod");
//		return dataSource;
//	}
//
//	@Bean
//	public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
//		JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(emf);
//		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
//		return transactionManager;
//	}*/
//
///*
// * @Bean public PersistenceExceptionTranslationPostProcessor
// * exceptionTranslation() { return new
// * PersistenceExceptionTranslationPostProcessor(); }
// */
//
///*
// * Properties additionalProperties() { Properties properties = new Properties();
// * properties.setProperty("hibernate.hbm2ddl.auto", "update");
// * properties.setProperty("hibernate.dialect",
// * "org.hibernate.dialect.PostgreSQLDialect"); return properties; }
// */
//
////@PropertySource("classpath:application.properties")
//
//@Configuration
//@EnableTransactionManagement
//@ComponentScans(value = { @ComponentScan("com.dss.dao"), @ComponentScan("com.dss.service") })
//public class AppConfig {
//
//	/*@PersistenceContext
//	EntityManagerFactory emf;*/
//
//	@Bean
//    public EntityManagerFactory entityManagerFactory()   {
//
//        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//        vendorAdapter.setGenerateDdl(true);
//
//        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
//        factory.setJpaVendorAdapter(vendorAdapter);
//        factory.setPackagesToScan("com.dss");
//        factory.setDataSource(dataSource());
//        factory.afterPropertiesSet();
//
//        return factory.getObject();
//    }
//
//    @Bean
//    public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {
//        return entityManagerFactory.createEntityManager();
//    }
//
//  @Bean
//    JpaTransactionManager transactionManager() {
//        JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(entityManagerFactory());
//        return transactionManager;
//    }
//
//	@Bean
//	public DataSource dataSource() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName("org.postgresql.Driver");
//		dataSource.setUrl("jdbc:postgresql://172.16.1.45:5432/PLMS");
//		dataSource.setUsername("plms_app");
//		dataSource.setPassword("plms_prod");
//		return dataSource;
//	}
//
//	/*@Bean
//	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
//		JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(emf);
//
//		return transactionManager;
//	}*/
//
//	/*@Bean
//	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
//		return new PersistenceExceptionTranslationPostProcessor();
//	}
//
//	Properties additionalProperties() {
//		Properties properties = new Properties();
//		properties.setProperty("hibernate.hbm2ddl.auto", "update");
//		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
//
//		return properties;
//	}*/
//
//}
