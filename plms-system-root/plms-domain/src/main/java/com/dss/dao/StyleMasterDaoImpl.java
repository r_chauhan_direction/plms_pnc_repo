
package com.dss.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.dss.entity.StyleMasterEntity;
import com.dss.model.StyleOverviewSearchCriteria;

@Transactional
@Repository
public class StyleMasterDaoImpl implements StyleMasterDaoCustom {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<StyleMasterEntity> findStyleMasterBySearchCriteria(
      StyleOverviewSearchCriteria styleOverviewSearchCriteria) {
    final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
    final CriteriaQuery<StyleMasterEntity> criteriaQuery = criteriaBuilder
        .createQuery(StyleMasterEntity.class);
    final Root<StyleMasterEntity> root = criteriaQuery.from(StyleMasterEntity.class);
    criteriaQuery.select(root);
    if (styleOverviewSearchCriteria != null) {
      List<Predicate> predicates = new ArrayList<Predicate>();
      
      if(styleOverviewSearchCriteria.getBrandTypeId()!=null)
      {
        predicates.add(criteriaBuilder.equal(root.get("brandEntity").get("brandTypeEntity").get("id"), styleOverviewSearchCriteria.getBrandTypeId()));
      }
      if(styleOverviewSearchCriteria.getBrandId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("brandEntity").get("id"),
            styleOverviewSearchCriteria.getBrandId()));
      }
      if (styleOverviewSearchCriteria.getAssortmentGroupId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("asssortmentGroupEntity").get("id"),
            styleOverviewSearchCriteria.getAssortmentGroupId()));
      }

      if (styleOverviewSearchCriteria.getBuyingDirectorId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("buyingDirectorEntity").get("id"),
            styleOverviewSearchCriteria.getBuyingDirectorId()));
      }

      if (styleOverviewSearchCriteria.getSeasonId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("seasonEntity").get("id"),
            styleOverviewSearchCriteria.getSeasonId()));
      }

      if (styleOverviewSearchCriteria.getStyleNumber() != null) {
        predicates.add(criteriaBuilder.equal(root.get("styleNumber"),
            styleOverviewSearchCriteria.getStyleNumber()));
      }

      if (styleOverviewSearchCriteria.getSalesProgramId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("salesProgramEntity").get("id"),
            styleOverviewSearchCriteria.getSalesProgramId()));
      }

      if (styleOverviewSearchCriteria.getTrendTopicId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("trendTopicEntity").get("id"),
            styleOverviewSearchCriteria.getTrendTopicId()));
      }

      if (styleOverviewSearchCriteria.getThemeDesc() != null) {
        predicates.add(criteriaBuilder.like(root.get("themeEntity").get("themeDesc"),
            "%" + styleOverviewSearchCriteria.getThemeDesc() + "%"));
        predicates.add(criteriaBuilder.like(criteriaBuilder.upper(root.get("themeEntity").get("themeDesc")), "%" + styleOverviewSearchCriteria.getThemeDesc() + "%"));
        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("themeEntity").get("themeDesc")), "%" + styleOverviewSearchCriteria.getThemeDesc() + "%"));
      }

      if (styleOverviewSearchCriteria.getFashionLevelId() != null) {
        predicates.add(criteriaBuilder.equal(root.get("fashionLevelEntity").get("id"),
            styleOverviewSearchCriteria.getFashionLevelId()));
      }
      criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    }
    final TypedQuery<StyleMasterEntity> createQuery = this.entityManager.createQuery(criteriaQuery);

    List<StyleMasterEntity> styleMasterEntityList = createQuery.getResultList();

    return styleMasterEntityList;
  }

}
