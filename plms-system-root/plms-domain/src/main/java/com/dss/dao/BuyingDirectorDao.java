
package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.BuyingDirectorEntity;

public interface BuyingDirectorDao
    extends JpaRepository<BuyingDirectorEntity, Long>, BuyingDirectorDaoCustom {

}
