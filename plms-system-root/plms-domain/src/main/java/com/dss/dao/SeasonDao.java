package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.SeasonEntity;

public interface SeasonDao extends JpaRepository<SeasonEntity, Long>{

}
