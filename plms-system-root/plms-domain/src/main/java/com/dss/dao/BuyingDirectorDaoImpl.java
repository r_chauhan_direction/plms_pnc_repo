
package com.dss.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.dss.entity.AssortmentGroupEntity;
import com.dss.entity.BuyingDirectorEntity;

@Transactional
@Repository
public class BuyingDirectorDaoImpl implements BuyingDirectorDaoCustom {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<AssortmentGroupEntity> findAssortmentGroupByBuyingDirectorId(Long id) {
    final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
    final CriteriaQuery<AssortmentGroupEntity> criteriaQuery = criteriaBuilder
        .createQuery(AssortmentGroupEntity.class);
    final Root<BuyingDirectorEntity> root = criteriaQuery.from(BuyingDirectorEntity.class);
    criteriaQuery.select(root.get("assortmentGroupEntityList"));
    criteriaQuery.where(criteriaBuilder.equal(root.get("id"), id));
    final TypedQuery<AssortmentGroupEntity> createQuery = this.entityManager
        .createQuery(criteriaQuery);
    List<AssortmentGroupEntity> assortmentGroupEntityList = createQuery.getResultList();
    return assortmentGroupEntityList;
  }
}
