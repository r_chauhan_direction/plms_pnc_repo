
package com.dss.dao;

import java.util.List;

import com.dss.entity.AssortmentGroupEntity;

public interface BuyingDirectorDaoCustom {

  List<AssortmentGroupEntity> findAssortmentGroupByBuyingDirectorId(Long buyingDirectorId);
}
