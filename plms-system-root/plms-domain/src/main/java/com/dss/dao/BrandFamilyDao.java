package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dss.entity.BrandFamilyEntity;

@Repository
public interface BrandFamilyDao extends JpaRepository<BrandFamilyEntity, Long>,BrandFamilyDaoCustom{

}
