
package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.ThemeEntity;

public interface ThemeDao extends JpaRepository<ThemeEntity, Long>, ThemeDaoCustom {


}
