
package com.dss.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.dss.entity.ThemeEntity;
import com.dss.model.ThemeOverviewSearchCriteria;

@Transactional
@Repository
public class ThemeDaoImpl implements ThemeDaoCustom {

  @PersistenceContext
  EntityManager entityManager;

  @Override
  public List<ThemeEntity> findThemeOverviewBySearchCriteria(
      ThemeOverviewSearchCriteria themeOverviewSearchCriteria) {
    CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
    CriteriaQuery<ThemeEntity> criteriaQuery = criteriaBuilder.createQuery(ThemeEntity.class);
    Root<ThemeEntity> root = criteriaQuery.from(ThemeEntity.class);
    criteriaQuery.select(root);
    if(themeOverviewSearchCriteria!=null){
      List<Predicate> predicates = new ArrayList<>();
      
      if(themeOverviewSearchCriteria.getBrandTypeId()!=null){
        predicates.add(criteriaBuilder.equal(root.get("brandEntity").get("brandTypeEntity").get("id"), themeOverviewSearchCriteria.getBrandTypeId()));
      }
      if(themeOverviewSearchCriteria.getAssortmentGroupId()!=null){
        predicates.add(criteriaBuilder.equal(root.get("assortmentGroupEntity").get("id"),themeOverviewSearchCriteria.getAssortmentGroupId()));
      }
      criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
    }
    TypedQuery<ThemeEntity> createQuery = this.entityManager.createQuery(criteriaQuery);
    List<ThemeEntity> themeEntityList = createQuery.getResultList();
    return themeEntityList;
  }

}
