package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.FashionLevelEntity;

public interface FashionLevelDao extends JpaRepository<FashionLevelEntity, Long>{

}
