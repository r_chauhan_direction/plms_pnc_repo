package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.StyleMasterEntity;

public interface StyleMasterDao extends JpaRepository<StyleMasterEntity, Long>,StyleMasterDaoCustom{

}
