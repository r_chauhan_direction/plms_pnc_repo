
package com.dss.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.dss.entity.BrandEntity;

@Transactional
@Repository
public class BrandDaoImpl implements BrandDaoCustom {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<BrandEntity> findBrandsByBrandTypeId(Long brandTypeId) {
		final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
		final CriteriaQuery<BrandEntity> criteriaQuery = criteriaBuilder.createQuery(BrandEntity.class);
		final Root<BrandEntity> root = criteriaQuery.from(BrandEntity.class);
		criteriaQuery.select(root);
		criteriaQuery.where(criteriaBuilder.equal(root.get("brandTypeEntity").get("id"), brandTypeId));
		final TypedQuery<BrandEntity> createQuery = this.entityManager.createQuery(criteriaQuery);
		List<BrandEntity> brandEntityList = createQuery.getResultList();
		return brandEntityList;
	}

}
