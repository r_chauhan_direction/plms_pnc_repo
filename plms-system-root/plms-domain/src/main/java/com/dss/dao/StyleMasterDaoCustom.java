package com.dss.dao;

import java.util.List;

import com.dss.entity.StyleMasterEntity;
import com.dss.model.StyleOverviewSearchCriteria;

public interface StyleMasterDaoCustom {

	List<StyleMasterEntity> findStyleMasterBySearchCriteria(StyleOverviewSearchCriteria styleOverviewSearchCriteria);
}
