package com.dss.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.dss.entity.BrandEntity;
import com.dss.entity.SeasonEntity;

@Repository("masterDataDaoImpl")
public class MasterDataDaoImpl implements MasterDataDao {

	@Resource
	private EntityManager entityManager;

	@Override
	public List<BrandEntity> findAllBrands() {
		System.out.println(" entitymanager"+entityManager.toString());
		final List<BrandEntity> brandList= new ArrayList<>();
// CriteriaBuilder criteriaBuilderObj = entityManager.getCriteriaBuilder();
//		// Making The Query Object From The 'CriteriaBuilder' Instance
//		final CriteriaQuery<BrandEntity> queryObj = criteriaBuilderObj.createQuery(BrandEntity.class);
//		final Root<BrandEntity> from = queryObj.from(BrandEntity.class);
//		// Step #1 - Displaying All Records
//		queryObj.select(from);
//		//		final CriteriaQuery selectQuery = queryObj.select(from);
//		//		final TypedQuery<Object> typedQuery = em.createQuery(selectQuery);
//		//		final List<Object> brandList = typedQuery.getResultList();
//		final Query q = entityManager.createQuery(queryObj);
		Query q = entityManager.createQuery("SELECT b FROM BrandEntity b");
//		 final List<BrandEntity> brandEntityList= q.getResultList();
		 //final List<BrandEntity> brandEntityList= q.getResultList();
		//q.getFirstResult();
		System.out.println(" No of brands = "+q.getResultList());
		return brandList;
	}
	
	@Override
	public List<SeasonEntity> findAllSeasons() {
		System.out.println(" entitymanager"+entityManager.toString());
		final List<SeasonEntity> seasonList= new ArrayList<>();
		Query q = entityManager.createQuery("SELECT b FROM SeasonEntity b");
		System.out.println(" No of seasons = "+q.getResultList());
		return seasonList;
	}
}
