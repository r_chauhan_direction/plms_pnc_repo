package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dss.entity.TrendTopicEntity;

@Repository
public interface TrendTopicDao extends JpaRepository<TrendTopicEntity, Long>,TrendTopicDaoCustom{

}
