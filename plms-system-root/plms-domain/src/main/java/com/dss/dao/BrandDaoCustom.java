
package com.dss.dao;

import java.util.List;

import com.dss.entity.BrandEntity;

public interface BrandDaoCustom {

	List<BrandEntity> findBrandsByBrandTypeId(Long brandTypeId);
}
