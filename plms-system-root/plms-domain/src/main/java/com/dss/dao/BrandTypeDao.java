package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dss.entity.BrandTypeEntity;

@Repository
public interface BrandTypeDao extends JpaRepository<BrandTypeEntity, Long>,BrandFamilyDaoCustom{

}
