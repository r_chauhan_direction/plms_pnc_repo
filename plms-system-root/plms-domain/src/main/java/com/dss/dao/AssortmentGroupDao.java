package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dss.entity.AssortmentGroupEntity;

public interface AssortmentGroupDao extends JpaRepository<AssortmentGroupEntity, Long>{

}
