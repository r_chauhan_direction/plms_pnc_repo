package com.dss.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dss.entity.SalesProgramEntity;

@Repository
public interface SalesProgramDao extends JpaRepository<SalesProgramEntity, Long>,SalesProgramDaoCustom{

}
