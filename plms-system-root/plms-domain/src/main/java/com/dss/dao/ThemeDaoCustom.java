
package com.dss.dao;

import java.util.List;

import com.dss.entity.ThemeEntity;
import com.dss.model.ThemeOverviewSearchCriteria;

public interface ThemeDaoCustom {

  List<ThemeEntity> findThemeOverviewBySearchCriteria(
      ThemeOverviewSearchCriteria themeOverviewSearchCriteria);
}
