package com.dss.dao;

import java.util.List;

import com.dss.entity.BrandEntity;
import com.dss.entity.SeasonEntity;

public interface MasterDataDao {
	
	List<BrandEntity> findAllBrands();
	
	List<SeasonEntity> findAllSeasons();

}
