
package com.dss.viewmodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.BrandType;
import com.dss.model.Buyer;
import com.dss.model.BuyingDirector;
import com.dss.model.FashionLevel;
import com.dss.model.SalesProgram;
import com.dss.model.Season;
import com.dss.model.StyleMaster;
import com.dss.model.StyleOverviewSearchCriteria;
import com.dss.model.Theme;
import com.dss.model.TrendTopic;
import com.dss.service.AssortmentGroupService;
import com.dss.service.BrandFamilyService;
import com.dss.service.BrandService;
import com.dss.service.BrandTypeService;
import com.dss.service.BuyerService;
import com.dss.service.BuyingDirectorService;
import com.dss.service.FashionLevelService;
import com.dss.service.SalesProgramService;
import com.dss.service.SeasonService;
import com.dss.service.StyleMasterService;
import com.dss.service.TrendTopicService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class StyleOverviewViewModel {

  @WireVariable
  private BrandService brandService;

  @WireVariable
  private BuyingDirectorService buyingDirectorService;

  @WireVariable
  private SeasonService seasonService;

  @WireVariable
  private AssortmentGroupService assortmentGroupService;

  @WireVariable
  private FashionLevelService fashionLevelService;

  @WireVariable
  private StyleMasterService styleMasterService;

  @WireVariable
  private BuyerService buyerService;

  @WireVariable
  private BrandFamilyService brandFamilyService;

  @WireVariable
  private BrandTypeService brandTypeService;

  @WireVariable
  private TrendTopicService trendTopicService;

  @WireVariable
  private SalesProgramService salesProgramService;

  private BrandType selectedBrandType = new BrandType();
  private BuyingDirector selectedBuyingDirector = new BuyingDirector();
  private Brand selectedBrand = new Brand();
  private Season selectedSeason = new Season();
  private AssortmentGroup selectedAssortmentGroup = new AssortmentGroup();
  private FashionLevel selectedFashionLevel = new FashionLevel();
  private SalesProgram selectedSalesProgram = new SalesProgram();
  private TrendTopic selectedTrandTopic = new TrendTopic();
  private StyleMaster selectedStyle = new StyleMaster();

  private final List<Buyer> buyerList = new ArrayList<>();
  private List<Brand> brandList = new ArrayList<>();;
  private List<BuyingDirector> buyingDirectorList = new ArrayList<>();
  private List<AssortmentGroup> assortmentGroupList = new ArrayList<>();
  private List<Season> seasonList = new ArrayList<>();
  private final List<Theme> themaList = new ArrayList<>();
  private List<FashionLevel> fashionLevelList = new ArrayList<>();
  private List<TrendTopic> trendTopicList = new ArrayList<>();
  private List<SalesProgram> salesProgramList = new ArrayList<>();
  private List<StyleMaster> styleList = new ArrayList<>();
  private List<BrandType> brandTypeList = new ArrayList<>();

  private String enteredStyleNumber;
  private String themeDesc;

  private final ListModelList<String> menuComboModel = new ListModelList<>();
  // private SubMenu currentPage = new SubMenu();

  private String styleDesc;

  private void loadData() {

    this.brandList = this.brandService.findAll();
    this.buyingDirectorList = this.buyingDirectorService.findAll();
    this.seasonList = this.seasonService.findAll();
    this.assortmentGroupList = this.assortmentGroupService.findAll();
    this.fashionLevelList = this.fashionLevelService.findAll();
    this.salesProgramList = this.salesProgramService.findAll();
    this.trendTopicList = this.trendTopicService.findAll();
    this.brandTypeList = this.brandTypeService.findAll();

    this.styleList = this.styleMasterService.findStyleMasterBySearchCriteria(null);
    Collections.sort(this.styleList, (final StyleMaster o1, final StyleMaster o2) -> o1.getBrand()
        .getBrandDesc().compareToIgnoreCase(o2.getBrand().getBrandDesc()));

    this.brandList.add(0, this.selectedBrand);
    this.buyingDirectorList.add(0, this.selectedBuyingDirector);
    this.seasonList.add(0, this.selectedSeason);
    this.assortmentGroupList.add(0, this.selectedAssortmentGroup);
    this.fashionLevelList.add(0, this.selectedFashionLevel);
    this.salesProgramList.add(0, this.selectedSalesProgram);
    this.trendTopicList.add(0, this.selectedTrandTopic);
    this.brandTypeList.add(0, this.selectedBrandType);

  }

  public ListModelList<String> getMenuComboModel() {
    return this.menuComboModel;
  }

  @Command("newContact")
  public void newContact(@BindingParam("contact")
  final String contact) {

    this.menuComboModel.addToSelection(contact);
  }

  @Init
  public void init() {
    loadData();
    this.selectedStyle = this.styleList.get(0);
  }

  public void setSelectedStyle(final StyleMaster selectedStyle) {
    this.selectedStyle = selectedStyle;

    final Map<String, Object> map = new HashMap<>();
    map.put("selectedStyle", selectedStyle);
    BindUtils.postGlobalCommand(null, null, "refreshStyleMaster", map);
  }

  public String getEnteredStyleNumber() {
    return this.enteredStyleNumber;
  }

  public void setEnteredStyleNumber(final String enteredStyleNumber) {
    this.enteredStyleNumber = enteredStyleNumber;
  }

  public BuyingDirector getSelectedBuyingDirector() {
    return this.selectedBuyingDirector;
  }

  public void setSelectedBuyingDirector(final BuyingDirector selectedBuyingDirector) {
    this.selectedBuyingDirector = selectedBuyingDirector;
  }

  public Season getSelectedSeason() {
    return this.selectedSeason;
  }

  public void setSelectedSeason(final Season selectedSeason) {
    this.selectedSeason = selectedSeason;
  }

  public AssortmentGroup getSelectedAssortmentGroup() {
    return this.selectedAssortmentGroup;
  }

  public void setSelectedAssortmentGroup(final AssortmentGroup selectedAssortmentGroup) {
    this.selectedAssortmentGroup = selectedAssortmentGroup;
  }

  public FashionLevel getSelectedFashionLevel() {
    return this.selectedFashionLevel;
  }

  public void setSelectedFashionLevel(final FashionLevel selectedFashionLevel) {
    this.selectedFashionLevel = selectedFashionLevel;
  }

  public void setSelectedBrand(final Brand selectedBrand) {
    this.selectedBrand = selectedBrand;
  }

  public Brand getSelectedBrand() {
    return this.selectedBrand;
  }

  public List<Buyer> getBuyerList() {
    return this.buyerList;
  }

  public StyleMaster getSelectedStyle() {
    return this.selectedStyle;
  }

  public List<Brand> getBrandList() {
    return this.brandList;
  }

  public List<BuyingDirector> getBuyingDirectorList() {
    return this.buyingDirectorList;
  }

  public List<AssortmentGroup> getAssortmentGroupList() {
    return this.assortmentGroupList;
  }

  public List<Season> getSeasonList() {
    return this.seasonList;
  }

  public List<Theme> getThemaList() {
    return this.themaList;
  }

  public List<TrendTopic> getTrendTopicList() {
    return this.trendTopicList;
  }

  public List<StyleMaster> getStyleList() {
    return this.styleList;
  }

  /**
   * @return the salesProgramList
   */
  public List<SalesProgram> getSalesProgramList() {
    return this.salesProgramList;
  }

  /**
   * @return the brandTypeList
   */
  public List<BrandType> getBrandTypeList() {
    return this.brandTypeList;
  }

  public List<FashionLevel> getFashionLevelList() {
    return this.fashionLevelList;
  }

  /**
   * @return the selectedBrandType
   */
  public BrandType getSelectedBrandType() {
    return this.selectedBrandType;
  }

  /**
   * @param selectedBrandType the selectedBrandType to set
   */
  public void setSelectedBrandType(final BrandType selectedBrandType) {
    this.selectedBrandType = selectedBrandType;
  }

  /**
   * @return the selectedSalesProgram
   */
  public SalesProgram getSelectedSalesProgram() {
    return this.selectedSalesProgram;
  }

  /**
   * @return the selectedTrandTopic
   */
  public TrendTopic getSelectedTrandTopic() {
    return this.selectedTrandTopic;
  }

  /**
   * @param selectedSalesProgram the selectedSalesProgram to set
   */
  public void setSelectedSalesProgram(final SalesProgram selectedSalesProgram) {
    this.selectedSalesProgram = selectedSalesProgram;
  }

  /**
   * @param selectedTrandTopic the selectedTrandTopic to set
   */
  public void setSelectedTrandTopic(final TrendTopic selectedTrandTopic) {
    this.selectedTrandTopic = selectedTrandTopic;
  }

  public String getThemeDesc() {
    return this.themeDesc;
  }

  public void setThemeDesc(final String themeDesc) {
    this.themeDesc = themeDesc;
  }

  @Command
  @NotifyChange("*")
  public void search() {
    StyleMaster styleMaster = null;
    final StyleOverviewSearchCriteria styleOverviewSearchCriteria = new StyleOverviewSearchCriteria();
    styleOverviewSearchCriteria
        .setBrandId(this.selectedBrand != null ? this.selectedBrand.getBrandId() : null);
    styleOverviewSearchCriteria.setAssortmentGroupId(this.selectedAssortmentGroup != null
        ? this.selectedAssortmentGroup.getAssortmentGroupId()
        : null);
    styleOverviewSearchCriteria.setBuyingDirectorId(this.selectedBuyingDirector != null
        ? this.selectedBuyingDirector.getBuyingDirectorId()
        : null);
    styleOverviewSearchCriteria.setStyleNumber(
        this.enteredStyleNumber != null ? Long.valueOf(this.enteredStyleNumber) : null);
    styleOverviewSearchCriteria
        .setSeasonId(this.selectedSeason != null ? this.selectedSeason.getSeasonId() : null);

    styleOverviewSearchCriteria.setSalesProgramId(
        this.selectedSalesProgram != null ? this.selectedSalesProgram.getSalesProgramId() : null);
    styleOverviewSearchCriteria.setTrendTopicId(
        this.selectedTrandTopic != null ? this.selectedTrandTopic.getTrendTopicId() : null);
    styleOverviewSearchCriteria.setBrandTypeId(
        this.selectedBrandType != null ? this.selectedBrandType.getBrandTypeId() : null);
    styleOverviewSearchCriteria.setThemeDesc(this.themeDesc != null ? this.themeDesc : null);
    styleOverviewSearchCriteria.setFashionLevelId(
        this.selectedFashionLevel != null ? this.selectedFashionLevel.getFashionLevelId() : null);

    final List<StyleMaster> data = this.styleMasterService
        .findStyleMasterBySearchCriteria(styleOverviewSearchCriteria);
    this.styleList.clear();
    if (data != null && !data.isEmpty()) {
      this.styleList.addAll(data);
      styleMaster = this.styleList.get(0);
    }
    setSelectedStyle(styleMaster);
  }

  @Command
  @NotifyChange(".")
  public void reset() {
    this.selectedBrandType = this.brandTypeList.get(0);
    this.selectedBuyingDirector = this.buyingDirectorList.get(0);
    this.selectedBrand = this.brandList.get(0);
    this.selectedSeason = this.seasonList.get(0);
    this.selectedAssortmentGroup = this.assortmentGroupList.get(0);
    this.selectedFashionLevel = this.fashionLevelList.get(0);
    this.selectedSalesProgram = this.salesProgramList.get(0);
    this.selectedTrandTopic = this.trendTopicList.get(0);
    this.enteredStyleNumber = null;
    this.themeDesc = null;
    search();
  }

  @Command
  @NotifyChange("assortmentGroupList")
  public void onBuyingDirectorSelect() {
    if (this.selectedBuyingDirector != null) {
      final List<AssortmentGroup> listOfAssortmentGroup = this.buyingDirectorService
          .findAssortmentGroupByBuyingDirectorId(getSelectedBuyingDirector().getBuyingDirectorId());
      this.assortmentGroupList = new ArrayList<>(listOfAssortmentGroup);
      if (this.assortmentGroupList != null || !this.assortmentGroupList.isEmpty()) {
        this.assortmentGroupList.add(0, this.selectedAssortmentGroup);
      }
    }

  }

  @GlobalCommand
  @NotifyChange({"selectedStyle"})
  public void refreshStyleMaster(@BindingParam("selectedStyle")
  final StyleMaster selectedStyle) {
    this.selectedStyle = selectedStyle;
  }


  public String getBrandLabel(Brand brand) {
    StringBuilder buffer = new StringBuilder("");
    if (brand.getBrandFamily() != null) {
      buffer.append(brand.getBrandFamily().getBrandFamilyCode().trim()).append(" - ");
    }
    if (brand.getBrandDesc() != null) {
      buffer.append(brand.getBrandDesc().trim());
    }
    return buffer.toString().trim();
  }

  @Command
  @NotifyChange("brandList")
  public void onSelectBrandType() {
    if (this.selectedBrandType != null) {
      final List<Brand> listOfBrand = this.brandService
          .findBrandsByBrandTypeId(getSelectedBrandType().getBrandTypeId());
      this.brandList = new ArrayList<>(listOfBrand);
      if (this.brandList != null || !this.brandList.isEmpty()) {
        this.brandList.add(0, this.selectedBrand);
      }
    }
  }

}
