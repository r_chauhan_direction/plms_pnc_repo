package com.dss.viewmodel;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.AbstractTreeModel;
import org.zkoss.zul.TreeModel;

import com.dss.model.Menu;
import com.dss.service.MenuService;
import com.dss.service.StyleMasterService;
import com.dss.treemodel.BinaryTreeModel;
 

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class SupplierStyleViewModel    {
	 @WireVariable
private	MenuService menuService; 
 
 TreeModel<Menu> menuList;

public TreeModel<Menu> getMenuList() {
	List<Menu> findAll = this.menuService.findAll();
	this.menuList=new BinaryTreeModel<>(new ArrayList<>(findAll));
	return menuList;
}
	 

	 
}
