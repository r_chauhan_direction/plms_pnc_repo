
package com.dss.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.BrandType;
import com.dss.model.Season;
import com.dss.model.Theme;
import com.dss.model.ThemeOverviewSearchCriteria;
import com.dss.service.AssortmentGroupService;
import com.dss.service.BrandFamilyService;
import com.dss.service.BrandService;
import com.dss.service.BrandTypeService;
import com.dss.service.BuyingDirectorService;
import com.dss.service.SeasonService;
import com.dss.service.ThemeService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ThemesOverviewViewModel {

  @WireVariable
  private BrandService brandService;

  @WireVariable
  private BuyingDirectorService buyingDirectorService;

  @WireVariable
  private SeasonService seasonService;

  @WireVariable
  private BrandFamilyService brandFamilyService;

  @WireVariable
  private AssortmentGroupService assortmentGroupService;

  @WireVariable
  private ThemeService themeService;

  private Theme selectedTheme = new Theme();

  private Brand selectedBrand = new Brand();
  private List<Season> seasonList = new ArrayList<>();
  private Season selectedSeason = new Season();
  private List<AssortmentGroup> assortmentGroupList = new ArrayList<>();
  private AssortmentGroup selectedAssortmentGroup = new AssortmentGroup();
  private List<Theme> themeList = new ArrayList<>();
  private List<Brand> brandList = new ArrayList<>();
  private String theme;
  private String style;

  @Init
  public void init() {
    loadData();
    this.selectedTheme = themeList.get(0);
  }

  private void loadData() {
    this.seasonList = this.seasonService.findAll();
    this.brandList = this.brandService.findAll();
    this.assortmentGroupList = this.assortmentGroupService.findAll();

    this.themeList = this.themeService.findThemeOverviewBySearchCriteria(null);
    // Collections.sort(this.themeList, (final Theme o1, final Theme o2) -> o1.getBrand()
    // .getBrandDesc().compareToIgnoreCase(o2.getBrand().getBrandDesc()));

    this.seasonList.add(0, this.selectedSeason);
    this.assortmentGroupList.add(0, this.selectedAssortmentGroup);
    this.brandList.add(0, this.selectedBrand);
  }

  public List<AssortmentGroup> getAssortmentGroupList() {
    return assortmentGroupList;
  }

  public Theme getSelectedTheme() {
    return selectedTheme;
  }

  public Brand getSelectedBrand() {
    return selectedBrand;
  }

  public void setSelectedBrand(Brand selectedBrand) {
    this.selectedBrand = selectedBrand;
  }

  public List<Season> getSeasonList() {
    return seasonList;
  }

  public Season getSelectedSeason() {
    return selectedSeason;
  }

  public void setSelectedSeason(Season selectedSeason) {
    this.selectedSeason = selectedSeason;
  }

  public AssortmentGroup getSelectedAssortmentGroup() {
    return selectedAssortmentGroup;
  }

  public void setSelectedAssortmentGroup(AssortmentGroup selectedAssortmentGroup) {
    this.selectedAssortmentGroup = selectedAssortmentGroup;
  }

  public List<Brand> getBrandList() {
    return brandList;
  }

  public List<Theme> getThemeList() {
    return themeList;
  }

  public void setSelectedTheme(Theme selectedTheme) {
    this.selectedTheme = selectedTheme;

    Map<String, Object> map = new HashMap<>();
    map.put("selectedTheme", selectedTheme);
    BindUtils.postGlobalCommand(null, null, "refreshTheme", map);
  }

  @GlobalCommand
  @NotifyChange({"selectedTheme"})
  public void refreshTheme(@BindingParam("selectedTheme")
  final Theme selectedTheme) {
    this.selectedTheme = selectedTheme;
  }

  @Command
  @NotifyChange("*")
  public void search() {
    Theme theme = null;
    ThemeOverviewSearchCriteria searchCriteria = new ThemeOverviewSearchCriteria();
    searchCriteria.setAssortmentGroupId(this.selectedAssortmentGroup != null
        ? selectedAssortmentGroup.getAssortmentGroupId()
        : null);

    List<Theme> serachData = this.themeService.findThemeOverviewBySearchCriteria(searchCriteria);
    this.themeList.clear();
    if (serachData != null && !serachData.isEmpty()) {
      this.themeList.addAll(serachData);
      theme = this.themeList.get(0);
    }
    setSelectedTheme(theme);
  }

  @Command
  @NotifyChange(".")
  public void reset() {
    this.selectedBrand = this.brandList.get(0);
    this.selectedSeason = this.seasonList.get(0);
    this.theme = null;
    this.style = null;
    this.selectedAssortmentGroup = this.assortmentGroupList.get(0);
    search();
  }

  public String getBrandLabel(Brand brand) {
    StringBuilder buffer = new StringBuilder("");
    if (brand.getBrandFamily() != null) {
      buffer.append(brand.getBrandFamily().getBrandFamilyCode().trim()).append(" - ");
    }
    if (brand.getBrandDesc() != null) {
      buffer.append(brand.getBrandDesc().trim());
    }
    return buffer.toString().trim();
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public String getStyle() {
    return style;
  }

  public void setStyle(String style) {
    this.style = style;
  }

}
