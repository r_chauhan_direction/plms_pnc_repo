
package com.dss.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.dss.model.Menu;
import com.dss.model.SubMenu;
import com.dss.service.MenuService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MainViewModel {

	@WireVariable
	private MenuService menuService;
	
	private List<Menu> menuList = new ArrayList<>();
	/*private List<SubMenu> subMenuList = new ArrayList<SubMenu>();
	private List<SubMenu> subMenuForMasterdataList = new ArrayList<SubMenu>();
*/
	private Menu selectedMenu=new Menu();

	private SubMenu currentPage = new SubMenu();
/*
	private Menu masterData = new Menu(1l, "Master Data", null);
	private Menu styles = new Menu(2l, "Styles", subMenuList);
	private Menu linelist = new Menu(3l, "Linelist", null);
	private Menu quantity_calculation = new Menu(4l, "Quantity calculation ", null);
	private Menu settings = new Menu(5l, "Settings", null);
*/
	@GlobalCommand
 @NotifyChange("currentPage")
	public void navigatePage(@BindingParam("target") SubMenu subMenu) {
	//	BindUtils.postNotifyChange(null, null, currentPage, "selected");
		currentPage = subMenu;
	//	BindUtils.postNotifyChange(null, null, this, "currentPage");
	}
	
	

	
  



  public Menu getSelectedMenu() {
		return selectedMenu;
	}








public SubMenu getCurrentPage() {
		return currentPage;
	}

	@Init
	public void init() {
		this.menuList=this.menuService.findAll();
		/*SubMenu overviewSubMenu = new SubMenu(1l, "Overview", "menu/styles/styleoverview.zul");
		SubMenu supplierStylesSubMenu = new SubMenu(2l, "Supplier styles", "menu/supplierstyles/supplierstyle.zul");
		SubMenu bulkCreateSubMenu = new SubMenu(3l, "Bulk create", "menu/masterdata/masterdata.zul");

		subMenuList.add(overviewSubMenu);
		subMenuList.add(supplierStylesSubMenu);
		subMenuList.add(bulkCreateSubMenu);

		styles.setSelectedSubMenu(overviewSubMenu);
		styles.setSelected(true);

		subMenuForMasterdataList.add(new SubMenu(3l, "masters", "menu/masterdata/masterdata.zul"));

		menuList.add(masterData);
		menuList.add(styles);
		menuList.add(linelist);
		menuList.add(quantity_calculation);
		menuList.add(settings);*/
this.selectedMenu=this.menuList.get(1);
		this.currentPage = this.selectedMenu.getSubmenuList().get(3);
		this.selectedMenu.setSelectedSubMenu(currentPage);
					this.selectedMenu.setSelected(true); 
			 


	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	 
	 

}
