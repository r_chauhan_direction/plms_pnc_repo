package com.dss.viewmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.Buyer;
import com.dss.model.BuyingDirector;
import com.dss.model.FashionLevel;
import com.dss.model.Menu;
import com.dss.model.Season;
import com.dss.model.StyleMaster;
import com.dss.model.StyleOverviewSearchCriteria;
import com.dss.model.SubMenu;
import com.dss.model.Theme;
import com.dss.model.TrendTopic;
import com.dss.service.AssortmentGroupService;
import com.dss.service.BrandService;
import com.dss.service.BuyerService;
import com.dss.service.BuyingDirectorService;
import com.dss.service.FashionLevelService;
import com.dss.service.SeasonService;
import com.dss.service.StyleMasterService;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class StylePreviewViewModel {

	@WireVariable
	BrandService brandService;

	@WireVariable
	BuyingDirectorService buyingDirectorService;

	@WireVariable
	SeasonService seasonService;

	@WireVariable
	AssortmentGroupService assortmentGroupService;

	@WireVariable
	FashionLevelService fashionLevelService;

	@WireVariable
	StyleMasterService styleMasterService;

	@WireVariable
	BuyerService buyerService;
	
	StyleMaster selectedStyle = new StyleMaster();
	String enteredStyleNumber;
	private String styleDesc;
	
	List<Brand> brandList;
	List<BuyingDirector> buyingDirectorList = new ArrayList<>();
	List<AssortmentGroup> assortmentGroupList = new ArrayList<>();
	List<Season> seasonList = new ArrayList<>();
	List<StyleMaster> styleList = new ArrayList<>();
	List<Buyer> buyerList = new ArrayList<>();
	List<Theme> themaList = new ArrayList<>();
	List<FashionLevel> fashionLevelList = new ArrayList<>();
	List<TrendTopic> trendTopicList = new ArrayList<>();
	
	BuyingDirector selectedBuyingDirector = new BuyingDirector();
	Brand selectedBrand = new Brand();
	Season selectedSeason = new Season();
	AssortmentGroup selectedAssortmentGroup = new AssortmentGroup();
	FashionLevel selectedFashionLevel = new FashionLevel();
	
	ListModelList<String> menuComboModel = new ListModelList<>();
	SubMenu currentPage = new SubMenu();
	
	
//	@NotifyChange("selectedStyle")
	public void setSelectedStyle(StyleMaster selectedStyle) {
		this.selectedStyle = selectedStyle;
//		styleDesc=selectedStyle.getStyleDesc();
////		BindUtils.postNotifyChange(null, null,  selectedStyle, "styleId");
		Map map=new HashMap<>();
		map.put("selectedStyle", selectedStyle);
		BindUtils.postGlobalCommand(null, null, "refreshStyleMaster", map);
	}
	
	private void loadData() {
		this.brandList = this.brandService.findAll();
		this.buyingDirectorList = this.buyingDirectorService.findAll();
		this.seasonList = seasonService.findAll();
		this.assortmentGroupList = assortmentGroupService.findAll();
		this.fashionLevelList = fashionLevelService.findAll();
		this.styleList = styleMasterService.findAll();
		// this.buyerList=buyerService.findAll();
	}
	
	
	@Init
	public void init() {
		loadData();
		this.selectedStyle = this.styleList.get(0);
	}

	
	public ListModelList<String> getMenuComboModel() {
		return menuComboModel;
	}

	@Command("newContact")
	public void newContact(@BindingParam("contact") String contact) {

		menuComboModel.addToSelection(contact);
	}

	
	public String getEnteredStyleNumber() {
		return enteredStyleNumber;
	}

	public void setEnteredStyleNumber(String enteredStyleNumber) {
		this.enteredStyleNumber = enteredStyleNumber;
	}

	public BuyingDirector getSelectedBuyingDirector() {
		return selectedBuyingDirector;
	}

	public void setSelectedBuyingDirector(BuyingDirector selectedBuyingDirector) {
		this.selectedBuyingDirector = selectedBuyingDirector;
	}

	public Season getSelectedSeason() {
		return selectedSeason;
	}

	public void setSelectedSeason(Season selectedSeason) {
		this.selectedSeason = selectedSeason;
	}

	public AssortmentGroup getSelectedAssortmentGroup() {
		return selectedAssortmentGroup;
	}

	public void setSelectedAssortmentGroup(AssortmentGroup selectedAssortmentGroup) {
		this.selectedAssortmentGroup = selectedAssortmentGroup;
	}

	public FashionLevel getSelectedFashionLevel() {
		return selectedFashionLevel;
	}

	public void setSelectedFashionLevel(FashionLevel selectedFashionLevel) {
		this.selectedFashionLevel = selectedFashionLevel;
	}

	public void setSelectedBrand(Brand selectedBrand) {
		this.selectedBrand = selectedBrand;
	}

	public Brand getSelectedBrand() {
		return selectedBrand;
	}

	public List<Buyer> getBuyerList() {
		return buyerList;
	}
	
	public StyleMaster getSelectedStyle() {
		return selectedStyle;
	}

	public List<Brand> getBrandList() {
		return brandList;
	}

	public List<BuyingDirector> getBuyingDirectorList() {
		return buyingDirectorList;
	}

	public List<AssortmentGroup> getAssortmentGroupList() {
		return assortmentGroupList;
	}
	/*
	 * @Command
	 * 
	 * @DependsOn({"selectedBuyingDirector"}) public List<AssortmentGroup>
	 * getAssortmentGroupList(@BindingParam("target") BuyingDirector buyingDirector
	 * ) { System.out.println("buying"); return assortmentGroupList; }
	 */

	public List<Season> getSeasonList() {
		return seasonList;
	}

	public List<Theme> getThemaList() {
		return themaList;
	}

	public List<TrendTopic> getTrendTopicList() {
		return trendTopicList;
	}

	public List<StyleMaster> getStyleList() {
		return styleList;
	}

	public void setBuyerList(List<Buyer> buyerList) {
		this.buyerList = buyerList;
	}

	public void setBrandList(List<Brand> brandList) {
		this.brandList = brandList;
	}

	@Command
	@NotifyChange("assortmentGroupList")
	public void onBuyingDirectorSelect() {
	List<AssortmentGroup> subList = getAssortmentGroupList().subList(0, 2);
	assortmentGroupList=new ArrayList<>(subList);
	
	}
//	
//	@DependsOn("selectedStyle")
	public String getStyleDesc() {
		return styleDesc;
	}
	
	@GlobalCommand
	@NotifyChange({"selectedStyle"})
	public void refreshStyleMaster(@BindingParam("selectedStyle") StyleMaster selectedStyle) {
//		Object object = ((GlobalCommandEvent) event).getArgs().get("selectedStyle");
		this.selectedStyle=selectedStyle;
//		this.selectedStyle = stylem;
	}
	@Command
	@NotifyChange("*")
	public void search() {
		StyleOverviewSearchCriteria styleOverviewSearchCriteria = new StyleOverviewSearchCriteria();
		styleOverviewSearchCriteria.setBrandId(selectedBrand != null ? selectedBrand.getBrandId() : null);
		styleOverviewSearchCriteria.setAssortmentGroupId(
				selectedAssortmentGroup != null ? selectedAssortmentGroup.getAssortmentGroupId() : null);
		styleOverviewSearchCriteria.setBuyingDirectorId(
				selectedBuyingDirector != null ? selectedBuyingDirector.getBuyingDirectorId() : null);
		styleOverviewSearchCriteria
				.setStyleNumber(enteredStyleNumber != null ? Long.valueOf(enteredStyleNumber) : null);
		styleOverviewSearchCriteria.setSeasonId(selectedSeason != null ? selectedSeason.getSeasonId() : null);
		List<StyleMaster> findStyleMasterBySearchCriteria = styleMasterService
				.findStyleMasterBySearchCriteria(styleOverviewSearchCriteria);
		styleList.clear();
		styleList.addAll(findStyleMasterBySearchCriteria);
		System.out.println("selectedBrand" + findStyleMasterBySearchCriteria.size());
		System.out.println("search");

	}
}
