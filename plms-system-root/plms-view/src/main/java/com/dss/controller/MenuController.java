package com.dss.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController 
@RequestMapping("/style")
public class MenuController {
	
	@RequestMapping(value = {"", "/", "index"}, method = RequestMethod.GET)
	public String index() {
		return "hi";
	}
	
	
	@RequestMapping(value = "/masters", method = RequestMethod.GET)
	public String getMasterPage(ModelMap model, HttpSession session) {
		return "../menu/masterdata/index-profile-masterdata.zul";
	}
	
	@RequestMapping(value = "/supplierstyles", method = RequestMethod.GET)
	public String getSupplierStyles(ModelMap model, HttpSession session) {
		return "../menu/masterdata/index-profile-masterdata.zul";
	}
	
	@RequestMapping(value = "/bulkcreate", method = RequestMethod.GET)
	public String getStyleList(ModelMap model, HttpSession session) {
		return "../menu/masterdata/index-profile-masterdata.zul";
	}

	 
}
