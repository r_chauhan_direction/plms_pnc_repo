package com.dss.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dss.service.AssortmentGroupService;
import com.dss.service.BrandService;
import com.dss.service.BuyerService;
import com.dss.service.FashionLevelService;
import com.dss.service.MasterDataService;
import com.dss.service.SalesProgramService;
import com.dss.service.SeasonService;
import com.dss.service.StyleMasterService;

@RestController 
@RequestMapping("/style")
public class StyleOverviewController {

	@Autowired
	BrandService brandService;

	@Autowired
	BuyerService buyerService;

	@Autowired
	AssortmentGroupService assortmentGroupService;

	@Autowired
	SeasonService seasonService;

	@Autowired
	FashionLevelService fashionLevelService;

	@Autowired
	SalesProgramService salesProgramService;

	@Autowired
	StyleMasterService styleService;
	
	@Autowired
	MasterDataService masterDataService;
	
	@RequestMapping(value = "/master", method = RequestMethod.GET)
	public String getMasterData()
	{
		 
		this.masterDataService.findAllActiveBrands();
		return "masterData";
	}
	

	@RequestMapping(value = "/stylesoverview", method = RequestMethod.GET)
	public String getStyleList(ModelMap model, HttpSession session) {

		model.addAttribute("brandList", brandService.findAll());
		model.addAttribute("buyerList", buyerService.findAll());
		model.addAttribute("assortmentGroupList", assortmentGroupService.findAll());
		model.addAttribute("seasonList", seasonService.findAll());

		model.addAttribute("styleList", styleService.findAll());
		System.out.println("in styleController");

		return "../menu/styles/styleoverview.zul";

	}

}
