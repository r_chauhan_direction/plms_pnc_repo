package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dss.model.Brand;
import com.dss.model.BrandFamily;

@Entity
@Table(name = "brand" )
@AttributeOverride(name = "id", column = @Column(name = "brand_id"))
public class BrandEntity extends BaseEntity<Long> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8877848395434689530L;

	@Column(name = "brand_code")
	private String brandCode;

	@Column(name = "brand_desc")
	private String brandDesc;

	@ManyToOne 
	@JoinColumn(name = "brandtype_id")
	private BrandTypeEntity brandTypeEntity;
	
	
	@ManyToOne 
	@JoinColumn(name = "brandfamily_id")
	private BrandFamilyEntity brandFamilyEntity;

	

	
	
	public BrandFamilyEntity getBrandFamilyEntity() {
		return brandFamilyEntity;
	}

	public void setBrandFamilyEntity(BrandFamilyEntity brandFamilyEntity) {
		this.brandFamilyEntity = brandFamilyEntity;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public BrandTypeEntity getBrandTypeEntity() {
		return brandTypeEntity;
	}

	public void setBrandTypeEntity(BrandTypeEntity brandTypeEntity) {
		this.brandTypeEntity = brandTypeEntity;
	}

	public String getBrandDesc() {
		return brandDesc;
	}

	public void setBrandDesc(String brandDesc) {
		this.brandDesc = brandDesc;
	}

	public BrandEntity convertToEntity(Brand brand) {
		 setBrandDesc(brand.getBrandDesc());
		 setBrandCode(brand.getBrandCode());
		 return this;
	}

	public Brand convertToDto(Brand brand) {
		brand.setBrandId(getId());
		brand.setBrandDesc(getBrandDesc());
		brand.setBrandCode(getBrandCode());
		brand.setBrandFamily(getBrandFamilyEntity()!=null?getBrandFamilyEntity().convertToDto(new BrandFamily()):null);
		return brand;
	}

	@Override
	public String toString() {
		return "BrandEntity [brandDesc=" + brandDesc + "]";
	}

}
