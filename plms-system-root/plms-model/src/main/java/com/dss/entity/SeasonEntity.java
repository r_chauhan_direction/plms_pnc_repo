package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dss.model.Season;

@Entity
@Table(name = "season" )
@AttributeOverride(name="id",column=@Column(name="season_id"))
public class SeasonEntity extends BaseEntity<Long>{

	 
	/**
   * 
   */
  private static final long serialVersionUID = -2079017924777384920L;
  @Column(name = "season_code")
	private Long seasonCode;

	 

	public Long getSeasonCode() {
		return seasonCode;
	}

	public void setSeasonCode(Long seasonCode) {
		this.seasonCode = seasonCode;
	}

	public Season convertToDto(Season season) {
		season.setSeasonId(getId()); 
		season.setSeasonCode(getSeasonCode());
		return season;
	}
	
	public SeasonEntity convertToEntity(Season season) {
		SeasonEntity seasonEntity=new SeasonEntity();
		seasonEntity.setSeasonCode(season.getSeasonCode());
		seasonEntity.setId(season.getSeasonId());
		return seasonEntity;
	}
}
