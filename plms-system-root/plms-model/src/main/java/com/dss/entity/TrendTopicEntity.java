
package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dss.model.TrendTopic;

@Entity
@Table(name = "trendtopic" )
@AttributeOverride(name = "id", column = @Column(name = "trendtopic_id"))
public class TrendTopicEntity extends BaseEntity<Long> {

  /**
   * 
   */
  private static final long serialVersionUID = 4835843120471975693L;

  @Column(name = "trendtopic_code")
  private String trendTopicCode;

  @Column(name = "trendtopic_desc")
  private String trendtTpicDesc;

  /**
   * @return the trendTopicCode
   */
  public String getTrendTopicCode() {
    return trendTopicCode;
  }

  /**
   * @param trendTopicCode the trendTopicCode to set
   */
  public void setTrendTopicCode(String trendTopicCode) {
    this.trendTopicCode = trendTopicCode;
  }

  /**
   * @return the trendtTpicDesc
   */
  public String getTrendtTpicDesc() {
    return trendtTpicDesc;
  }

  /**
   * @param trendtTpicDesc the trendtTpicDesc to set
   */
  public void setTrendtTpicDesc(String trendtTpicDesc) {
    this.trendtTpicDesc = trendtTpicDesc;
  }

  public TrendTopicEntity() {
    super();
    // TODO Auto-generated constructor stub
  }

  public TrendTopic convertToDto(TrendTopic trendTopic) {
    trendTopic.setTrendTopicId(getId());
    trendTopic.setTrendTopicCode(getTrendTopicCode());
    trendTopic.setTrendTopicDesc(getTrendtTpicDesc());
    return trendTopic;
  }

  public TrendTopicEntity convertToEntity(TrendTopic trendTopic) {
    setId(trendTopic.getTrendTopicId());
    setTrendTopicCode(trendTopic.getTrendTopicCode());
    setTrendtTpicDesc(trendTopic.getTrendTopicDesc());
    return this;
  }

}
