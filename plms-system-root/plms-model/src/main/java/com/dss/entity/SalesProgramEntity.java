package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dss.model.SalesProgram;
import com.dss.model.Season;

@Entity
@Table(name =  "salesprogram" )
@AttributeOverride(name="id",column=@Column(name="salesprogram_id"))
public class SalesProgramEntity extends BaseEntity<Long>{

  /**
   * 
   */
  private static final long serialVersionUID = 7390708408486385725L;

  @Column(name = "salesprogram_code")
  private Long salesProgramCode;
  
  @Column(name = "salesprogram_desc")
  private String salesProgramDesc;

  
  /**
   * @return the salesProgramCode
   */
  public Long getSalesProgramCode() {
    return salesProgramCode;
  }

  
  /**
   * @param salesProgramCode the salesProgramCode to set
   */
  public void setSalesProgramCode(Long salesProgramCode) {
    this.salesProgramCode = salesProgramCode;
  }

  
  /**
   * @return the salesProgramDesc
   */
  public String getSalesProgramDesc() {
    return salesProgramDesc;
  }

  
  /**
   * @param salesProgramDesc the salesProgramDesc to set
   */
  public void setSalesProgramDesc(String salesProgramDesc) {
    this.salesProgramDesc = salesProgramDesc;
  }
  
  public SalesProgram convertToDto(SalesProgram salesProgram) {
    salesProgram.setSalesProgramCode(getSalesProgramCode());
    salesProgram.setSalesProgramId(getId());
    salesProgram.setSalesProgramDesc(getSalesProgramDesc());
    return salesProgram;
  }
  
  
  public SalesProgramEntity convertToEntity(SalesProgram salesProgram) {
  setId(salesProgram.getSalesProgramId());
  setSalesProgramCode(salesProgram.getSalesProgramCode());
  setSalesProgramDesc(salesProgram.getSalesProgramDesc());
    return this;
  }
}
