
package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.Theme;

@Entity
@Table(name = "theme")
@AttributeOverride(name = "id", column = @Column(name = "theme_id"))
public class ThemeEntity extends BaseEntity<Long> {

  @ManyToOne
  @JoinColumn(name = "assortment_group_id")
  private AssortmentGroupEntity assortmentGroupEntity;

  @ManyToOne
  @JoinColumn(name = "brand_id")
  private BrandEntity brandEntity;

  @Column(name = "theme_desc")
  private String themeDesc;

  @Column(name = "theme_code")
  private String themeCode;

  public AssortmentGroupEntity getAssortmentGroupEntity() {
    return assortmentGroupEntity;
  }

  public void setAssortmentGroupEntity(AssortmentGroupEntity assortmentGroupEntity) {
    this.assortmentGroupEntity = assortmentGroupEntity;
  }

  public BrandEntity getBrandEntity() {
    return brandEntity;
  }

  public void setBrandEntity(BrandEntity brandEntity) {
    this.brandEntity = brandEntity;
  }

  public String getThemeDesc() {
    return themeDesc;
  }

  public void setThemeDesc(String themeDesc) {
    this.themeDesc = themeDesc;
  }

  public String getThemeCode() {
    return themeCode;
  }

  public void setThemeCode(String themeCode) {
    this.themeCode = themeCode;
  }

  public Theme convertToDto(Theme theme) {
    theme.setThemeId(getId());
    theme.setThemeDesc(getThemeDesc());
    theme.setCode(getThemeCode());
    if (getAssortmentGroupEntity() != null) {
      theme.setAssortmentGroup(getAssortmentGroupEntity().convertToDto(new AssortmentGroup()));
    }
    if (getBrandEntity() != null) {
      theme.setBrand(getBrandEntity().convertToDto(new Brand()));
    }
    return theme;
  }

  public ThemeEntity convertToEntity(Theme theme) {
    ThemeEntity themeEntity = new ThemeEntity();
    themeEntity.setId(theme.getThemeId());
    themeEntity.setThemeCode(theme.getCode());
    themeEntity.setThemeDesc(theme.getThemeDesc());
    themeEntity.setAssortmentGroupEntity(
        new AssortmentGroupEntity().convertToEntity(theme.getAssortmentGroup()));
    themeEntity.setBrandEntity(new BrandEntity().convertToEntity(theme.getBrand()));
    return themeEntity;
  }
}
