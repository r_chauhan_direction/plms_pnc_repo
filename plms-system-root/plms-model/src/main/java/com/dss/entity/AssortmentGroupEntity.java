
package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.dss.model.AssortmentGroup;

@Entity
@Table(name = "assortmentgroup")
@AttributeOverride(name = "id", column = @Column(name = "assortmentgroup_id"))
public class AssortmentGroupEntity extends BaseEntity<Long> {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Column(name = "assortmentgroup_code")
  private Long assortmentGroupCode;

  @Column(name = "assortmentgroup_desc")
  private String assortmentGroupDesc;

  @ManyToOne
  @JoinColumn(name = "buying_director_id")
  private BuyingDirectorEntity buyingDirectorEntity;

  public BuyingDirectorEntity getBuyingDirectorEntity() {
    return buyingDirectorEntity;
  }

  public void setBuyingDirectorEntity(BuyingDirectorEntity buyingDirectorEntity) {
    this.buyingDirectorEntity = buyingDirectorEntity;
  }

  public Long getAssortmentGroupCode() {
    return assortmentGroupCode;
  }

  public void setAssortmentGroupCode(Long assortmentGroupCode) {
    this.assortmentGroupCode = assortmentGroupCode;
  }

  public String getAssortmentGroupDesc() {
    return assortmentGroupDesc;
  }

  public void setAssortmentGroupDesc(String assortmentGroupDesc) {
    this.assortmentGroupDesc = assortmentGroupDesc;
  }

  public AssortmentGroupEntity convertToEntity(AssortmentGroup assortmentGroup) {
    setId(assortmentGroup.getAssortmentGroupId());
    setAssortmentGroupCode(assortmentGroup.getAssortmentGroupCode());
    setAssortmentGroupDesc(assortmentGroup.getAssortmentGroupDesc());
    return this;
  }

  public AssortmentGroup convertToDto(AssortmentGroup assortmentGroup) {
    assortmentGroup.setAssortmentGroupCode(getAssortmentGroupCode());
    assortmentGroup.setAssortmentGroupDesc(getAssortmentGroupDesc());
    assortmentGroup.setAssortmentGroupId(getId());
    return assortmentGroup;
  }
}
