package com.dss.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dss.model.AssortmentGroup;
import com.dss.model.BuyingDirector;

@Entity
@Table(name = "buying_director" )
@AttributeOverride(name = "id", column = @Column(name = "buying_director_id"))
public class BuyingDirectorEntity extends BaseEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "buying_director_code")
	private String buyingDirectorCode;

	@OneToMany(mappedBy = "buyingDirectorEntity",fetch=FetchType.EAGER)
	List<AssortmentGroupEntity> assortmentGroupEntityList;

	public List<AssortmentGroupEntity> getAssortmentGroupEntityList() {
		return assortmentGroupEntityList;
	}

	public void setAssortmentGroupEntityList(List<AssortmentGroupEntity> assortmentGroupEntityList) {
		this.assortmentGroupEntityList = assortmentGroupEntityList;
	}

	public String getBuyingDirectorCode() {
		return buyingDirectorCode;
	}

	public void setBuyingDirectorCode(String buyingDirectorCode) {
		this.buyingDirectorCode = buyingDirectorCode;
	}

	
	public BuyingDirectorEntity(List<AssortmentGroupEntity> assortmentGroupEntityList) {
		super();
		this.assortmentGroupEntityList = assortmentGroupEntityList;
	}

	
	public BuyingDirectorEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BuyingDirectorEntity convetToEntity(BuyingDirector buyingDirector) {
		BuyingDirectorEntity buyingDirectorEntity = new BuyingDirectorEntity();
		buyingDirectorEntity.setBuyingDirectorCode(buyingDirector.getBuyingDirectorCode());
		buyingDirectorEntity.setId(buyingDirector.getBuyingDirectorId());

		List<AssortmentGroupEntity> assortmentGroupEntityList = new ArrayList<>();
		if (buyingDirector.getAssortmentGroupList() != null) {
			for (AssortmentGroup assortmentGroup : buyingDirector.getAssortmentGroupList()) {
				AssortmentGroupEntity assortmentGroupEntity = new AssortmentGroupEntity();
				AssortmentGroupEntity entity = assortmentGroupEntity.convertToEntity(assortmentGroup);
				assortmentGroupEntityList.add(entity);
			}
		}

		setAssortmentGroupEntityList(assortmentGroupEntityList);

		return buyingDirectorEntity;
	}

	public BuyingDirector converToDto(BuyingDirector buyingDirector) {
		buyingDirector.setBuyingDirectorCode(getBuyingDirectorCode());
		buyingDirector.setBuyingDirectorId(getId());

		List<AssortmentGroup> assortmentGroupList = new ArrayList<>();
		if (getAssortmentGroupEntityList() != null) {
			for (AssortmentGroupEntity assortmentGroupEntity : getAssortmentGroupEntityList()) {
				AssortmentGroup assortmentGroup = assortmentGroupEntity.convertToDto(new AssortmentGroup());
				assortmentGroupList.add(assortmentGroup);
			}
		}

		buyingDirector.setAssortmentGroupList(assortmentGroupList);

		return buyingDirector;
	}

}
