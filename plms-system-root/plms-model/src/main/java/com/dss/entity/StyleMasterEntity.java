
package com.dss.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.dss.model.Article;
import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.BuyingDirector;
import com.dss.model.FashionLevel;
import com.dss.model.SalesProgram;
import com.dss.model.Season;
import com.dss.model.StyleMaster;
import com.dss.model.Theme;
import com.dss.model.TrendTopic;

@Entity
@Table(name = "style_master" )
@AttributeOverride(name = "id", column = @Column(name = "style_id"))
public class StyleMasterEntity extends BaseEntity<Long> {

  @Column(name = "style_number")
  private Long styleNumber;

  @Column(name = "style_desc")
  private String styleDesc;

  @Column(name = "buyer")
  private String buyer;

  @Column(name = "supplier_shape")
  private String supplierShape;

  @Column(name = "supplier_material")
  private String supplierMaterial;

  @Column(name = "supplier_desc")
  private String supplierDesc;

  @Column(name = "image_url")
  private String imageUrl;

  @ManyToOne
  @JoinColumn(name = "season_id")
  private SeasonEntity seasonEntity;

  @ManyToOne
  @JoinColumn(name = "fashion_level_id")
  private FashionLevelEntity fashionLevelEntity;

  @ManyToOne
  @JoinColumn(name = "assortmentgroup_ID")
  private AssortmentGroupEntity asssortmentGroupEntity;

  @ManyToOne
  @JoinColumn(name = "brand_id")
  private BrandEntity brandEntity;

  @ManyToOne
  @JoinColumn(name = "buying_director_id")
  private BuyingDirectorEntity buyingDirectorEntity;

  @ManyToOne
  @JoinColumn(name = "salesprogram_id")
  private SalesProgramEntity salesProgramEntity;

  @ManyToOne
  @JoinColumn(name = "trendtopic_id")
  private TrendTopicEntity trendTopicEntity;

  @OneToMany(mappedBy = "styleMasterEntity", fetch = FetchType.EAGER)
  private List<ArticleEntity> listOfArticleEntity;

  @ManyToOne
  @JoinColumn(name = "theme_id")
  private ThemeEntity themeEntity;

  /**
   * @return the salesProgramEntity
   */
  public SalesProgramEntity getSalesProgramEntity() {
    return salesProgramEntity;
  }

  /**
   * @param salesProgramEntity the salesProgramEntity to set
   */
  public void setSalesProgramEntity(SalesProgramEntity salesProgramEntity) {
    this.salesProgramEntity = salesProgramEntity;
  }

  /**
   * @return the trendTopicEntity
   */
  public TrendTopicEntity getTrendTopicEntity() {
    return trendTopicEntity;
  }

  /**
   * @param trendTopicEntity the trendTopicEntity to set
   */
  public void setTrendTopicEntity(TrendTopicEntity trendTopicEntity) {
    this.trendTopicEntity = trendTopicEntity;
  }

  /**
   * @return the listOfArticleEntity
   */
  public List<ArticleEntity> getListOfArticleEntity() {
    return this.listOfArticleEntity;
  }

  /**
   * @param listOfArticleEntity the listOfArticleEntity to set
   */
  public void setListOfArticleEntity(final List<ArticleEntity> listOfArticleEntity) {
    this.listOfArticleEntity = listOfArticleEntity;
  }

  public Long getStyleNumber() {
    return this.styleNumber;
  }

  public void setStyleNumber(final Long styleNumber) {
    this.styleNumber = styleNumber;
  }

  public String getStyleDesc() {
    return this.styleDesc;
  }

  public void setStyleDesc(final String styleDesc) {
    this.styleDesc = styleDesc;
  }

  public SeasonEntity getSeasonEntity() {
    return this.seasonEntity;
  }

  public void setSeasonEntity(final SeasonEntity seasonEntity) {
    this.seasonEntity = seasonEntity;
  }

  public String getBuyer() {
    return this.buyer;
  }

  public void setBuyer(final String buyer) {
    this.buyer = buyer;
  }

  public String getSupplierShape() {
    return this.supplierShape;
  }

  public void setSupplierShape(final String supplierShape) {
    this.supplierShape = supplierShape;
  }

  public String getSupplierMaterial() {
    return this.supplierMaterial;
  }

  public void setSupplierMaterial(final String supplierMaterial) {
    this.supplierMaterial = supplierMaterial;
  }

  public String getSupplierDesc() {
    return this.supplierDesc;
  }

  public void setSupplierDesc(final String supplierDesc) {
    this.supplierDesc = supplierDesc;
  }

  public AssortmentGroupEntity getAsssortmentGroupEntity() {
    return this.asssortmentGroupEntity;
  }

  public void setAsssortmentGroupEntity(final AssortmentGroupEntity asssortmentGroupEntity) {
    this.asssortmentGroupEntity = asssortmentGroupEntity;
  }

  public BrandEntity getBrandEntity() {
    return this.brandEntity;
  }

  public void setBrandEntity(final BrandEntity brandEntity) {
    this.brandEntity = brandEntity;
  }

  public BuyingDirectorEntity getBuyingDirectorEntity() {
    return this.buyingDirectorEntity;
  }

  public void setBuyingDirectorEntity(final BuyingDirectorEntity buyingDirectorEntity) {
    this.buyingDirectorEntity = buyingDirectorEntity;
  }

  public String getImageUrl() {
    return this.imageUrl;
  }

  public void setImageUrl(final String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public ThemeEntity getThemeEntity() {
    return themeEntity;
  }

  public void setThemeEntity(ThemeEntity themeEntity) {
    this.themeEntity = themeEntity;
  }

  public FashionLevelEntity getFashionLevelEntity() {
    return fashionLevelEntity;
  }

  public void setFashionLevelEntity(FashionLevelEntity fashionLevelEntity) {
    this.fashionLevelEntity = fashionLevelEntity;
  }

  public StyleMasterEntity convertToEntity(final StyleMaster styleMaster) {
    final StyleMasterEntity styleMasterEntity = new StyleMasterEntity();
    styleMasterEntity.setId(styleMaster.getStyleId());
    styleMasterEntity.setStyleDesc(styleMaster.getStyleDesc());
    styleMasterEntity.setStyleNumber(styleMaster.getStyleNumber());
    styleMasterEntity.setStyleNumber(styleMaster.getStyleNumber());
    styleMasterEntity.setAsssortmentGroupEntity(
        new AssortmentGroupEntity().convertToEntity(styleMaster.getAsssortmentGroup()));
    styleMasterEntity.setBrandEntity(new BrandEntity().convertToEntity(styleMaster.getBrand()));
    styleMasterEntity.setBuyingDirectorEntity(
        new BuyingDirectorEntity().convetToEntity(styleMaster.getBuyingDirector()));
    styleMasterEntity.setSeasonEntity(new SeasonEntity().convertToEntity(styleMaster.getSeason()));
    styleMasterEntity.setSalesProgramEntity(
        new SalesProgramEntity().convertToEntity(styleMaster.getSalesProgram()));
    styleMasterEntity
        .setTrendTopicEntity(new TrendTopicEntity().convertToEntity(styleMaster.getTrendTopic()));
    styleMasterEntity.setImageUrl(styleMaster.getImageUrl());
    styleMasterEntity.setSupplierShape(styleMaster.getSupplierShape());
    styleMasterEntity.setSupplierDesc(styleMaster.getSupplierDesc());
    styleMasterEntity.setSupplierMaterial(styleMaster.getSupplierMaterial());
    styleMasterEntity.setFashionLevelEntity(
        new FashionLevelEntity().converToEntity(styleMaster.getFashionLevel()));
styleMasterEntity.setThemeEntity(new ThemeEntity().convertToEntity(styleMaster.getTheme()));
    
    
    if (styleMaster.getListOfArticle() != null) {
      final List<ArticleEntity> articleEntityList = new ArrayList<>();

      for (final Article article : styleMaster.getListOfArticle()) {
        final ArticleEntity articleEntity = new ArticleEntity().convertToEntity(article);
        articleEntityList.add(articleEntity);
      }
      setListOfArticleEntity(articleEntityList);
    }

    return styleMasterEntity;
  }

  public StyleMaster convertToDto(final StyleMaster styleMaster) {
    styleMaster.setSalesProgram(new SalesProgramEntity().convertToDto(new SalesProgram()));
    styleMaster.setTrendTopic(new TrendTopicEntity().convertToDto(new TrendTopic()));
    styleMaster
        .setAsssortmentGroup(getAsssortmentGroupEntity().convertToDto(new AssortmentGroup()));
    styleMaster.setBrand(getBrandEntity().convertToDto(new Brand()));
    styleMaster.setBuyingDirector(getBuyingDirectorEntity().converToDto(new BuyingDirector()));
    styleMaster.setSeason(getSeasonEntity().convertToDto(new Season()));
    styleMaster.setStyleDesc(getStyleDesc());
    styleMaster.setStyleNumber(getStyleNumber());
    styleMaster.setImageUrl(getImageUrl());
    styleMaster.setStyleId(getId());
    styleMaster.setBuyer(getBuyer());
    styleMaster.setSupplierShape(getSupplierShape());
    styleMaster.setSupplierDesc(getSupplierDesc());
    styleMaster.setSupplierMaterial(getSupplierMaterial());
    styleMaster.setFashionLevel(new FashionLevelEntity().converToDto(new FashionLevel()));
    styleMaster.setTheme(new ThemeEntity().convertToDto(new Theme()));
    if (getListOfArticleEntity() != null) {
      final List<Article> articleList = new ArrayList<>();

      for (final ArticleEntity articleEntity : getListOfArticleEntity()) {
        articleList.add(articleEntity.convertToDto(new Article()));
      }
      styleMaster.setListOfArticle(articleList);
    }
    return styleMaster;
  }
}
