package com.dss.entity;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * This class is the base class of all entities that use a primitive as their id. The actual type of
 * the the id column must be specified on the subclass extending the <code>BaseEntity</code>.
 * Additionally <code>&#64;AttributeOverride</code> should be used to specify the name of the
 * primary key group.
 * 
 * <pre>
 * &lt;code&gt;
 * &#064;AttributeOverride(name = &quot;id&quot;, column = @Column(name = &quot;USER_ID&quot;))
 * &#064;Entity
 * &#064;Table(name=&quot;User&quot;)
 * public class User extends AbstractNamedEntity {
 * 
 * }
 * &lt;/code&gt;
 * </pre>
 * 
 * The value of the id will be created automatically from the default generator. If this is
 * unsuitable the <code>getId()</code> must be overridden an annotated accordingly.
 * 
 * @see javax.persistence.Id
 * @see javax.persistence.GeneratedValue;
 * @param <IdClass> the class of the entity's id
 */
@MappedSuperclass
public class BaseEntity<IdClass extends Object> implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private IdClass id;

	
	public IdClass getId() {
		return id;
	}


	public void setId(IdClass id) {
		this.id = id;
	}


	public BaseEntity() {
		super();
		 
	}




	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof BaseEntity)) {
			return false;
		}
		BaseEntity other = (BaseEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}


	public String toString() {
		return id.toString();
	}



	
	

}
