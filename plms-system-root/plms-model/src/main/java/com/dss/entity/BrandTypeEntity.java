package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dss.model.BrandType;

@Entity
@Table(name = "brandtype" )
@AttributeOverride(name = "id", column = @Column(name = "brandtype_id"))
public class BrandTypeEntity extends BaseEntity<Long> {

	@Column(name = "brandtype_code")
	private Long brandTypeCode;

	@Column(name = "brandtype_desc")
	private String brandTypeDesc;

	public Long getBrandTypeCode() {
		return brandTypeCode;
	}

	public void setBrandTypeCode(Long brandTypeCode) {
		this.brandTypeCode = brandTypeCode;
	}

	public String getBrandTypeDesc() {
		return brandTypeDesc;
	}

	public void setBrandTypeDesc(String brandTypeDesc) {
		this.brandTypeDesc = brandTypeDesc;
	}

	public BrandTypeEntity convertToEntity(BrandType brandType) {
		 
	 setBrandTypeCode(brandType.getBrandTypeCode());
		 setBrandTypeDesc(brandType.getBrandTypeDesc());
		setId(brandType.getBrandTypeId());
		return this;

	}
	
	public BrandType convertToDto(BrandType brandType)
	{
		brandType.setBrandTypeCode(getBrandTypeCode());
		brandType.setBrandTypeDesc(getBrandTypeDesc());
		brandType.setBrandTypeId(getId());
		return brandType;
	}

}
