
package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.dss.model.BrandFamily;

@Entity
@Table(name = "brandfamily" )
@AttributeOverride(name = "id", column = @Column(name = "brandfamily_id"))
public class BrandFamilyEntity extends BaseEntity<Long> {

  /**
   * 
   */
  private static final long serialVersionUID = -46044114192436132L;

  @Column(name = "brandfamily_code")
  private String brandFamilyCode;

//	@OneToMany(mappedBy = "brandFamilyEntity", fetch = FetchType.EAGER)
//	private List<BrandEntity> listOfBrandEntity;

//	public List<BrandEntity> getListOfBrandEntity() {
//		return listOfBrandEntity;
//	}
//
//	public void setListOfBrandEntity(List<BrandEntity> listOfBrandEntity) {
//		this.listOfBrandEntity = listOfBrandEntity;
//	}

  public String getBrandFamilyCode() {
    return brandFamilyCode;
  }

  public void setBrandFamilyCode(String brandFamilyCode) {
    this.brandFamilyCode = brandFamilyCode;
  }

  public BrandFamilyEntity convertToEntity(BrandFamily brandFamily) {
    setId(brandFamily.getBrandFamilyId());
    setBrandFamilyCode(brandFamily.getBrandFamilyCode());

//		List<BrandEntity> brandEnities = new ArrayList<>();
//		if (brandFamily.getListOfBrand() != null) {
//		for (Brand brand : brandFamily.getListOfBrand()) {
//			BrandEntity brandEntity = new BrandEntity();
//			brandEntity.setId(brand.getBrandId());
//			brandEntity.convertToEntity(brand);
//			brandEnities.add(brandEntity);
//		}
//		}
//		setListOfBrandEntity(brandEnities);
    return this;
  }

  public BrandFamily convertToDto(BrandFamily brandFamily) {
    brandFamily.setBrandFamilyCode(getBrandFamilyCode());
    brandFamily.setBrandFamilyId(getId());
    return brandFamily;
  }

}
