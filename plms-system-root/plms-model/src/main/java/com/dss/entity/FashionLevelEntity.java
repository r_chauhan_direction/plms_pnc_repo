package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.dss.model.FashionLevel;

@Entity
@Table(
		name = "fashion_level" )
@AttributeOverride(name="id",column=@Column(name="fashion_level_id"))
public class FashionLevelEntity extends BaseEntity<Long>{
	
	 
	
	@Column(name="fashion_level_desc")
	private String fashionLevelDesc;
	
	@Column(name="fashion_level_code")
	private Long fashionLevelCode;
	
	
	 
 
	public String getFashionLevelDesc() {
		return fashionLevelDesc;
	}
	public void setFashionLevelDesc(String fashionLevelDesc) {
		this.fashionLevelDesc = fashionLevelDesc;
	}
	public Long getFashionLevelCode() {
		return fashionLevelCode;
	}
	public void setFashionLevelCode(Long fashionLevelCode) {
		this.fashionLevelCode = fashionLevelCode;
	}
	public FashionLevelEntity(  Long fashionLevelCode, String fashionLevelDesc) {
		super();
		 
		this.fashionLevelDesc = fashionLevelDesc;
		this.fashionLevelCode = fashionLevelCode;
	}
	public FashionLevelEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public FashionLevelEntity converToEntity(FashionLevel fashionLevel) {
		FashionLevelEntity fashionLevelEntity=new FashionLevelEntity();
		fashionLevelEntity.setFashionLevelCode(fashionLevel.getFashionLevelCode());
		fashionLevelEntity.setFashionLevelDesc(fashionLevel.getFashionLevelDesc());
		fashionLevelEntity.setId(fashionLevel.getFashionLevelId());
		return fashionLevelEntity;
	}
	
	public FashionLevel  converToDto(FashionLevel fashionLevel) {
		 
		fashionLevel.setFashionLevelCode( getFashionLevelCode());
		fashionLevel.setFashionLevelDesc( getFashionLevelDesc());
		fashionLevel.setFashionLevelId(getId());
		return fashionLevel;
	}
	
}
