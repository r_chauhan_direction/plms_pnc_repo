
package com.dss.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.dss.model.Article;

@Entity
@Table(name = "article")
@AttributeOverride(name = "id", column = @Column(name = "article_id"))
public class ArticleEntity extends BaseEntity<Long> {

  /**
   * 
   */
  private static final long serialVersionUID = 2691042844662410360L;

  @Column(name = "supplier_article_number")
  private String supplierArticleNumber;

  @Column(name = "articlenumber")
  private Long articleNumber;

  @Column(name = "order_selling_price")
  private Long orderSellingPrice;

  @Column(name = "current_selling_price")
  private Long currentSellingPrice;

  @Column(name = "stylecolour_id")
  private Long styleColorId;

  @Column(name = "size_range")
  private String sizeRange;

 /* @Column(name = "nos")
  private Boolean neverOutOfStock;*/
  
  @Transient
  private Boolean neverOutOfStock=Boolean.TRUE;

  @ManyToOne
  @JoinColumn(name = "season_id")
  private SeasonEntity seasonEntity;

  @ManyToOne
  @JoinColumn(name = "style_id")
  private StyleMasterEntity styleMasterEntity;
   

/*  @Column(name = "stylecolour_desc")
  private String styleColorDesc;*/


  /**
   * @return the neverOutOfStock
   */
  public Boolean getNeverOutOfStock() {
    return neverOutOfStock;
  }

  /**
   * @param neverOutOfStock the neverOutOfStock to set
   */
  public void setNeverOutOfStock(Boolean neverOutOfStock) {
    this.neverOutOfStock = neverOutOfStock;
  }
 
   

  
  /**
   * @return the supplierArticleNumber
   */
  public String getSupplierArticleNumber() {
    return supplierArticleNumber;
  }

  
  /**
   * @param supplierArticleNumber the supplierArticleNumber to set
   */
  public void setSupplierArticleNumber(String supplierArticleNumber) {
    this.supplierArticleNumber = supplierArticleNumber;
  }

  /**
   * @return the articleNumber
   */
  public Long getArticleNumber() {
    return articleNumber;
  }

  /**
   * @param articleNumber the articleNumber to set
   */
  public void setArticleNumber(Long articleNumber) {
    this.articleNumber = articleNumber;
  }

  /**
   * @return the orderSellingPrice
   */
  public Long getOrderSellingPrice() {
    return orderSellingPrice;
  }

  /**
   * @param orderSellingPrice the orderSellingPrice to set
   */
  public void setOrderSellingPrice(Long orderSellingPrice) {
    this.orderSellingPrice = orderSellingPrice;
  }

  /**
   * @return the currentSellingPrice
   */
  public Long getCurrentSellingPrice() {
    return currentSellingPrice;
  }

  /**
   * @param currentSellingPrice the currentSellingPrice to set
   */
  public void setCurrentSellingPrice(Long currentSellingPrice) {
    this.currentSellingPrice = currentSellingPrice;
  }

  /**
   * @return the styleColorId
   */
  public Long getStyleColorId() {
    return styleColorId;
  }

  /**
   * @param styleColorId the styleColorId to set
   */
  public void setStyleColorId(Long styleColorId) {
    this.styleColorId = styleColorId;
  }

  /**
   * @return the sizeRange
   */
  public String getSizeRange() {
    return sizeRange;
  }

  /**
   * @param sizeRange the sizeRange to set
   */
  public void setSizeRange(String sizeRange) {
    this.sizeRange = sizeRange;
  }

  /**
   * @return the seasonEntity
   */
  public SeasonEntity getSeasonEntity() {
    return seasonEntity;
  }

  /**
   * @param seasonEntity the seasonEntity to set
   */
  public void setSeasonEntity(SeasonEntity seasonEntity) {
    this.seasonEntity = seasonEntity;
  }

  /**
   * @return the styleMasterEntity
   */
  public StyleMasterEntity getStyleMasterEntity() {
    return styleMasterEntity;
  }
  
  /**
   * @param styleMasterEntity the styleMasterEntity to set
   */
  public void setStyleMasterEntity(StyleMasterEntity styleMasterEntity) {
    this.styleMasterEntity = styleMasterEntity;
  }
  
  

  public ArticleEntity convertToEntity(Article article) {

    setArticleNumber(article.getArticleNumber());
    setCurrentSellingPrice(article.getCurrentSellingPrice());
    setNeverOutOfStock(article.getNeverOutOfStock());
    setOrderSellingPrice(article.getOrderSellingPrice());
    setStyleColorId(article.getStyleColorId());
    setSupplierArticleNumber(article.getSupplierArticleNumber());
    setSizeRange(article.getSizeRange());
    

    return this;
  }

  public Article convertToDto(Article article) {

    article.setArticleId(getId());
    article.setArticleNumber(getArticleNumber());
    article.setCurrentSellingPrice(getCurrentSellingPrice());
    article.setStyleColorId(getStyleColorId());
    article.setNeverOutOfStock(getNeverOutOfStock());
    article.setSizeRange(getSizeRange());
    article.setSupplierArticleNumber(getSupplierArticleNumber());
 
    return article;
  }
}
