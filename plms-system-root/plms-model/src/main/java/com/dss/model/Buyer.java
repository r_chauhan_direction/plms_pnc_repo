package com.dss.model;

public class Buyer {
	private String buyerName;
	private String buyingDirector;
	 
	public String getBuyingDirector() {
		return buyingDirector;
	}
	public void setBuyingDirector(String buyingDirector) {
		this.buyingDirector = buyingDirector;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public Buyer(String buyerName, String buyingDirector) {
		super();
		this.buyerName = buyerName;
		this.buyingDirector = buyingDirector;
	}
 
	
	


}
