
package com.dss.model;

public class StyleOverviewSearchCriteria {

  private Long brandId;
  private Long buyingDirectorId;
  private Long seasonId;
  private Long assortmentGroupId;
  private Long styleNumber;
  private Long trendTopicId;
  private Long salesProgramId;
  private Long brandTypeId;
  private String themeDesc;
  private Long fashionLevelId;

  /**
   * @return the trendTopicId
   */
  public Long getTrendTopicId() {
    return trendTopicId;
  }

  /**
   * @param trendTopicId the trendTopicId to set
   */
  public void setTrendTopicId(Long trendTopicId) {
    this.trendTopicId = trendTopicId;
  }

  /**
   * @return the salesProgramId
   */
  public Long getSalesProgramId() {
    return salesProgramId;
  }

  /**
   * @param salesProgramId the salesProgramId to set
   */
  public void setSalesProgramId(Long salesProgramId) {
    this.salesProgramId = salesProgramId;
  }

  public Long getBrandId() {
    return brandId;
  }

  public void setBrandId(Long brandId) {
    this.brandId = brandId;
  }

  public Long getBuyingDirectorId() {
    return buyingDirectorId;
  }

  public void setBuyingDirectorId(Long buyingDirectorId) {
    this.buyingDirectorId = buyingDirectorId;
  }

  public Long getSeasonId() {
    return seasonId;
  }

  public void setSeasonId(Long seasonId) {
    this.seasonId = seasonId;
  }

  public Long getAssortmentGroupId() {
    return assortmentGroupId;
  }

  public void setAssortmentGroupId(Long assortmentGroupId) {
    this.assortmentGroupId = assortmentGroupId;
  }

  public Long getStyleNumber() {
    return styleNumber;
  }

  public void setStyleNumber(Long styleNumber) {
    this.styleNumber = styleNumber;
  }

  /**
   * @return the brandTypeId
   */
  public Long getBrandTypeId() {
    return brandTypeId;
  }

  /**
   * @param brandTypeId the brandTypeId to set
   */
  public void setBrandTypeId(Long brandTypeId) {
    this.brandTypeId = brandTypeId;
  }

  public String getThemeDesc() {
    return themeDesc;
  }

  public void setThemeDesc(String themeDesc) {
    this.themeDesc = themeDesc;
  }

  public Long getFashionLevelId() {
    return fashionLevelId;
  }

  public void setFashionLevelId(Long fashionLevelId) {
    this.fashionLevelId = fashionLevelId;
  }

}
