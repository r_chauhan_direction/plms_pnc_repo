
package com.dss.model;

public class Theme {

  private Long themeId;
  private AssortmentGroup assortmentGroup;
  private Brand brand;
  private String themeDesc;
  private String active;
  private String code;

  public Long getThemeId() {
    return themeId;
  }

  public void setThemeId(Long themeId) {
    this.themeId = themeId;
  }

  public String getThemeDesc() {
    return themeDesc;
  }

  public void setThemeDesc(String themeDesc) {
    this.themeDesc = themeDesc;
  }

  public String getActive() {
    return active;
  }

  public void setActive(String active) {
    this.active = active;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public AssortmentGroup getAssortmentGroup() {
    return assortmentGroup;
  }

  public void setAssortmentGroup(AssortmentGroup assortmentGroup) {
    this.assortmentGroup = assortmentGroup;
  }

  public Brand getBrand() {
    return brand;
  }

  public void setBrand(Brand brand) {
    this.brand = brand;
  }

  public Theme(Long themeId, AssortmentGroup assortmentGroup, Brand brand, String themeDesc,
      String active, String code) {
    super();
    this.themeId = themeId;
    this.assortmentGroup = assortmentGroup;
    this.brand = brand;
    this.themeDesc = themeDesc;
    this.active = active;
    this.code = code;
  }

  public Theme() {
    super();
    // TODO Auto-generated constructor stub
  }

}
