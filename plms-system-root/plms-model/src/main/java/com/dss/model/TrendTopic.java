
package com.dss.model;

public class TrendTopic {

  private Long trendTopicId;
  private String trendTopicDesc;
  private String trendTopicCode;

  public Long getTrendTopicId() {
    return trendTopicId;
  }

  public void setTrendTopicId(Long trendTopicId) {
    this.trendTopicId = trendTopicId;
  }

  public String getTrendTopicDesc() {
    return trendTopicDesc;
  }

  public void setTrendTopicDesc(String trendTopicDesc) {
    this.trendTopicDesc = trendTopicDesc;
  }

  /**
   * @return the trendTopicCode
   */
  public String getTrendTopicCode() {
    return trendTopicCode;
  }

  /**
   * @param trendTopicCode the trendTopicCode to set
   */
  public void setTrendTopicCode(String trendTopicCode) {
    this.trendTopicCode = trendTopicCode;
  }

  public TrendTopic() {
    super();
    // TODO Auto-generated constructor stub
  }

}
