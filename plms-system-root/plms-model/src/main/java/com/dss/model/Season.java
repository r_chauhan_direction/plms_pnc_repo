package com.dss.model;

public class Season {

	private Long seasonId;
	
	private Long seasonCode;

	public Long getSeasonCode() {
		return seasonCode;
	}

	public void setSeasonCode(Long seasonCode) {
		this.seasonCode = seasonCode;
	}

	public Long getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(Long seasonId) {
		this.seasonId = seasonId;
	}
	
	

	public Season() {
		super();
	}

	public Season(Long seasonId, Long seasonCode) {
		super();
		this.seasonId = seasonId;
		this.seasonCode = seasonCode;
	}

	public Season(Long seasonId) {
		super();
		this.seasonId = seasonId;
	}

	 
	 

	 
	
	
}
