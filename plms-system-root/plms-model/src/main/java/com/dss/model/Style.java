package com.dss.model;

public class Style {

	private Long styleId; 
	private String styleDesc;
	private Long styleColorCode;
	private Long styleColorSubCode;
	private String styleColorDesc;
	private Season season;
	private Brand brand;
	private AsssortmentGroup asssortmentGroup;
	private Buyer buyer;
	
	
	
	
	 
	public Buyer getBuyer() {
		return buyer;
	}
	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}
	public Long getStyleColorSubCode() {
		return styleColorSubCode;
	}
	public void setStyleColorSubCode(Long styleColorSubCode) {
		this.styleColorSubCode = styleColorSubCode;
	}
	public Long getStyleId() {
		return styleId;
	}
	public void setStyleId(Long styleId) {
		this.styleId = styleId;
	}
	public String getStyleDesc() {
		return styleDesc;
	}
	public void setStyleDesc(String styleDesc) {
		this.styleDesc = styleDesc;
	}
	public Long getStyleColorCode() {
		return styleColorCode;
	}
	public void setStyleColorCode(Long styleColorCode) {
		this.styleColorCode = styleColorCode;
	}
 
	public String getStyleColorDesc() {
		return styleColorDesc;
	}
	public void setStyleColorDesc(String styleColorDesc) {
		this.styleColorDesc = styleColorDesc;
	}
 
	public Season getSeason() {
		return season;
	}
	public void setSeason(Season season) {
		this.season = season;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	 
	public AsssortmentGroup getAsssortmentGroup() {
		return asssortmentGroup;
	}
	public void setAsssortmentGroup(AsssortmentGroup asssortmentGroup) {
		this.asssortmentGroup = asssortmentGroup;
	}
	
	
	public Style(Long styleId, String styleDesc, Long styleColorCode, Long styleColorSubCode, String styleColorDesc,
			Season season, Brand brand, AsssortmentGroup asssortmentGroup, Buyer buyer) {
		super();
		this.styleId = styleId;
		this.styleDesc = styleDesc;
		this.styleColorCode = styleColorCode;
		this.styleColorSubCode = styleColorSubCode;
		this.styleColorDesc = styleColorDesc;
		this.season = season;
		this.brand = brand;
		this.asssortmentGroup = asssortmentGroup;
		this.buyer = buyer;
	}
	public Style() {
		// TODO Auto-generated constructor stub
	}
	 
	
	
	 
	
	
	
	
	 
}
