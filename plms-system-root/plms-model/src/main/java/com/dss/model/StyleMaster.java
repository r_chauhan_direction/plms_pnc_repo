
package com.dss.model;

import java.util.List;

public class StyleMaster {

  private Long styleId;
  private String styleDesc;
  private String styleColorCode;

  private String brandDesc;

  private Season season;
  private String seasons;
  private Brand brand;
  private AssortmentGroup asssortmentGroup;
  private String buyer;
  private BuyingDirector buyingDirector;

  private String supplierShape;
  private String supplierMaterial;
  private String supplierDesc;
  private Long styleNumber;
  private List<Article> listOfArticle;
  private String imageUrl;
  private SalesProgram salesProgram;
  private TrendTopic trendTopic;
  private Integer numberOfColors;
  private FashionLevel fashionLevel;
  private Theme theme;

  public Theme getTheme() {
    return theme;
  }

  public void setTheme(Theme theme) {
    this.theme = theme;
  }

  /**
   * @return the numberOfColors
   */
  public Integer getNumberOfColors() {
    return numberOfColors;
  }

  /**
   * @param numberOfColors the numberOfColors to set
   */
  public void setNumberOfColors(Integer numberOfColors) {
    this.numberOfColors = numberOfColors;
  }

  /**
   * @return the salesProgram
   */
  public SalesProgram getSalesProgram() {
    return salesProgram;
  }

  /**
   * @param salesProgram the salesProgram to set
   */
  public void setSalesProgram(SalesProgram salesProgram) {
    this.salesProgram = salesProgram;
  }

  /**
   * @return the trendTopic
   */
  public TrendTopic getTrendTopic() {
    return trendTopic;
  }

  /**
   * @param trendTopic the trendTopic to set
   */
  public void setTrendTopic(TrendTopic trendTopic) {
    this.trendTopic = trendTopic;
  }

  /**
   * @return the listOfArticle
   */
  public List<Article> getListOfArticle() {
    return this.listOfArticle;
  }

  /**
   * @param listOfArticle the listOfArticle to set
   */
  public void setListOfArticle(final List<Article> listOfArticle) {
    this.listOfArticle = listOfArticle;
  }

  public Long getStyleNumber() {
    return this.styleNumber;
  }

  public void setStyleNumber(final Long styleNumber) {
    this.styleNumber = styleNumber;
  }

  public String getBuyer() {
    return this.buyer;
  }

  public void setBuyer(final String buyer) {
    this.buyer = buyer;
  }

  public String getSupplierShape() {
    return this.supplierShape;
  }

  public void setSupplierShape(final String supplierShape) {
    this.supplierShape = supplierShape;
  }

  public String getSupplierMaterial() {
    return this.supplierMaterial;
  }

  public void setSupplierMaterial(final String supplierMaterial) {
    this.supplierMaterial = supplierMaterial;
  }

  public String getSupplierDesc() {
    return this.supplierDesc;
  }

  public void setSupplierDesc(final String supplierDesc) {
    this.supplierDesc = supplierDesc;
  }

  public BuyingDirector getBuyingDirector() {
    return this.buyingDirector;
  }

  public void setBuyingDirector(final BuyingDirector buyingDirector) {
    this.buyingDirector = buyingDirector;
  }

  public Long getStyleId() {
    return this.styleId;
  }

  public void setStyleId(final Long styleId) {
    this.styleId = styleId;
  }

  public String getStyleDesc() {
    return this.styleDesc;
  }

  public void setStyleDesc(final String styleDesc) {
    this.styleDesc = styleDesc;
  }

  public Season getSeason() {
    return this.season;
  }

  public void setSeason(final Season season) {
    this.season = season;
  }

  public Brand getBrand() {
    return this.brand;
  }

  public void setBrand(final Brand brand) {
    this.brand = brand;
  }

  public AssortmentGroup getAsssortmentGroup() {
    return this.asssortmentGroup;
  }

  public void setAsssortmentGroup(final AssortmentGroup asssortmentGroup) {
    this.asssortmentGroup = asssortmentGroup;
  }

  public String getImageUrl() {
    return this.imageUrl;
  }

  public void setImageUrl(final String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getStyleColorCode() {
    return this.styleColorCode;
  }

  public void setStyleColorCode(final String styleColorCode) {
    this.styleColorCode = styleColorCode;
  }

  /**
   * @return the seasons
   */
  public String getSeasons() {
    return seasons;
  }

  /**
   * @param seasons the seasons to set
   */
  public void setSeasons(String seasons) {
    this.seasons = seasons;
  }

  public FashionLevel getFashionLevel() {
    return fashionLevel;
  }

  public void setFashionLevel(FashionLevel fashionLevel) {
    this.fashionLevel = fashionLevel;
  }

  public String getBrandDesc() {
    return brandDesc;
  }

  public void setBrandDesc(Brand brand2) {
    if (brand2 != null) this.brandDesc = brand2.getBrandDesc();
  }

  public StyleMaster(final Long styleId, final String styleDesc, final String styleColorCode,
      final Long styleColorSubCode, final Season season, final Brand brand,
      final AssortmentGroup asssortmentGroup, final String buyer) {
    super();
    this.styleId = styleId;
    this.styleDesc = styleDesc;
    this.styleColorCode = styleColorCode;
    this.season = season;
    this.brand = brand;
    this.asssortmentGroup = asssortmentGroup;
    this.buyer = buyer;
    this.buyingDirector = this.buyingDirector;
  }

  public StyleMaster() {
    // TODO Auto-generated constructor stub
  }

}
