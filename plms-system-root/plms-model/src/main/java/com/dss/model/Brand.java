package com.dss.model;

public class Brand {

	private Long brandId;
	private String brandCode;
	private String brandDesc;
	private BrandFamily brandFamily;

	public Brand() {
		super();
	}

	public Brand(Long brandId, String brandCode, String brandDesc, BrandFamily brandFamily) {
		super();
		this.brandId = brandId;
		this.brandCode = brandCode;
		this.brandDesc = brandDesc;
		this.brandFamily = brandFamily;
	}

	/**
	 * @return the brandFamily
	 */
	public BrandFamily getBrandFamily() {
		return brandFamily;
	}

	/**
	 * @param brandFamily
	 *            the brandFamily to set
	 */
	public void setBrandFamily(BrandFamily brandFamily) {
		this.brandFamily = brandFamily;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getBrandDesc() {
		return brandDesc;
	}

	public void setBrandDesc(String brandDesc) {
		this.brandDesc = brandDesc;
	}
}
