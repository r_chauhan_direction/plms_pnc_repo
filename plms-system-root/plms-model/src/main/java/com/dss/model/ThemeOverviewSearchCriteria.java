
package com.dss.model;

public class ThemeOverviewSearchCriteria {

  private Long brandTypeId;
  private Long assortmentGroupId;
  private Long seasonId;

  public Long getBrandTypeId() {
    return brandTypeId;
  }

  public void setBrandTypeId(Long brandTypeId) {
    this.brandTypeId = brandTypeId;
  }

  public Long getAssortmentGroupId() {
    return assortmentGroupId;
  }

  public void setAssortmentGroupId(Long assortmentGroupId) {
    this.assortmentGroupId = assortmentGroupId;
  }

  public Long getSeasonId() {
    return seasonId;
  }

  public void setSeasonId(Long seasonId) {
    this.seasonId = seasonId;
  }

}
