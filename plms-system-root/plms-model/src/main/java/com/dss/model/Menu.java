package com.dss.model;

import java.util.List;

public class Menu {

	private Long id;
	private String name;
	private List<SubMenu> submenuList;
	private SubMenu selectedSubMenu;
	private boolean selected;
	
	

	public Menu() {
		super();
	}

	public Menu(Long id, String name, List<SubMenu> submenuList) {
		super();
		this.id = id;
		this.name = name;
		this.submenuList = submenuList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<SubMenu> getSubmenuList() {
		return submenuList;
	}

	public void setSubmenuList(List<SubMenu> submenuList) {
		this.submenuList = submenuList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public SubMenu getSelectedSubMenu() {
		return selectedSubMenu;
	}

	public void setSelectedSubMenu(SubMenu selectedSubMenu) {
		this.selectedSubMenu = selectedSubMenu;
	}
}
