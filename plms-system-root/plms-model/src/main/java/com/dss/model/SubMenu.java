package com.dss.model;

public class SubMenu {
	private Long id;
	private String name;
	private String link;
	private boolean selected;

	public SubMenu() {
		super();
	}
	public SubMenu(Long id, String name, String link) {
		super();
		this.id = id;
		this.name = name;
		this.link = link;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
