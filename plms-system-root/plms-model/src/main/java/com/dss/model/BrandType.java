package com.dss.model;

public class BrandType {

	private Long brandTypeCode;
	 private Long brandTypeId;
	private String brandTypeDesc;

	public Long getBrandTypeCode() {
		return brandTypeCode;
	}

	public void setBrandTypeCode(Long brandTypeCode) {
		this.brandTypeCode = brandTypeCode;
	}

	public String getBrandTypeDesc() {
		return brandTypeDesc;
	}

	public void setBrandTypeDesc(String brandTypeDesc) {
		this.brandTypeDesc = brandTypeDesc;
	}

  
  /**
   * @return the brandTypeId
   */
  public Long getBrandTypeId() {
    return brandTypeId;
  }

  
  /**
   * @param brandTypeId the brandTypeId to set
   */
  public void setBrandTypeId(Long brandTypeId) {
    this.brandTypeId = brandTypeId;
  }
	
	
}
