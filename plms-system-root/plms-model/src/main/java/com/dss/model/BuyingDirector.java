package com.dss.model;

import java.util.List;

public class BuyingDirector {
	private String buyingDirectorCode;
	private Long buyingDirectorId;
	List<AssortmentGroup> assortmentGroupList;

	
	
	
  public List<AssortmentGroup> getAssortmentGroupList() {
    return assortmentGroupList;
  }

  
  public void setAssortmentGroupList(List<AssortmentGroup> assortmentGroupList) {
    this.assortmentGroupList = assortmentGroupList;
  }

  public Long getBuyingDirectorId() {
		return buyingDirectorId;
	}

	public void setBuyingDirectorId(Long buyingDirectorId) {
		this.buyingDirectorId = buyingDirectorId;
	}

	public String getBuyingDirectorCode() {
		return buyingDirectorCode;
	}

	public void setBuyingDirectorCode(String buyingDirectorCode) {
		this.buyingDirectorCode = buyingDirectorCode;
	}

	public BuyingDirector() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
