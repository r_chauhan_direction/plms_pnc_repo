package com.dss.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class AssortmentGroup  {
	private Long assortmentGroupId;
	private Long assortmentGroupCode;
	private String assortmentGroupDesc;
	private Boolean isDeactivated;
	private Boolean isDeleted;
	 
	public Boolean getIsDeactivated() {
		return isDeactivated;
	}
	public void setIsDeactivated(Boolean isDeactivated) {
		this.isDeactivated = isDeactivated;
	}
	public Boolean getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	 
	public Long getAssortmentGroupCode() {
		return assortmentGroupCode;
	}
	public void setAssortmentGroupCode(Long assortmentGroupCode) {
		this.assortmentGroupCode = assortmentGroupCode;
	}
	public String getAssortmentGroupDesc() {
		return assortmentGroupDesc;
	}
	public void setAssortmentGroupDesc(String assortmentGroupDesc) {
		this.assortmentGroupDesc = assortmentGroupDesc;
	}
	public Long getAssortmentGroupId() {
		return assortmentGroupId;
	}
	public void setAssortmentGroupId(Long assortmentGroupId) {
		this.assortmentGroupId = assortmentGroupId;
	}
	public AssortmentGroup(Long assortmentGroupId, String assortmentGroupDesc) {
		super();
		this.assortmentGroupId = assortmentGroupId;
		this.assortmentGroupDesc = assortmentGroupDesc;
	}
	public AssortmentGroup() {
		// TODO Auto-generated constructor stub
	}
	 
}