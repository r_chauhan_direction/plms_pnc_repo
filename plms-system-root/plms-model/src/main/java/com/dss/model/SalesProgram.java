package com.dss.model;

public class SalesProgram {
	private Long salesProgramId; 
	private String salesProgramDesc;
	private Long salesProgramCode;
	public Long getSalesProgramId() {
		return salesProgramId;
	}
	public void setSalesProgramId(Long salesProgramId) {
		this.salesProgramId = salesProgramId;
	}
	public String getSalesProgramDesc() {
		return salesProgramDesc;
	}
	public void setSalesProgramDesc(String salesProgramDesc) {
		this.salesProgramDesc = salesProgramDesc;
	}
	public Long getSalesProgramCode() {
		return salesProgramCode;
	}
	public void setSalesProgramCode(Long salesProgramCode) {
		this.salesProgramCode = salesProgramCode;
	}
	public SalesProgram(Long salesProgramId, String salesProgramDesc, Long salesProgramCode) {
		super();
		this.salesProgramId = salesProgramId;
		this.salesProgramDesc = salesProgramDesc;
		this.salesProgramCode = salesProgramCode;
	}
  public SalesProgram() {
    super();
    // TODO Auto-generated constructor stub
  }
	
	
	
}
