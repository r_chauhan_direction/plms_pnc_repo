package com.dss.model;

import java.util.List;

public class BrandFamily {
  private Long brandFamilyId;
	private String brandFamilyCode;
	private	List<Brand> listOfBrand;
	
	
	
	
  /**
   * @return the brandFamilyId
   */
  public Long getBrandFamilyId() {
    return brandFamilyId;
  }
  
  /**
   * @param brandFamilyId the brandFamilyId to set
   */
  public void setBrandFamilyId(Long brandFamilyId) {
    this.brandFamilyId = brandFamilyId;
  }
  public String getBrandFamilyCode() {
		return brandFamilyCode;
	}
	public void setBrandFamilyCode(String brandFamilyCode) {
		this.brandFamilyCode = brandFamilyCode;
	}
	public List<Brand> getListOfBrand() {
		return listOfBrand;
	}
	public void setListOfBrand(List<Brand> listOfBrand) {
		this.listOfBrand = listOfBrand;
	}
	
	
	
	
}
