package com.dss.model;

public class FashionLevel {
	private Long fashionLevelId; 
	private String fashionLevelDesc;
	private Long fashionLevelCode;
	public Long getFashionLevelId() {
		return fashionLevelId;
	}
	public void setFashionLevelId(Long fashionLevelId) {
		this.fashionLevelId = fashionLevelId;
	}
	public String getFashionLevelDesc() {
		return fashionLevelDesc;
	}
	public void setFashionLevelDesc(String fashionLevelDesc) {
		this.fashionLevelDesc = fashionLevelDesc;
	}
	public Long getFashionLevelCode() {
		return fashionLevelCode;
	}
	public void setFashionLevelCode(Long fashionLevelCode) {
		this.fashionLevelCode = fashionLevelCode;
	}
	public FashionLevel(Long fashionLevelId, Long fashionLevelCode, String fashionLevelDesc) {
		super();
		this.fashionLevelId = fashionLevelId;
		this.fashionLevelDesc = fashionLevelDesc;
		this.fashionLevelCode = fashionLevelCode;
	}
	public FashionLevel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
