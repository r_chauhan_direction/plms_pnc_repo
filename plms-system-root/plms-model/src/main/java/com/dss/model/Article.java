package com.dss.model;

public class Article {
	private Long articleId;
	 private String supplierArticleNumber;

	private Long articleNumber;
	private Long orderSellingPrice;
	private Long currentSellingPrice;
	private Long styleColorId;
	private String sizeRange;
	private Boolean neverOutOfStock;
	private Season season;
	private StyleMaster styleMaster;

 
	/**
	 * @return the articleId
	 */
	public Long getArticleId() {
		return articleId;
	}
	/**
	 * @param articleId the articleId to set
	 */
	public void setArticleId(Long articleId) {
		this.articleId = articleId;
	}
	 
	
  
  
  /**
   * @return the supplierArticleNumber
   */
  public String getSupplierArticleNumber() {
    return supplierArticleNumber;
  }
  
  /**
   * @param supplierArticleNumber the supplierArticleNumber to set
   */
  public void setSupplierArticleNumber(String supplierArticleNumber) {
    this.supplierArticleNumber = supplierArticleNumber;
  }
  /**
	 * @return the articleNumber
	 */
	public Long getArticleNumber() {
		return articleNumber;
	}
	/**
	 * @param articleNumber the articleNumber to set
	 */
	public void setArticleNumber(Long articleNumber) {
		this.articleNumber = articleNumber;
	}
	/**
	 * @return the orderSellingPrice
	 */
	public Long getOrderSellingPrice() {
		return orderSellingPrice;
	}
	/**
	 * @param orderSellingPrice the orderSellingPrice to set
	 */
	public void setOrderSellingPrice(Long orderSellingPrice) {
		this.orderSellingPrice = orderSellingPrice;
	}
	/**
	 * @return the currentSellingPrice
	 */
	public Long getCurrentSellingPrice() {
		return currentSellingPrice;
	}
	/**
	 * @param currentSellingPrice the currentSellingPrice to set
	 */
	public void setCurrentSellingPrice(Long currentSellingPrice) {
		this.currentSellingPrice = currentSellingPrice;
	}
	/**
	 * @return the styleColorId
	 */
	public Long getStyleColorId() {
		return styleColorId;
	}
	/**
	 * @param styleColorId the styleColorId to set
	 */
	public void setStyleColorId(Long styleColorId) {
		this.styleColorId = styleColorId;
	}
	/**
	 * @return the sizeRange
	 */
	public String getSizeRange() {
		return sizeRange;
	}
	/**
	 * @param sizeRange the sizeRange to set
	 */
	public void setSizeRange(String sizeRange) {
		this.sizeRange = sizeRange;
	}
	/**
	 * @return the neverOutOfStock
	 */
	public Boolean getNeverOutOfStock() {
		return neverOutOfStock;
	}
	/**
	 * @param neverOutOfStock the neverOutOfStock to set
	 */
	public void setNeverOutOfStock(Boolean neverOutOfStock) {
		this.neverOutOfStock = neverOutOfStock;
	}
	/**
	 * @return the season
	 */
	public Season getSeason() {
		return season;
	}
	/**
	 * @param season the season to set
	 */
	public void setSeason(Season season) {
		this.season = season;
	}
	/**
	 * @return the styleMaster
	 */
	public StyleMaster getStyleMaster() {
		return styleMaster;
	}
	/**
	 * @param styleMaster the styleMaster to set
	 */
	public void setStyleMaster(StyleMaster styleMaster) {
		this.styleMaster = styleMaster;
	}
	 
	 
	 
	
}
