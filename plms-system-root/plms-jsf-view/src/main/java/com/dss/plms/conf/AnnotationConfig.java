
package com.dss.plms.conf;

import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@PropertySource({"classpath:application.properties"})
@ComponentScan(basePackages = {"com.dss"})
@EnableJpaRepositories(basePackages = {"com.dss.dao"})
public class AnnotationConfig {

  @Autowired
  private Environment environment;

  /*
   * @Bean public LocalSessionFactoryBean sessionFactory() { LocalSessionFactoryBean sessionFactory
   * = new LocalSessionFactoryBean(); sessionFactory.setDataSource(dataSource(environment));
   * sessionFactory.setPackagesToScan( new String[] { "org.baeldung.spring.persistence.model" });
   * sessionFactory.setHibernateProperties(hibernateProperties()); return sessionFactory; }
   */
  @Bean
  DataSource dataSource(Environment environment) {

    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(environment.getRequiredProperty("spring.jpa.driver"));
    dataSource.setUrl(environment.getRequiredProperty("spring.datasource.url"));
    dataSource.setUsername(environment.getRequiredProperty("spring.datasource.username"));
    dataSource.setPassword(environment.getRequiredProperty("spring.datasource.password"));

    return dataSource;
  }

  @Bean
  @Autowired
  public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {

    HibernateTransactionManager txManager = new HibernateTransactionManager();
    txManager.setSessionFactory(sessionFactory);

    return txManager;
  }

  @Bean
  public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
    return new PersistenceExceptionTranslationPostProcessor();
  }

  Properties hibernateProperties() {
    return new Properties() {

      private static final long serialVersionUID = 1L;

      {

        setProperty("hibernate.dialect", environment.getProperty("spring.jpa.dialect"));
        setProperty("hibernate.show_sql", environment.getProperty("spring.jpa.show-sql"));

      }
    };
  }

  @Bean
  public EntityManagerFactory entityManagerFactory(DataSource dataSource, Environment environment) {

    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
    // vendorAdapter.setGenerateDdl(true);
    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
    factory.setJpaVendorAdapter(vendorAdapter);
    factory.setPackagesToScan("com.dss");
    factory.setDataSource(dataSource(environment));
    factory.setJpaProperties(hibernateProperties());
    factory.afterPropertiesSet();

    return factory.getObject();
  }

  @Bean
  public EntityManager entityManager(EntityManagerFactory entityManagerFactory) {

    return entityManagerFactory.createEntityManager();
  }

  @Bean
  JpaTransactionManager transactionManager() {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager
        .setEntityManagerFactory(entityManagerFactory(dataSource(environment), environment));

    return transactionManager;

  }
}
