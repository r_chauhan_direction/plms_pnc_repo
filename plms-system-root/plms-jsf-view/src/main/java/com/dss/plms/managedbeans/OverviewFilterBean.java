
package com.dss.plms.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.BuyingDirector;
import com.dss.model.FashionLevel;
import com.dss.model.Season;
import com.dss.service.BrandService;
import com.dss.service.BuyingDirectorService;
import com.dss.service.FashionLevelService;
import com.dss.service.SeasonService;

/**
 * Managed bean to perform brand related operations
 * 
 * @author r.chauhan
 */
@Component
public class OverviewFilterBean implements Serializable {

  private static final long serialVersionUID = 1L;

  @Autowired
  BrandService brandService;

  @Autowired
  BuyingDirectorService buyingDirectorService;

  @Autowired
  SeasonService seasonService;

  @Autowired
  FashionLevelService fashionLevelService;

  private String selectedBrand;

  private List<Brand> brands;

  private Long selectedBuyingDirector;

  private List<BuyingDirector> buyingDirectors;

  private Long selectedAssortmentGroup;

  private List<AssortmentGroup> assortmentGroups;

  private Long selectedSeason;

  private List<Season> seasons;

  private Long selectedFashionLevel;

  private List<FashionLevel> fashionLevels;

  public OverviewFilterBean() {}

  public BrandService getBrandService() {
    return brandService;
  }

  public void setBrandService(BrandService brandService) {
    this.brandService = brandService;
  }

  public BuyingDirectorService getBuyingDirectorService() {
    return buyingDirectorService;
  }

  public void setBuyingDirectorService(BuyingDirectorService buyingDirectorService) {
    this.buyingDirectorService = buyingDirectorService;
  }

  public SeasonService getSeasonService() {
    return seasonService;
  }

  public void setSeasonService(SeasonService seasonService) {
    this.seasonService = seasonService;
  }

  public FashionLevelService getFashionLevelService() {
    return fashionLevelService;
  }

  public void setFashionLevelService(FashionLevelService fashionLevelService) {
    this.fashionLevelService = fashionLevelService;
  }

  public String getSelectedBrand() {
    return selectedBrand;
  }

  public void setSelectedBrand(String selectedBrand) {
    this.selectedBrand = selectedBrand;
  }

  public List<Brand> getBrands() {
    return brands;
  }

  public void setBrands(List<Brand> brands) {
    this.brands = brands;
  }

  public Long getSelectedBuyingDirector() {
    return selectedBuyingDirector;
  }

  public void setSelectedBuyingDirector(Long selectedBuyingDirector) {
    this.selectedBuyingDirector = selectedBuyingDirector;
  }

  public List<BuyingDirector> getBuyingDirectors() {
    return buyingDirectors;
  }

  public void setBuyingDirectors(List<BuyingDirector> buyingDirectors) {
    this.buyingDirectors = buyingDirectors;
  }

  public Long getSelectedAssortmentGroup() {
    return selectedAssortmentGroup;
  }

  public void setSelectedAssortmentGroup(Long selectedAssortmentGroup) {
    this.selectedAssortmentGroup = selectedAssortmentGroup;
  }

  public List<AssortmentGroup> getAssortmentGroups() {
    return assortmentGroups;
  }

  public void setAssortmentGroups(List<AssortmentGroup> assortmentGroups) {
    this.assortmentGroups = assortmentGroups;
  }

  public Long getSelectedSeason() {
    return selectedSeason;
  }

  public void setSelectedSeason(Long selectedSeason) {
    this.selectedSeason = selectedSeason;
  }

  public List<Season> getSeasons() {
    return seasons;
  }

  public void setSeasons(List<Season> seasons) {
    this.seasons = seasons;
  }

  public Long getSelectedFashionLevel() {
    return selectedFashionLevel;
  }

  public void setSelectedFashionLevel(Long selectedFashionLevel) {
    this.selectedFashionLevel = selectedFashionLevel;
  }

  public List<FashionLevel> getFashionLevels() {
    return fashionLevels;
  }

  public void setFashionLevels(List<FashionLevel> fashionLevels) {
    this.fashionLevels = fashionLevels;
  }

  @PostConstruct
  public void init() {
    this.brands = this.brandService.findAll();
    this.buyingDirectors = this.buyingDirectorService.findAll();
    this.seasons = this.seasonService.findAll();
    this.fashionLevels = this.fashionLevelService.findAll();
  }

  public void onSelectingBuyingDirector() {
    this.assortmentGroups = this.buyingDirectorService
        .findAssortmentGroupByBuyingDirectorId(selectedBuyingDirector);
  }
}
