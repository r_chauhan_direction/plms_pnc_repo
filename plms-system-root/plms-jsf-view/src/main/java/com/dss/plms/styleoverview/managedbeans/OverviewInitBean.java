
package com.dss.plms.styleoverview.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import com.dss.model.AssortmentGroup;
import com.dss.model.Brand;
import com.dss.model.BrandType;
import com.dss.model.BuyingDirector;
import com.dss.model.FashionLevel;
import com.dss.model.SalesProgram;
import com.dss.model.Season;
import com.dss.model.TrendTopic;
import com.dss.service.BrandService;
import com.dss.service.BrandTypeService;
import com.dss.service.BuyingDirectorService;
import com.dss.service.FashionLevelService;
import com.dss.service.SalesProgramService;
import com.dss.service.SeasonService;
import com.dss.service.TrendTopicService;

/**
 * Managed bean to initialize overview filter operations
 * 
 * @author r.chauhan
 */
public class OverviewInitBean implements Serializable {

  private static final long serialVersionUID = 1L;

  private BrandTypeService brandTypeService;

  private BrandService brandService;

  private BuyingDirectorService buyingDirectorService;

  private SeasonService seasonService;

  private FashionLevelService fashionLevelService;

  private SalesProgramService salesProgramService;

  private TrendTopicService trendTopicService;

  private List<BrandType> brandTypes;

  private List<Brand> brands;

  private List<BuyingDirector> buyingDirectors;

  private List<AssortmentGroup> assortmentGroups;

  private List<Season> seasons;

  private List<FashionLevel> fashionLevels;

  private List<SalesProgram> salesProgram;

  private List<TrendTopic> trendTopics;

  private StyleMasterFilterBean styleMasterFilterBean;

  public OverviewInitBean() {}

  public BrandTypeService getBrandTypeService() {
    return brandTypeService;
  }

  public void setBrandTypeService(BrandTypeService brandTypeService) {
    this.brandTypeService = brandTypeService;
  }

  public BrandService getBrandService() {
    return brandService;
  }

  public void setBrandService(BrandService brandService) {
    this.brandService = brandService;
  }

  public BuyingDirectorService getBuyingDirectorService() {
    return buyingDirectorService;
  }

  public void setBuyingDirectorService(BuyingDirectorService buyingDirectorService) {
    this.buyingDirectorService = buyingDirectorService;
  }

  public SeasonService getSeasonService() {
    return seasonService;
  }

  public void setSeasonService(SeasonService seasonService) {
    this.seasonService = seasonService;
  }

  public FashionLevelService getFashionLevelService() {
    return fashionLevelService;
  }

  public void setFashionLevelService(FashionLevelService fashionLevelService) {
    this.fashionLevelService = fashionLevelService;
  }

  public SalesProgramService getSalesProgramService() {
    return salesProgramService;
  }

  public void setSalesProgramService(SalesProgramService salesProgramService) {
    this.salesProgramService = salesProgramService;
  }

  public TrendTopicService getTrendTopicService() {
    return trendTopicService;
  }

  public void setTrendTopicService(TrendTopicService trendTopicService) {
    this.trendTopicService = trendTopicService;
  }

  public List<BrandType> getBrandTypes() {
    return brandTypes;
  }

  public void setBrandTypes(List<BrandType> brandTypes) {
    this.brandTypes = brandTypes;
  }

  public List<Brand> getBrands() {
    return brands;
  }

  public void setBrands(List<Brand> brands) {
    this.brands = brands;
  }

  public List<BuyingDirector> getBuyingDirectors() {
    return buyingDirectors;
  }

  public void setBuyingDirectors(List<BuyingDirector> buyingDirectors) {
    this.buyingDirectors = buyingDirectors;
  }

  public List<AssortmentGroup> getAssortmentGroups() {
    return assortmentGroups;
  }

  public void setAssortmentGroups(List<AssortmentGroup> assortmentGroups) {
    this.assortmentGroups = assortmentGroups;
  }

  public List<Season> getSeasons() {
    return seasons;
  }

  public void setSeasons(List<Season> seasons) {
    this.seasons = seasons;
  }

  public List<FashionLevel> getFashionLevels() {
    return fashionLevels;
  }

  public void setFashionLevels(List<FashionLevel> fashionLevels) {
    this.fashionLevels = fashionLevels;
  }

  public StyleMasterFilterBean getStyleMasterFilterBean() {
    return styleMasterFilterBean;
  }

  public void setStyleMasterFilterBean(StyleMasterFilterBean styleMasterFilterBean) {
    this.styleMasterFilterBean = styleMasterFilterBean;
  }

  public List<SalesProgram> getSalesProgram() {
    return salesProgram;
  }

  public void setSalesProgram(List<SalesProgram> salesProgram) {
    this.salesProgram = salesProgram;
  }

  public List<TrendTopic> getTrendTopics() {
    return trendTopics;
  }

  public void setTrendTopics(List<TrendTopic> trendTopics) {
    this.trendTopics = trendTopics;
  }

  @PostConstruct
  public void init() {
    this.brandTypes = this.brandTypeService.findAll();
    this.brands = this.brandService.findAll();
    this.buyingDirectors = this.buyingDirectorService.findAll();
    this.seasons = this.seasonService.findAll();
    this.fashionLevels = this.fashionLevelService.findAll();
    this.salesProgram = this.salesProgramService.findAll();
    this.trendTopics = this.trendTopicService.findAll();
  }

  public void onSelectingBuyingDirector() {
    this.assortmentGroups = this.buyingDirectorService.findAssortmentGroupByBuyingDirectorId(
        this.styleMasterFilterBean.getSelectedBuyingDirector());
  }

}
