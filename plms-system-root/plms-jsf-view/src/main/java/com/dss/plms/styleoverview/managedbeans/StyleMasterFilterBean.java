
package com.dss.plms.styleoverview.managedbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.dss.model.StyleMaster;
import com.dss.model.StyleOverviewSearchCriteria;
import com.dss.plms.jsf.util.objects.ColumnModel;
import com.dss.service.StyleMasterService;

@ManagedBean(name = "styleMasterFilterBean")
@ViewScoped
public class StyleMasterFilterBean implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long selectedBrandType;

  private Long selectedBrand;

  private Long selectedBuyingDirector;

  private Long selectedAssortmentGroup;

  private Long selectedSeason;

  private Long selectedFashionLevel;

  private String enteredStyleNumber;

  private Long selectedSalesProgram;

  private Long selectedTrendTopic;

  private String enteredTheme;

  private List<StyleMaster> styleMasters = new ArrayList<StyleMaster>();

  private StyleMaster selectedStyleMaster;

  @ManagedProperty(value = "#{styleMasterService}")
  private StyleMasterService styleMasterService;

  private List<ColumnModel> styleMasterTableColumn;

  public StyleMasterFilterBean() {}

  public Long getSelectedBrandType() {
    return selectedBrandType;
  }

  public void setSelectedBrandType(Long selectedBrandType) {
    this.selectedBrandType = selectedBrandType;
  }

  public Long getSelectedBrand() {
    return selectedBrand;
  }

  public void setSelectedBrand(Long selectedBrand) {
    this.selectedBrand = selectedBrand;
  }

  public Long getSelectedBuyingDirector() {
    return selectedBuyingDirector;
  }

  public void setSelectedBuyingDirector(Long selectedBuyingDirector) {
    this.selectedBuyingDirector = selectedBuyingDirector;
  }

  public Long getSelectedAssortmentGroup() {
    return selectedAssortmentGroup;
  }

  public void setSelectedAssortmentGroup(Long selectedAssortmentGroup) {
    this.selectedAssortmentGroup = selectedAssortmentGroup;
  }

  public Long getSelectedSeason() {
    return selectedSeason;
  }

  public void setSelectedSeason(Long selectedSeason) {
    this.selectedSeason = selectedSeason;
  }

  public Long getSelectedFashionLevel() {
    return selectedFashionLevel;
  }

  public void setSelectedFashionLevel(Long selectedFashionLevel) {
    this.selectedFashionLevel = selectedFashionLevel;
  }

  public String getEnteredStyleNumber() {
    return enteredStyleNumber;
  }

  public void setEnteredStyleNumber(String enteredStyleNumber) {
    this.enteredStyleNumber = enteredStyleNumber;
  }

  public Long getSelectedSalesProgram() {
    return selectedSalesProgram;
  }

  public void setSelectedSalesProgram(Long selectedSalesProgram) {
    this.selectedSalesProgram = selectedSalesProgram;
  }

  public Long getSelectedTrendTopic() {
    return selectedTrendTopic;
  }

  public void setSelectedTrendTopic(Long selectedTrendTopic) {
    this.selectedTrendTopic = selectedTrendTopic;
  }

  public String getEnteredTheme() {
    return enteredTheme;
  }

  public void setEnteredTheme(String enteredTheme) {
    this.enteredTheme = enteredTheme;
  }

  public StyleMasterService getStyleMasterService() {
    return styleMasterService;
  }

  public void setStyleMasterService(StyleMasterService styleMasterService) {
    this.styleMasterService = styleMasterService;
  }

  public List<StyleMaster> getStyleMasters() {
    return styleMasters;
  }

  public void setStyleMasters(List<StyleMaster> styleMasters) {
    this.styleMasters = styleMasters;
  }

  public StyleMaster getSelectedStyleMaster() {
    return selectedStyleMaster;
  }

  public void setSelectedStyleMaster(StyleMaster selectedStyleMaster) {
    this.selectedStyleMaster = selectedStyleMaster;
  }

  public List<ColumnModel> getStyleMasterTableColumn() {
    return styleMasterTableColumn;
  }

  public void setStyleMasterTableColumn(List<ColumnModel> styleMasterTableColumn) {
    this.styleMasterTableColumn = styleMasterTableColumn;
  }

  @PostConstruct
  public void init() {
    LinkedHashMap<String, String> defaultStyleColumns = new LinkedHashMap<String, String>();
    defaultStyleColumns.put("Brand", "brandDesc");
    defaultStyleColumns.put("Style Number", "styleNumber");
    defaultStyleColumns.put("Style Description", "styleDesc");
    defaultStyleColumns.put("Season", "seasons");
    defaultStyleColumns.put("Number of Colors", "numberOfColors");
    this.styleMasterTableColumn = new ArrayList<ColumnModel>();
    for (Map.Entry<String, String> columnModel : defaultStyleColumns.entrySet()) {
      this.styleMasterTableColumn
          .add(new ColumnModel(columnModel.getKey(), columnModel.getValue()));
    }
  }

  public void getStyleData() {
    final StyleOverviewSearchCriteria styleOverviewSearchCriteria = new StyleOverviewSearchCriteria();
    styleOverviewSearchCriteria.setBrandId(this.selectedBrand != null ? this.selectedBrand : null);
    styleOverviewSearchCriteria.setAssortmentGroupId(
        this.selectedAssortmentGroup != null ? this.selectedAssortmentGroup : null);
    styleOverviewSearchCriteria.setBuyingDirectorId(
        this.selectedBuyingDirector != null ? this.selectedBuyingDirector : null);
    styleOverviewSearchCriteria.setStyleNumber(
        !this.enteredStyleNumber.isEmpty() ? Long.valueOf(this.enteredStyleNumber) : null);
    styleOverviewSearchCriteria
        .setSeasonId(this.selectedSeason != null ? this.selectedSeason : null);
    styleOverviewSearchCriteria
        .setSalesProgramId(this.selectedSalesProgram != null ? this.selectedSalesProgram : null);
    styleOverviewSearchCriteria
        .setTrendTopicId(this.selectedTrendTopic != null ? this.selectedTrendTopic : null);
    styleOverviewSearchCriteria
        .setBrandTypeId(this.selectedBrandType != null ? this.selectedBrandType : null);
    styleOverviewSearchCriteria
        .setThemeDesc(!this.enteredTheme.isEmpty() ? this.enteredTheme : null);
    styleOverviewSearchCriteria
        .setFashionLevelId(this.selectedFashionLevel != null ? this.selectedFashionLevel : null);
    final List<StyleMaster> data = this.styleMasterService
        .findStyleMasterBySearchCriteria(styleOverviewSearchCriteria);
    this.styleMasters = data;
    if (this.styleMasters != null && !this.styleMasters.isEmpty())
      this.selectedStyleMaster = this.styleMasters.get(0);
  }

  public void resetValues() {
    this.styleMasters = new ArrayList<StyleMaster>();
    this.selectedBrandType = null;
    this.selectedBrand = null;
    this.selectedAssortmentGroup = null;
    this.selectedBuyingDirector = null;
    this.enteredStyleNumber = null;
    this.selectedSeason = null;
    this.selectedSalesProgram = null;
    this.selectedTrendTopic = null;
    this.enteredTheme = null;
    this.selectedFashionLevel = null;
    this.selectedStyleMaster = null;
  }

  public void onStyleMasterSelect() {
    // System.out.println(this.selectedStyleMaster);
  }

}
